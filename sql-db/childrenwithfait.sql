-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-08-2017 a las 03:17:10
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `childrenwithfait`
--
CREATE DATABASE IF NOT EXISTS `childrenwithfait` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `childrenwithfait`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admincorebundlerole`
--

CREATE TABLE IF NOT EXISTS `admincorebundlerole` (
  `id` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admincorebundlerole`
--

INSERT INTO `admincorebundlerole` (`id`, `role`) VALUES
(2, 'ROLE_ADMIN'),
(3, 'ROLE_SUPER_ADMIN'),
(1, 'ROLE_USER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admincorebundleuser`
--

CREATE TABLE IF NOT EXISTS `admincorebundleuser` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photoPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admincorebundleuser`
--

INSERT INTO `admincorebundleuser` (`id`, `username`, `fullName`, `email`, `password`, `salt`, `photoPath`) VALUES
(1, 'admin', 'System Admin', 'admin@localhost.loc', 'dCy/vw+d4FupK9KKSTiCiYOk17ii0G7b9/I528mcwNLIMvs0Fyo0Juf4bi/dmrUs8sQOTbTQsQ0hdw+YppwhXA==', '4f7eeb4f4eadea2a402efc651f5252ff', 'user.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admincorebundleuser_admincorebundlerole`
--

CREATE TABLE IF NOT EXISTS `admincorebundleuser_admincorebundlerole` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `admincorebundleuser_admincorebundlerole`
--

INSERT INTO `admincorebundleuser_admincorebundlerole` (`user_id`, `role_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlebanner`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlebanner` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photoPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlebanner`
--

INSERT INTO `appbackendbundlebanner` (`id`, `name`, `title`, `photoPath`, `position`) VALUES
(1, 'Jonathan', 'Jonathan  quiere ser futbolista', '64d93355ac039727e2a60facf1cb65647f1065bf.jpg', 2),
(2, 'Alondra', 'Alondra quiere ser maestra.', 'b4832296bb98b5afdbe98c9d817f2022fbb0ad83.jpg', 1),
(3, 'Pablo', 'Pablo quiere ser mecánico...', '8974f026411ebf9b39df2c71a3b85952b9ccf935.jpg', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlecenteraspiration`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlecenteraspiration` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlecenteraspiration`
--

INSERT INTO `appbackendbundlecenteraspiration` (`id`, `title`, `description`) VALUES
(1, 'Primera aspiración.', 'Tener posibilidades de expansión, y así poder dar alojo a una mayor\r\ncantidad de niños que requieran de nuestro apoyo, acondicionamiento constante\r\nde las instalaciones, promoviendo así el desarrollo integral y poder lograr\r\nnuestras metas de manera más á'),
(2, 'Segunda aspiración.', 'Continuar proporcionándoles a los niños y las niñas alternativas de\r\ndesarrollo humano integral que les permitan adquirir cada vez más y mejores herramientas\r\npara así superar su condición social, ofreciéndoles en un futuro mejores\r\noportunidades para que'),
(3, 'Tercera aspiración.', '<p><span style="background-color:rgb(250, 250, 250); font-family:open sans,sans-serif; font-size:15px">Richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tem');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlecentercharacteristic`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlecentercharacteristic` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlecentercharacteristic`
--

INSERT INTO `appbackendbundlecentercharacteristic` (`id`, `title`, `icon`, `content`) VALUES
(1, 'Quiénes somos?', 'fa-cog', '“Niños con Fe” Casa\r\nHogar A. C., refugio de niños víctimas de violencia intra-familiar,\r\nProporcionando a los niños hospedaje, alimentación, ropa, calzado, atención\r\nmédica y apoyo escolar. <br><br><br><br>'),
(2, 'Horario de atención', 'fa-check', 'Con\r\nlas puertas abiertas las 24 horas del día, los 7 días de la semana, para las\r\npersonas que así lo soliciten, sin importar su credo, condición social,\r\ncultural o económica.&nbsp; <br><br><br><br>'),
(3, 'Los objetivos que persigue “Niños con fe” Casa Hogar A. C. y que se encuentran definidos dentro de nuestros estatutos, son:  ', 'fa-desktop', '<br><ol><li><i>La atención a\r\nrequerimientos básicos de subsistencia en materia de alimentación, vestido o\r\nvivienda.&nbsp;&nbsp;&nbsp;</i> </li><li><i>La asistencia o\r\nrehabilitación médica o a la atención en establecimientos especializados.</i><i>&nbsp;'),
(4, 'Nuestro propósito', 'fa-users', 'Se procura la\r\nreintegración del menor a su núcleo familiar cuando éste vuelva a ser propicio,\r\nsalvaguardando tanto su integridad física, así como emocional en todo momento.\r\n\r\n<br><br><br><br>'),
(5, 'Característica 5', 'fa-leaf', '<i><span>La atención a requerimientos básicos de\r\nsubsistencia en materia de alimentación, vestido o vivienda.&nbsp;&nbsp;&nbsp; </span></i><br><br><br><br>'),
(6, 'Nuestra Misión', 'fa-area-chart', 'Ayudar con el cuidado y la atención de niños y niñas que se\r\nencuentran en situación de maltrato, pobreza y sin escolarizar.&nbsp; \r\n\r\n<br><br><br><br>'),
(7, 'Característica 7', 'fa-child', '<i>La asistencia jurídica,\r\nel apoyo y la promoción, para la tutela de los derechos de los menores, así\r\ncomo para la readaptación social de personas que han llevado a cabo conductas\r\nilícitas.&nbsp; </i>\r\n\r\n<br><br><br><br>'),
(8, 'Nuestra Prioridad', 'fa-codepen', 'Fomentar su educación, procurando su integridad física y\r\nemocional en todo momento. Y si fuera posible, reintegrarlos a su núcleo\r\nfamiliar cuando éste ya sea un lugar seguro para ellos\r\n\r\n<br><br><br><br>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundledonation`
--

CREATE TABLE IF NOT EXISTS `appbackendbundledonation` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `currency` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateOf` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundledonation`
--

INSERT INTO `appbackendbundledonation` (`id`, `user_id`, `amount`, `currency`, `dateOf`) VALUES
(1, NULL, 10, 'USD', '2017-05-22'),
(2, NULL, 5, 'MXN', '2017-05-22'),
(3, NULL, 5, 'MXN', '2017-05-23'),
(4, NULL, 5, 'MXN', '2017-06-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundledonationcurrencies`
--

CREATE TABLE IF NOT EXISTS `appbackendbundledonationcurrencies` (
  `id` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundledonationcurrencies`
--

INSERT INTO `appbackendbundledonationcurrencies` (`id`, `type`, `label`) VALUES
(1, 'USD', '- $ USD -'),
(2, 'CAD', '- $ CAD -'),
(3, 'AUD', '- $ AUD -'),
(4, 'NZD', '- $ NZD -'),
(5, '-  EUR -', '-  EUR -'),
(6, 'GBP', '- £ GBP -'),
(7, 'MXN', '- $ MXN -');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundledonationqty`
--

CREATE TABLE IF NOT EXISTS `appbackendbundledonationqty` (
  `id` int(11) NOT NULL,
  `value` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundledonationqty`
--

INSERT INTO `appbackendbundledonationqty` (`id`, `value`) VALUES
(1, 10),
(2, 25),
(3, 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlehelperblock`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlehelperblock` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlehelperblock`
--

INSERT INTO `appbackendbundlehelperblock` (`id`, `title`, `content`, `subtitle`, `photo`) VALUES
(1, 'Colaboradores', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure aperiam consequatur quo.', 'Someone famous in Source Title', '/resources/icon-people.png'),
(2, 'Usuarios registrados', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure aperiam consequatur quo.', 'Someone famous in Source Title', '/resources/icon-people.png'),
(3, 'Patrocinadores', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure aperiam consequatur quo.', 'Someone famous in Source Title', '/resources/icon-people.png'),
(4, 'Registros', 'Puedes ragistrarte en nuestra página y recibir notificaciones sobre los avances de nuestro centro, logros de nuestros niños o proyectos que tenemos en puerta.<br>', 'Someone famous in Source Title', '/resources/icon-people.png'),
(5, 'Usuarios anónimos', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure aperiam consequatur quo.', 'Someone famous in Source Title', '/resources/icon-people.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlekid`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlekid` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `introText` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gallery` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `biography` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `dateOfReception` date NOT NULL,
  `ailments` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlekid`
--

INSERT INTO `appbackendbundlekid` (`id`, `name`, `introText`, `photo`, `gallery`, `birthdate`, `biography`, `dateOfReception`, `ailments`, `slug`) VALUES
(1, 'Brianda A.', 'Brianda sueña...', '/resources/Alondra_toon.jpeg', '-', '2004-07-31', '<p>Alondra sue&ntilde;a con seguir desarrollandose, y en un futuro ser M&eacute;dico, Profesora y Futbolista. Tiene buen desempe&ntilde;o acad&eacute;mico, participa en el equipo de futbol de su escuela, es portera.</p>', '2014-06-10', 'Visuales', 'brianda-a'),
(5, 'Pablo Q.', 'Pablo sueña...', '/resources/Pablo.jpeg', '-', '2004-12-10', '<p>Pablo sue&ntilde;a con ser mec&aacute;nico.&nbsp; Su padre le ha ense&ntilde;ado, y quiere seguir aprendiendo, a ayudado ya en un taller, junto a su padre...</p>\r\n\r\n<p>En un futuro quisiera viajar con su familia y conocer distintas ciudades de su pa&am', '2015-10-12', '-', 'pablo-q'),
(6, 'Jonathan A. Luis', 'Jonathan sueña...', '/resources/Jonathan.jpeg', '-', '2005-08-06', '<p>Jonathan sue&ntilde;a con ser futbolista, es muy habil en ese deporte, aunque tambi&eacute;n tiene mucha habilidad en el &aacute;rea l&oacute;gico-matem&aacute;tica, desea tambi&eacute;n ser Ingeniero en Tecnologias de la Informaci&oacute;n y Comunicac', '2015-11-13', '-', 'jonathan-a-luis'),
(9, 'Erick R. Hernández', 'Erick sueña...', '/resources/Erick.jpeg', '-', '2009-06-02', '<p>Erick sue&ntilde;a con...</p>', '2015-11-13', '-', 'erick-r-hernndez'),
(10, 'Kimberly B. Hernández', 'Kimberly sueña...', '/resources/Kimberly.jpeg', '-', '2009-05-23', '<p>Berenice sue&ntilde;a con seguir estudiando y&nbsp; lograr ser una gran maestra y m&eacute;dico.</p>', '2014-06-10', '-', 'kimberly-b-hernndez'),
(11, 'Dilan G. Hernández', 'Dilan sueña...', '/resources/Dilan.jpeg', '-', '2012-07-29', '<p>Dilan sue&ntilde;a con...</p>', '2014-06-10', '-', 'dilan-g-hernndez'),
(12, 'Keyli H.', 'Keyli sueña...', '/resources/Keyli.jpeg', '-', '2014-06-06', '<p>A Keyli le gusta mucho jugar con los beb&eacute;s....</p>', '2014-06-10', '-', 'keyli-h'),
(16, 'Lesli D. Ramirez', 'Lesli sueña con...', '/resources/Lesli.jpeg', '-', '2008-04-07', '<p>Lesli sue&ntilde;a con ser profesora</p>', '2017-01-20', '-', 'lesli-d-ramirez'),
(17, 'Madeline A.Ontiveros', 'Madeline Sueña...', '/resources/Madeline.jpeg', '-', '2010-10-26', '<p>Madeline sue&ntilde;a con ser Enfermera</p>', '2017-01-20', '-', 'madeline-aontiveros'),
(18, 'Bayron J. Ontiveros', 'Bayron sueña...', '/resources/Bayron.jpeg', '-', '2013-08-20', '<p>Bayron sue&ntilde;a...</p>', '2017-01-20', '-', 'bayron-j-ontiveros'),
(19, 'Rosa C.Rodriguez', 'Rosa es una pequeña...', '/resources/Rosa.jpeg', '-', '2016-07-23', '<p>Rosa apenas es una peque&ntilde;a...</p>', '2017-01-20', '-', 'rosa-crodriguez'),
(20, 'Etny A. Casanova', 'Etny es un niño muy inteligente...', '/resources/Etny.jpeg', '-', '2014-03-15', '<p>Etny es un ni&ntilde;o muy inteligente...</p>', '2015-10-12', '-Visuales - Otros', 'etny-a-casanova');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlemenu`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlemenu` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pagePosition` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `externalLink` tinyint(1) DEFAULT NULL,
  `enable` tinyint(1) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `onlyDisplayIcon` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlemenu`
--

INSERT INTO `appbackendbundlemenu` (`id`, `name`, `caption`, `link`, `pagePosition`, `externalLink`, `enable`, `position`, `icon`, `onlyDisplayIcon`) VALUES
(1, 'GALERÍA / VIDA', 'GALERÍA / VIDA', '#galeriavida', 'top', 0, 1, 10, '-', 0),
(2, '¿QUIÉNES SOMOS?', '¿QUIÉNES SOMOS?', '#quienessomos', 'top', 0, 1, 9, '-', 0),
(3, 'DONATIVO', 'DONATIVO', '#donativo', 'top', 0, 1, 8, '-', 0),
(4, 'CONTÁCTENOS', 'CONTÁCTENOS', '#contacto', 'top', 0, 1, 7, '-', 0),
(5, 'Facebook', 'Facebook', 'www.childrenwithfaith.com#', 'bottom', 1, 1, 6, 'fa-facebook', 1),
(6, 'Twitter', 'Twitter', 'http://twitter.com/', 'bottom', 1, 1, 5, 'fa-twitter', 1),
(7, 'Google+', 'Google+', 'http://plus.google.com/', 'bottom', 1, 1, 4, 'fa-google-plus', 1),
(8, 'Skype', 'Skype', 'http://www.skype.com/', 'bottom', 1, 1, 3, 'fa-skype', 1),
(9, 'Youtube', 'Youtube', 'http://www.youtube.com/', 'bottom', 1, 1, 2, 'fa-youtube', 1),
(10, 'Flickr', 'Flickr', 'http://www.flickr.com/', 'bottom', 1, 1, 1, 'fa-flickr', 1),
(11, 'Pinterest', 'Pinterest', 'http://www.pinterest.com/', 'bottom', 1, 1, 0, 'fa-pinterest', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlepage`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlepage` (
  `id` int(11) NOT NULL,
  `title` varchar(550) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlepage`
--

INSERT INTO `appbackendbundlepage` (`id`, `title`, `content`, `score`, `slug`) VALUES
(1, 'P&aacute;gina de prueba', '<p>En inform&aacute;tica, la World Wide Web (WWW) o red inform&aacute;tica mundial es un sistema de distribuci&oacute;n de documentos de hipertexto o hipermedios interconectados y accesibles v&iacute;a Internet. Con un navegador web,&nbsp;un usuario visualiza sitios web compuestos de p&aacute;ginas web que pueden contener texto, im&aacute;genes, v&iacute;deos u otros contenidos multimedia, y navega a trav&eacute;s de esas p&aacute;ginas usando hiperenlaces.</p>\n\n<p>En el n&uacute;mero de mayo de 1970 de la revista Popular Science, Arthur C. Clarke predijo que alg&uacute;n d&iacute;a los sat&eacute;lites &quot;llevar&aacute;n el conocimiento acumulado del mundo a sus manos&quot; con una consola que combinara la funcionalidad de la fotocopiadora, tel&eacute;fono, televisi&oacute;n y un peque&ntilde;o ordenador, que permitir&aacute; la transferencia de datos y videoconferencia en todo el mundo.[10]</p>\n\n<p>En marzo de 1989, Tim Berners-Lee escribi&oacute; una propuesta que hace referencia ENQUIRE, una base de datos y proyectos de software que hab&iacute;a construido en 1980, y describe un sistema de gesti&oacute;n de la informaci&oacute;n m&aacute;s elaborado.</p>\n\n<p>La idea subyacente de la Web se remonta a la propuesta de Vannevar Bush en los a&ntilde;os 40 sobre un sistema similar: a grandes rasgos, un entramado de informaci&oacute;n distribuida con una interfaz operativa que permit&iacute;a el acceso tanto a la misma como a otros art&iacute;culos relevantes determinados por claves. Este proyecto nunca fue materializado, quedando relegado al plano te&oacute;rico bajo el nombre de Memex. Es en los a&ntilde;os 50 cuando Ted Nelson realiza la primera referencia a un sistema de hipertexto, donde la informaci&oacute;n es enlazada de forma libre. Pero no es hasta 1980, con un soporte operativo tecnol&oacute;gico para la distribuci&oacute;n de informaci&oacute;n en redes inform&aacute;ticas, cuando Tim Berners-Lee propone ENQUIRE al CERN (refiri&eacute;ndose a Enquire Within Upon Everything, en castellano Preguntando de Todo Sobre Todo), donde se materializa la realizaci&oacute;n pr&aacute;ctica de este concepto de incipientes nociones de la Web.</p>\n\n<p>En marzo de 1989, Tim Berners Lee, ya como personal de la divisi&oacute;n DD del CERN, redacta la propuesta,[12] que referenciaba a ENQUIRE y describ&iacute;a un sistema de gesti&oacute;n de informaci&oacute;n m&aacute;s elaborado. No hubo un bautizo oficial o un acu&ntilde;amiento del t&eacute;rmino Web en esas referencias iniciales, utiliz&aacute;ndose para tal efecto el t&eacute;rmino mesh. Sin embargo, el World Wide Web ya hab&iacute;a nacido. Con la ayuda de Robert Cailliau, se public&oacute; una propuesta m&aacute;s formal para la World Wide Web[13] el 6 de agosto de 1991.</p>\n', 0, 'pagina-de-prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlerequirementscatalog`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlerequirementscatalog` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlesponsor`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlesponsor` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `photoPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patronageType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amountMonthly` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appbackendbundlevisitcounter`
--

CREATE TABLE IF NOT EXISTS `appbackendbundlevisitcounter` (
  `id` int(11) NOT NULL,
  `device` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `navigator` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `os` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateOfVisit` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=357 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appbackendbundlevisitcounter`
--

INSERT INTO `appbackendbundlevisitcounter` (`id`, `device`, `navigator`, `version`, `os`, `dateOfVisit`) VALUES
(1, 'Browser', 'Firefox', '54.0', 'Windows 8', '2017-05-17 03:03:19'),
(2, 'Browser', 'Firefox', '53.0', 'Windows 8', '2017-05-17 03:04:11'),
(3, 'Browser', 'Firefox', '53.0', 'Windows 8', '2017-05-17 03:20:21'),
(4, 'Library', 'curl', '', ' ', '2017-05-17 04:22:41'),
(5, 'Browser', 'Safari', '6.1.6', 'Mac 10.7.5', '2017-05-17 04:47:39'),
(6, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-17 05:37:48'),
(7, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-05-17 06:16:09'),
(8, 'Browser', 'Firefox', '33.0', 'Windows XP', '2017-05-17 06:26:06'),
(9, 'Browser', 'Firefox', '47.0', 'Windows Vista', '2017-05-17 06:26:37'),
(10, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 08:48:35'),
(11, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 08:48:45'),
(12, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 09:48:09'),
(13, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 09:48:12'),
(14, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 10:37:49'),
(15, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 10:37:53'),
(16, 'Browser', 'Chrome', '21.0.1180.89', 'Windows 7', '2017-05-17 11:20:46'),
(17, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 11:27:58'),
(18, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 11:28:00'),
(19, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 11:47:40'),
(20, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 11:47:43'),
(21, 'Browser', 'Safari', '10.0.3', 'Mac 10.12.3', '2017-05-17 15:00:27'),
(22, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 18:46:53'),
(23, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 18:46:55'),
(24, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 21:55:43'),
(25, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-17 21:55:45'),
(26, 'Browser', 'Firefox', '1.0', 'Windows 8', '2017-05-17 23:28:35'),
(27, 'Browser', 'Firefox', '14.0.1', 'Mac 10.7', '2017-05-17 23:28:40'),
(28, 'Browser', 'Firefox', '51.0', 'Windows 10', '2017-05-18 01:23:01'),
(29, 'Browser', 'Firefox', '31.0', 'Windows 7', '2017-05-18 01:24:41'),
(30, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 01:41:49'),
(31, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 01:41:51'),
(32, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 04:42:45'),
(33, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 04:42:47'),
(34, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-05-18 05:00:58'),
(35, 'Smartphone', 'Mobile Safari', '6.0', 'iOS 6.1.3', '2017-05-18 06:16:05'),
(36, 'Browser', 'Internet Explorer', '8', 'Windows 7', '2017-05-18 06:16:05'),
(37, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-18 06:25:44'),
(38, 'Browser', 'Chrome', '46.0.2490.86', 'Mac 10.11.1', '2017-05-18 07:04:53'),
(39, 'Browser', 'Chrome', '46.0.2490.86', 'Mac 10.11.1', '2017-05-18 07:04:53'),
(40, 'Browser', 'Chrome', '46.0.2490.86', 'Mac 10.11.1', '2017-05-18 07:04:53'),
(41, 'Browser', 'Chrome', '39.0.2171.95', 'Mac 10.10.1', '2017-05-18 07:22:10'),
(42, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 07:40:57'),
(43, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 07:40:59'),
(44, 'Browser', 'Firefox', '3.6.13', 'Mac 10.5', '2017-05-18 09:23:31'),
(45, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 10:54:50'),
(46, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 10:54:52'),
(47, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 12:42:58'),
(48, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 12:43:00'),
(49, 'Browser', 'Firefox', '49.0', 'Ubuntu ', '2017-05-18 17:20:42'),
(50, 'Browser', 'Firefox', '35.0', ' ', '2017-05-18 19:05:23'),
(51, 'Browser', 'Internet Explorer', '8', 'Windows XP', '2017-05-18 19:39:45'),
(52, 'Browser', 'Internet Explorer', '8', 'Windows XP', '2017-05-18 19:39:48'),
(53, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 22:31:10'),
(54, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-18 22:31:14'),
(55, 'Browser', 'Firefox', '47.0', 'Mac 10.11', '2017-05-18 23:35:38'),
(56, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-18 23:52:30'),
(57, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-19 01:18:51'),
(58, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-19 01:20:11'),
(59, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 05:51:50'),
(60, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 05:52:03'),
(61, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-19 06:31:01'),
(62, 'Library', 'Python Requests', '2.13.0', ' ', '2017-05-19 09:45:46'),
(63, 'Browser', 'Firefox', '35.0', ' ', '2017-05-19 11:53:14'),
(64, 'Browser', 'Firefox', '52.0', 'Windows 10', '2017-05-19 12:29:05'),
(65, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 12:53:18'),
(66, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 12:53:21'),
(67, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 23:25:55'),
(68, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-19 23:25:57'),
(69, 'Browser', 'Chrome', '41.0.2224.3', 'Windows Vista', '2017-05-19 23:39:53'),
(70, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 03:54:13'),
(71, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 03:54:15'),
(72, 'Library', 'Perl', '6.26', ' ', '2017-05-20 04:33:49'),
(73, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-20 05:40:35'),
(74, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 06:32:42'),
(75, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 06:32:45'),
(76, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-20 11:53:22'),
(77, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 14:49:57'),
(78, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-20 14:49:59'),
(79, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-21 00:37:44'),
(80, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-21 00:37:46'),
(81, 'Browser', 'Firefox', '9.0.1', 'Windows XP', '2017-05-21 11:32:17'),
(82, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-21 15:50:50'),
(83, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-21 15:50:52'),
(84, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-22 01:30:30'),
(85, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-22 01:45:23'),
(86, 'Browser', 'Chrome', '21.0.1180.89', 'Windows 7', '2017-05-22 05:29:14'),
(87, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-22 05:30:56'),
(88, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-22 07:29:55'),
(89, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-22 07:59:32'),
(90, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-22 08:25:10'),
(91, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-05-22 11:29:20'),
(92, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-05-22 11:29:27'),
(93, 'Smartphone', 'Mobile Safari', '10.0', 'iOS 10.3.1', '2017-05-22 13:30:46'),
(94, 'Smartphone', 'Mobile Safari', '10.0', 'iOS 10.3.1', '2017-05-22 14:49:31'),
(95, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-22 17:29:01'),
(96, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-22 17:29:05'),
(97, 'Browser', 'Chrome', '21.0.1180.89', 'Windows 7', '2017-05-22 18:58:31'),
(98, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-05-22 23:23:29'),
(99, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-22 23:24:40'),
(100, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-23 01:53:51'),
(101, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-23 03:37:54'),
(102, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-23 03:37:56'),
(103, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-05-23 04:42:02'),
(104, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-23 08:03:18'),
(105, 'Browser', 'Chrome', '31.0.1650.57', 'Windows 7', '2017-05-23 10:07:37'),
(106, 'Browser', 'Chrome', '11.0.696.71', 'Windows 7', '2017-05-23 10:49:40'),
(107, 'Browser', 'Chrome', '11.0.696.71', 'Windows 7', '2017-05-23 14:42:20'),
(108, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-23 15:27:10'),
(109, 'Browser', 'Internet Explorer', '11', 'Windows 10', '2017-05-23 15:28:19'),
(110, 'Browser', 'Firefox', '53.0', 'Windows 10', '2017-05-23 15:28:25'),
(111, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-23 16:52:12'),
(112, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-23 16:52:14'),
(113, 'Browser', 'Chrome', '21.0.1180.89', 'Windows 7', '2017-05-23 17:42:38'),
(114, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-23 20:43:54'),
(115, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-23 21:26:57'),
(116, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-23 21:45:54'),
(117, 'Browser', 'Firefox', '49.0', 'Windows 10', '2017-05-23 22:09:05'),
(118, 'Browser', 'Firefox', '49.0', 'Windows 10', '2017-05-23 22:09:21'),
(119, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-23 22:38:53'),
(120, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-05-23 23:27:00'),
(121, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-24 00:44:21'),
(122, 'Browser', 'Firefox', '0.10.1', 'Windows XP', '2017-05-24 01:21:34'),
(123, 'Browser', 'Chrome', '56.0.2924.87', 'Windows 10', '2017-05-24 10:06:51'),
(124, 'Browser', 'Chrome', '46.0.2490.86', 'Windows 7', '2017-05-24 11:39:13'),
(125, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-24 12:06:35'),
(126, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-24 12:06:37'),
(127, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-24 12:06:39'),
(128, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-24 12:36:17'),
(129, 'Browser', 'Chrome', '53.0.2785.116', 'Mac 10.12.2', '2017-05-24 16:01:12'),
(130, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-05-24 16:01:43'),
(131, 'Browser', 'Chrome', '53.0.2785.116', 'Mac 10.12.2', '2017-05-24 16:03:45'),
(132, 'Browser', 'Chrome', '53.0.2785.116', 'Mac 10.12.2', '2017-05-24 16:03:51'),
(133, 'Browser', 'Chrome', '53.0.2785.116', 'Mac 10.12.2', '2017-05-24 16:04:28'),
(134, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-05-24 16:04:48'),
(135, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-05-24 16:05:09'),
(136, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-05-24 16:05:39'),
(137, 'Browser', 'Chrome', '52.0.2743.116', 'Windows 10', '2017-05-24 16:22:51'),
(138, 'Browser', 'Safari', '9.1.2', 'Mac 10.10.5', '2017-05-24 16:22:52'),
(139, 'Browser', 'Chrome', '52.0.2743.116', 'Windows 7', '2017-05-24 16:22:54'),
(140, 'Browser', 'Safari', '10.0', 'Mac 10.10.5', '2017-05-24 16:22:56'),
(141, 'Browser', 'Chrome', '53.0.2785.116', 'GNU/Linux ', '2017-05-24 16:22:56'),
(142, 'Browser', 'Chrome', '53.0.2785.116', 'Windows 8.1', '2017-05-24 16:22:57'),
(143, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-24 17:48:09'),
(144, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-24 17:48:12'),
(145, 'Browser', 'Firefox', '2.0.0.9', 'Windows XP', '2017-05-24 20:32:42'),
(146, 'Library', 'Python urllib', '2.7', ' ', '2017-05-24 20:49:06'),
(147, 'Browser', 'Internet Explorer', '7.0', 'Windows Vista', '2017-05-25 00:33:19'),
(148, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:19:16'),
(149, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:22:48'),
(150, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:22:55'),
(151, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:26:11'),
(152, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:28:02'),
(153, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:30:32'),
(154, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:43:46'),
(155, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:53:58'),
(156, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:55:18'),
(157, 'Browser', 'Internet Explorer', '6.0', 'Windows XP', '2017-05-25 01:56:40'),
(158, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-05-25 05:14:07'),
(159, 'Library', 'Python Requests', '2.7.0', 'Windows ', '2017-05-25 10:08:00'),
(160, 'Browser', 'Firefox', '52.0', 'Windows 8.1', '2017-05-26 02:43:42'),
(161, 'Browser', 'Firefox', '52.0', 'Windows 8.1', '2017-05-26 02:43:45'),
(162, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:03:51'),
(163, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:06:57'),
(164, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:07:38'),
(165, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:08:39'),
(166, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:09:19'),
(167, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:10:40'),
(168, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:11:22'),
(169, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:13:46'),
(170, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:18:27'),
(171, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:20:25'),
(172, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:21:33'),
(173, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:22:14'),
(174, 'Browser', 'Safari', '5.0.4', 'Windows XP', '2017-05-26 06:23:36'),
(175, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-05-26 07:55:37'),
(176, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 6.0.1', '2017-05-26 12:00:33'),
(177, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-26 15:42:21'),
(178, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-26 18:24:12'),
(179, 'Browser', 'Firefox', '7.0.1', 'Mac 10.7', '2017-05-26 19:40:28'),
(180, 'Browser', 'Safari', '10.1', 'Mac 10.12.4', '2017-05-27 00:15:26'),
(181, 'Browser', 'Firefox', '52.0', 'Windows 8.1', '2017-05-27 04:16:34'),
(182, 'Browser', 'Firefox', '52.0', 'Windows 8.1', '2017-05-27 04:16:37'),
(183, 'Browser', 'Chrome', '34.0.1847.116', 'Windows Vista', '2017-05-27 13:38:58'),
(184, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-27 17:18:26'),
(185, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-05-27 17:18:28'),
(186, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-05-27 20:51:41'),
(187, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-05-27 22:26:58'),
(188, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-05-27 22:27:09'),
(189, 'Browser', 'Internet Explorer', '8', 'Windows 7', '2017-05-27 22:55:01'),
(190, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-28 00:12:33'),
(191, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-28 00:12:35'),
(192, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-28 00:12:37'),
(193, 'Browser', 'Firefox', '49.0', 'Ubuntu ', '2017-05-28 04:01:30'),
(194, 'Browser', 'Chrome', '34.0.1847.137', 'Windows 8.1', '2017-05-28 04:17:52'),
(195, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-05-28 09:14:53'),
(196, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-05-28 09:17:43'),
(197, 'Browser', 'Firefox', '9.0.1', 'Windows XP', '2017-05-28 09:42:34'),
(198, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-29 18:27:56'),
(199, 'Browser', 'Chrome', '56.0.2924.76', 'Windows 7', '2017-05-29 19:32:02'),
(200, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-05-29 20:16:19'),
(201, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-05-30 05:57:37'),
(202, 'Browser', 'Firefox', '3.0', 'Windows XP', '2017-05-30 07:05:20'),
(203, 'Browser', 'Chrome', '56.0.2924.76', 'Windows 7', '2017-05-30 07:40:20'),
(204, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-30 15:20:53'),
(205, 'Browser', 'Internet Explorer', '11', 'Windows 7', '2017-05-30 19:11:55'),
(206, 'Browser', 'Firefox', '15.0', 'Windows 7', '2017-05-30 19:19:14'),
(207, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-05-30 22:03:32'),
(208, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-05-30 23:06:29'),
(209, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-05-30 23:34:29'),
(210, 'Browser', 'Chrome', '56.0.2924.87', 'Windows 10', '2017-05-31 01:35:28'),
(211, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-31 11:55:40'),
(212, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-31 11:55:43'),
(213, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-05-31 11:55:45'),
(214, 'Browser', 'Chrome', '39.0.2171.99', 'GNU/Linux ', '2017-05-31 13:41:36'),
(215, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 6.0.1', '2017-05-31 17:12:49'),
(216, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-05-31 22:25:51'),
(217, 'Browser', 'Chrome', '56.0.2924.87', 'Windows 7', '2017-06-01 00:31:06'),
(218, 'Browser', 'Firefox', '49.0', 'Ubuntu ', '2017-06-01 01:49:15'),
(219, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-01 05:05:37'),
(220, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-06-01 20:46:40'),
(221, 'Browser', 'Chrome', '27.0.1453.90', 'Windows 7', '2017-06-01 20:46:44'),
(222, 'Browser', 'Firefox', '3.6.28', 'Windows 7', '2017-06-02 01:48:43'),
(223, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-02 07:07:33'),
(224, 'Browser', 'Chrome', '11.0.696.71', 'Windows 7', '2017-06-02 16:09:28'),
(225, 'Browser', 'Firefox', '52.0', 'Windows 8.1', '2017-06-02 17:52:25'),
(226, 'Browser', 'Chrome', '11.0.696.71', 'Windows 7', '2017-06-02 21:01:42'),
(227, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-03 19:27:29'),
(228, 'Browser', 'Chrome', '56.0.2924.87', 'Windows 7', '2017-06-03 22:50:13'),
(229, 'Browser', 'Chrome', '21.0.1180.89', 'Windows 7', '2017-06-04 00:25:18'),
(230, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-06-04 00:48:23'),
(231, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-06-04 01:03:58'),
(232, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-04 01:41:46'),
(233, 'Browser', 'Firefox', '49.0', 'Windows 10', '2017-06-04 05:23:24'),
(234, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-06-04 10:30:56'),
(235, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-06-04 19:42:04'),
(236, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-04 20:44:54'),
(237, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-06-04 22:59:13'),
(238, 'Browser', 'Chrome', '56.0.2924.76', 'Windows 7', '2017-06-05 00:52:02'),
(239, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-06-05 07:39:12'),
(240, 'Browser', 'Firefox', '3.0', 'Windows XP', '2017-06-05 09:30:18'),
(241, 'Browser', 'Chrome', '30.0.1599.101', 'Windows XP', '2017-06-05 14:14:22'),
(242, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-06-05 17:01:47'),
(243, 'Browser', 'Firefox', '3.6.3', 'Windows Vista', '2017-06-05 18:49:12'),
(244, 'Browser', 'Firefox', '3.6.3', 'Windows Vista', '2017-06-05 18:49:12'),
(245, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-05 21:46:14'),
(246, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-06 05:52:18'),
(247, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-07 02:11:22'),
(248, 'Browser', 'Chrome', '34.0.1847.116', 'Windows Vista', '2017-06-07 20:56:50'),
(249, 'Browser', 'Chrome', '25.0.1364.172', 'Windows 7', '2017-06-08 01:45:00'),
(250, 'Browser', 'Internet Explorer', '11', 'Windows 7', '2017-06-08 04:22:21'),
(251, 'Browser', 'Firefox', '26.0', 'Ubuntu ', '2017-06-08 04:22:26'),
(252, 'Browser', 'Chrome', '57.0.2987.133', 'Mac 10.11.6', '2017-06-08 06:15:07'),
(253, 'Browser', 'Internet Explorer', '9.0', 'Windows 7', '2017-06-08 07:16:26'),
(254, 'Browser', 'Chrome', '31.0.1650.57', 'Windows 7', '2017-06-08 07:30:09'),
(255, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-08 17:12:44'),
(256, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-06-08 17:42:29'),
(257, 'Library', 'Python Requests', '2.7.0', 'Windows ', '2017-06-09 06:14:00'),
(258, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-09 06:47:37'),
(259, 'Browser', 'Internet Explorer', '11', 'Windows 8.1', '2017-06-09 20:12:18'),
(260, 'Browser', 'Internet Explorer', '11', 'Windows 8.1', '2017-06-09 20:12:19'),
(261, 'Smartphone', 'Chrome Mobile', '18.0.1025.133', 'Android 4.0.4', '2017-06-09 20:12:21'),
(262, 'Smartphone', 'Chrome Mobile', '18.0.1025.133', 'Android 4.0.4', '2017-06-09 20:12:33'),
(263, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-09 20:47:33'),
(264, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-06-09 20:49:05'),
(265, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-06-09 20:49:08'),
(266, 'Browser', 'Opera', '36.0.2130.32', 'Windows 10', '2017-06-10 03:33:57'),
(267, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-10 04:00:52'),
(268, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-10 06:37:37'),
(269, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-10 07:44:11'),
(270, 'Smartphone', 'Chrome Mobile', '56.0.2924.87', 'Android 5.1', '2017-06-10 07:44:16'),
(271, 'Smartphone', 'Mobile Safari', '10.0', 'iOS 10.3.2', '2017-06-10 07:45:50'),
(272, 'Browser', 'Chrome', '53.0.2785.116', 'Mac 10.12.2', '2017-06-10 15:38:07'),
(273, 'Browser', 'Firefox', '2.0.0.13', 'Windows XP', '2017-06-10 15:38:29'),
(274, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-06-10 16:15:29'),
(275, 'Browser', 'Firefox', '15.0.1', 'Mac 10.5', '2017-06-10 19:52:15'),
(276, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-06-10 20:08:30'),
(277, 'Smartphone', 'Mobile Safari', '', 'iOS 10.3.2', '2017-06-10 20:22:56'),
(278, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-11 03:02:05'),
(279, 'Browser', 'Firefox', '9.0.1', 'Windows XP', '2017-06-11 10:02:24'),
(280, 'Browser', 'Firefox', '22.0', 'Windows XP', '2017-06-11 15:18:03'),
(281, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 6.0.1', '2017-06-11 15:41:00'),
(282, 'Browser', 'Microsoft Edge', '14.14393', 'Windows 10', '2017-06-11 20:25:41'),
(283, 'Browser', 'Chrome', '41.0.2228.0', 'Windows 7', '2017-06-11 23:11:14'),
(284, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-12 03:25:17'),
(285, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-12 03:36:19'),
(286, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-12 03:36:21'),
(287, 'Browser', 'Firefox', '9.0.1', 'Windows XP', '2017-06-12 13:46:06'),
(288, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-13 03:10:53'),
(289, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-13 05:06:58'),
(290, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-13 16:24:08'),
(291, 'Browser', 'Firefox', '53.0', 'Windows 10', '2017-06-14 01:07:27'),
(292, 'Browser', 'Chrome', '57.0.2987.133', 'Windows 10', '2017-06-14 05:00:18'),
(293, 'Browser', 'Firefox', '3.6.28', 'Windows 7', '2017-06-14 07:24:27'),
(294, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-14 07:25:54'),
(295, 'Browser', 'Firefox', '49.0', 'Ubuntu ', '2017-06-14 09:48:15'),
(296, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-14 13:22:02'),
(297, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-06-14 14:33:41'),
(298, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-06-14 14:33:45'),
(299, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-06-14 14:33:50'),
(300, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-14 14:47:01'),
(301, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-14 16:24:13'),
(302, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-14 17:51:56'),
(303, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-14 18:47:39'),
(304, 'Browser', 'Firefox', '49.0', 'Windows 10', '2017-06-15 05:45:04'),
(305, 'Browser', 'Chrome', '31.0.1650.57', 'Windows 7', '2017-06-15 11:53:26'),
(306, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-15 14:05:15'),
(307, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-15 14:39:09'),
(308, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8.1', '2017-06-15 15:50:45'),
(309, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-15 17:29:37'),
(310, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-15 18:00:32'),
(311, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-15 19:33:18'),
(312, 'Smartphone', 'Mobile Safari', '10.0', 'iOS 10.2.1', '2017-06-16 04:28:21'),
(313, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-16 04:53:29'),
(314, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-16 09:09:27'),
(315, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-16 10:07:19'),
(316, 'Browser', 'Chrome', '31.0.1650.57', 'Windows 7', '2017-06-16 10:47:25'),
(317, 'Browser', 'Firefox', '10.0', 'Windows NT', '2017-06-16 10:49:52'),
(318, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-16 13:37:17'),
(319, 'Browser', 'Chrome', '33.0.1750.152', 'Mac 10.9.2', '2017-06-16 13:51:08'),
(320, 'Browser', 'Chrome', '49.0.2623.75', 'GNU/Linux ', '2017-06-16 19:33:27'),
(321, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-16 20:57:49'),
(322, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-16 22:45:12'),
(323, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-17 00:10:38'),
(324, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-17 00:18:35'),
(325, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-17 01:40:43'),
(326, 'Browser', 'Chrome', '25.0.1364.172', 'Windows 7', '2017-06-17 02:15:44'),
(327, 'Browser', 'Chrome', '23.0.1271.64', 'GNU/Linux ', '2017-06-17 04:47:56'),
(328, 'Browser', 'Opera', '12.16', 'Windows 7', '2017-06-17 05:09:55'),
(329, 'Browser', 'Chrome', '31.0.1650.63', 'Mac 10.7.5', '2017-06-17 05:09:58'),
(330, 'Browser', 'Firefox', '24.0', 'Windows 7', '2017-06-17 05:10:06'),
(331, 'Browser', 'Firefox', '26.0', 'Mac 10.7', '2017-06-17 05:10:06'),
(332, 'Browser', 'Firefox', '26.0', 'Windows 8', '2017-06-17 05:10:10'),
(333, 'Browser', 'Firefox', '53.0', 'Windows 10', '2017-06-17 05:10:43'),
(334, 'Smartphone', 'Chrome Mobile', '56.0.2924.87', 'Android 5.1', '2017-06-17 05:12:53'),
(335, 'Smartphone', 'Chrome Mobile', '58.0.3029.83', 'Android 5.1', '2017-06-17 05:42:10'),
(336, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-06-17 12:58:25'),
(337, 'Browser', 'Internet Explorer', '8.0', 'Windows Vista', '2017-06-17 12:58:28'),
(338, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 10', '2017-06-17 18:15:41'),
(339, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-17 19:27:04'),
(340, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-18 02:07:08'),
(341, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-18 03:30:03'),
(342, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-18 03:38:11'),
(343, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-19 20:33:53'),
(344, 'Browser', 'Chrome', '58.0.3029.110', 'Windows 8', '2017-06-19 20:33:53'),
(345, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-05 23:57:45'),
(346, 'Browser', 'Firefox', '54.0', 'Windows 8', '2017-07-08 19:50:49'),
(347, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-18 08:30:39'),
(348, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-20 22:29:42'),
(349, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-23 23:29:49'),
(350, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-25 04:04:06'),
(351, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-30 19:46:05'),
(352, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-30 19:46:05'),
(353, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-31 02:06:22'),
(354, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-07-31 05:28:40'),
(355, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-08-01 03:11:41'),
(356, 'Browser', 'Chrome', '59.0.3071.115', 'Windows 8', '2017-08-01 03:11:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `appfrontendbundleuser`
--

CREATE TABLE IF NOT EXISTS `appfrontendbundleuser` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `appfrontendbundleuser`
--

INSERT INTO `appfrontendbundleuser` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `facebook_id`, `facebook_access_token`, `google_id`, `google_access_token`) VALUES
(1, 'Liliana Frontela Inclán', 'liliana frontela inclán', 'lfrontelainclan@gmail.com', 'lfrontelainclan@gmail.com', 1, 'fh5c9fcp70o40cgkkg0kos8cc480g4g', 'W+7/64HO+tiKH/GbcKaHZaAB1OXiwz21Y4LQvs1lgkjreyJk9atfqYya1qns3ZfxU1lF7Jxzlz6hmgRY6bXALQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL),
(2, 'rnapoles86', 'rnapoles86', 'rnapoles86@gmail.com', 'rnapoles86@gmail.com', 1, '5vw5cg1e3ncwgws480cww440gs08g4g', 'lKdaBwos1qVJAyUbUoDHQk0J+z449Z8hOL7CIiPSXLQXI8pwZEs8BSMpa9snhjrTHaxJVuIbysBF+ooZ3XDFIA==', '2017-06-18 03:30:36', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `id` int(11) NOT NULL,
  `varname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `config`
--

INSERT INTO `config` (`id`, `varname`, `value`, `type`) VALUES
(1, 'SiteTitle', 'Niños con Fé', 'text'),
(2, 'OwnInfantText', 'Nuestros Peques', 'text'),
(3, 'OwnInfantSubtitleText', 'Pequeños gigantes que sueñan con un mejor mañana.<br>\r\nEllos quieren ser...', 'textarea'),
(4, 'WithSponsorText', 'Con Patrocinio', 'text'),
(5, 'WithoutSponsorText', 'Sin Patrocinio', 'text'),
(6, 'AllText', 'Todos', 'text'),
(7, 'SiteAddress', ' Calle La Misi&oacute;n, 5018, El Tecolote, Tijuana, Baja California, M&eacute;xico. ', 'text'),
(8, 'SitePhone', '	6643783464', 'text'),
(9, 'SitePBX', '6641741247', 'text'),
(10, 'SiteMainEmail', 'donations_childrenfaith@outlook.com', 'email'),
(11, 'OwnCenterText', 'Nuestro centro', 'text'),
(12, 'OwnCenterSubtitleText', 'Nuestro centro sueña crecer...', 'text'),
(13, 'LifeInCenterText', 'Vida en el Centro', 'text'),
(14, 'LifeInCenterSubtitleText', 'Tener posibilidades de expansión, y así poder dar alojo a una mayor cantidad de niños que requieran de nuestro apoyo', 'text'),
(15, 'LifeInCenterDescriptionText', 'Contar\r\ncon personal de calidad, que se maneje bajo el sentido de pertenencia,\r\nbrindándole seguridad a nuestros niños, para que así se desarrollen de una\r\nmanera óptima e integral.<br><br><br><br>', 'textarea'),
(16, 'AspirationText', 'Aspiramos a...', 'text'),
(17, 'AspirationDescriptionText', '<h3><em>Continuar proporcion&aacute;ndoles a los ni&ntilde;os y las ni&ntilde;as alternativas de desarrollo humano integral que les permitan adquirir cada vez m&aacute;s y mejores herramientas para as&iacute; superar su condici&oacute;n social, ofreci&eacute;ndoles en un futuro mejores oportunidades para que logren ser autosuficientes y promotores de una sociedad m&aacute;s justa y humanitaria. </em></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3><em>Tener posibilidades de expansi&oacute;n, y as&iacute; poder dar alojo a una mayor cantidad de ni&ntilde;os que requieran de nuestro apoyo, acondicionamiento constante de las instalaciones, promoviendo as&iacute; el desarrollo integral y poder lograr nuestras metas de manera m&aacute;s &aacute;gil. </em></h3>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<h3>&nbsp;</h3>\n\n<h3>&nbsp;</h3>\n\n<p>&nbsp;</p>\n', 'textarea'),
(18, 'DonationText', 'Las Donaciones', 'text'),
(19, 'DonationSubtitleText', 'Qué destino tienen las donaciones de quienes nos ayudan.', 'text'),
(20, 'DonationDescriptionText', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi adipisci illo, voluptatum ipsam fuga error commodi architecto, laudantium culpa tenetur at id, beatae placeat deserunt iure quas voluptas fugit eveniet.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo ducimus explicabo quibusdam temporibus deserunt doloremque pariatur ea, animi a. Delectus similique atque eligendi, enim vel reiciendis deleniti neque aliquid, sit?</p>\r\n\r\n<ul>\r\n	<li>&nbsp; &nbsp; Lorem ipsum dolor sit amet</li>\r\n	<li>&nbsp; &nbsp; Reiciendis deleniti neque aliquid</li>\r\n	<li>&nbsp; &nbsp; Ipsam fuga error commodi</li>\r\n	<li>&nbsp; &nbsp; Lorem ipsum dolor sit amet</li>\r\n	<li>&nbsp; &nbsp; Dignissimos molestiae necessitatibus</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', 'html'),
(21, 'WhoHelpText', 'Quienes gustan ayudar', 'text'),
(22, 'AllCanBePartText', 'Todos pueden ser parte de este sueño!!!', 'text'),
(23, 'RegisterText', 'Registrate', 'text'),
(24, 'ContactInformationText', 'Le invitamos a ser parte de nuestra gran misi&oacute;n...<br />\n<br />\nLorem ipsum dolor sit amet, consectetur adipisicing elit. Ve<b>nam </b>magnam natus tempora cumque, aliquam deleniti voluptatibus voluptas. Repellat vel, et itaque commodi iste ab, laudantium voluptas deserunt nobis lorum.', 'textarea'),
(25, 'SendText', 'Enviar', 'text'),
(26, 'ContactUsText', 'Cont&aacute;ctenos', 'text'),
(27, 'MailPort', '25', 'integer'),
(28, 'MailUser', 'rnapoles86@localhost.loc', 'email'),
(29, 'MailPass', '123456', 'password'),
(30, 'MailRecipient', 'rnapoles86@localhost.loc', 'email'),
(31, 'MailServer', '127.0.0.1', 'text'),
(32, 'PayPalDonateText', 'PayPal hacer una donación', 'text'),
(33, 'PayPalAmountText', 'Introducir cantidad', 'text'),
(34, 'PayPalOwnText', 'Manual', 'text'),
(35, 'PayPalSelectCurrText', 'Seleccione el tipo de moneda', 'text'),
(36, 'PayPalSelectText', 'Seleccionar', 'text'),
(37, 'PayPalBtnDonationText', 'Donar', 'text'),
(38, 'AmountText', 'Cantidad', 'text'),
(42, 'DefaultErrText', 'Ups! Error desconocido.', 'text'),
(43, 'PayPalThanksText', 'Thank you very much for your donation!', 'text'),
(44, 'SponsorText', 'Patrocinar', 'text'),
(45, 'ProfileText', 'Perfil', 'text');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ext_translations`
--

CREATE TABLE IF NOT EXISTS `ext_translations` (
  `id` int(11) NOT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `ext_translations`
--

INSERT INTO `ext_translations` (`id`, `locale`, `object_class`, `field`, `foreign_key`, `content`) VALUES
(1, 'en', 'AppBackendBundleEntityMenu', 'caption', '4', 'CONTACT US'),
(2, 'en', 'AppBackendBundleEntityMenu', 'caption', '3', 'CONTRIBUTION'),
(3, 'en', 'AppBackendBundleEntityMenu', 'caption', '2', 'WHO ARE?'),
(4, 'en', 'AppBackendBundleEntityMenu', 'caption', '1', 'GALLERY / LIFE'),
(5, 'en', 'AppBackendBundleEntityBanner', 'title', '1', 'Jonathan wants to be a soccer player'),
(6, 'en', 'AppBackendBundleEntityBanner', 'title', '2', 'Alondra wants to be a teacher'),
(7, 'en', 'AdminCoreBundleEntityConfig', 'value', '4', 'With sponsorship'),
(8, 'en', 'AdminCoreBundleEntityConfig', 'value', '5', 'Without sponsorship'),
(9, 'en', 'AppBackendBundleEntityCenterCharacteristic', 'title', '2', 'Characteristic 2'),
(10, 'en', 'AdminCoreBundleEntityConfig', 'value', '26', 'Contact US'),
(11, 'en', 'AppBackendBundleEntityCenterCharacteristic', 'title', '1', 'Characteristic 1'),
(12, 'en', 'AdminCoreBundleEntityConfig', 'value', '2', 'Our Peques'),
(13, 'en', 'AppBackendBundleEntityBanner', 'title', '3', 'Mechanic...'),
(14, 'en', 'Admin\\CoreBundle\\Entity\\Config', 'value', '43', '¡Gracias por su donativo!');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `requirementscatalog_kid`
--

CREATE TABLE IF NOT EXISTS `requirementscatalog_kid` (
  `requirementscatalog_id` int(11) NOT NULL,
  `kid_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sponsor_kid`
--

CREATE TABLE IF NOT EXISTS `sponsor_kid` (
  `sponsor_id` int(11) NOT NULL,
  `kid_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admincorebundlerole`
--
ALTER TABLE `admincorebundlerole`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_191C2EE657698A6A` (`role`);

--
-- Indices de la tabla `admincorebundleuser`
--
ALTER TABLE `admincorebundleuser`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_C3E672C5F85E0677` (`username`), ADD UNIQUE KEY `UNIQ_C3E672C5E7927C74` (`email`);

--
-- Indices de la tabla `admincorebundleuser_admincorebundlerole`
--
ALTER TABLE `admincorebundleuser_admincorebundlerole`
  ADD PRIMARY KEY (`user_id`,`role_id`), ADD KEY `IDX_EF15F738A76ED395` (`user_id`), ADD KEY `IDX_EF15F738D60322AC` (`role_id`);

--
-- Indices de la tabla `appbackendbundlebanner`
--
ALTER TABLE `appbackendbundlebanner`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlecenteraspiration`
--
ALTER TABLE `appbackendbundlecenteraspiration`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlecentercharacteristic`
--
ALTER TABLE `appbackendbundlecentercharacteristic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundledonation`
--
ALTER TABLE `appbackendbundledonation`
  ADD PRIMARY KEY (`id`), ADD KEY `IDX_61F547ACA76ED395` (`user_id`);

--
-- Indices de la tabla `appbackendbundledonationcurrencies`
--
ALTER TABLE `appbackendbundledonationcurrencies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundledonationqty`
--
ALTER TABLE `appbackendbundledonationqty`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlehelperblock`
--
ALTER TABLE `appbackendbundlehelperblock`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlekid`
--
ALTER TABLE `appbackendbundlekid`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_42D13BE7989D9B62` (`slug`);

--
-- Indices de la tabla `appbackendbundlemenu`
--
ALTER TABLE `appbackendbundlemenu`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlepage`
--
ALTER TABLE `appbackendbundlepage`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_73D00E5F989D9B62` (`slug`);

--
-- Indices de la tabla `appbackendbundlerequirementscatalog`
--
ALTER TABLE `appbackendbundlerequirementscatalog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appbackendbundlesponsor`
--
ALTER TABLE `appbackendbundlesponsor`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_943BF9A4E7927C74` (`email`);

--
-- Indices de la tabla `appbackendbundlevisitcounter`
--
ALTER TABLE `appbackendbundlevisitcounter`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `appfrontendbundleuser`
--
ALTER TABLE `appfrontendbundleuser`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_F83FC12092FC23A8` (`username_canonical`), ADD UNIQUE KEY `UNIQ_F83FC120A0D96FBF` (`email_canonical`);

--
-- Indices de la tabla `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `UNIQ_D3262A4A2D09F425` (`varname`);

--
-- Indices de la tabla `ext_translations`
--
ALTER TABLE `ext_translations`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`), ADD KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`);

--
-- Indices de la tabla `requirementscatalog_kid`
--
ALTER TABLE `requirementscatalog_kid`
  ADD PRIMARY KEY (`requirementscatalog_id`,`kid_id`), ADD KEY `IDX_E38E6B02AE8D7EF5` (`requirementscatalog_id`), ADD KEY `IDX_E38E6B026A973770` (`kid_id`);

--
-- Indices de la tabla `sponsor_kid`
--
ALTER TABLE `sponsor_kid`
  ADD PRIMARY KEY (`sponsor_id`,`kid_id`), ADD KEY `IDX_3289767612F7FB51` (`sponsor_id`), ADD KEY `IDX_328976766A973770` (`kid_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admincorebundlerole`
--
ALTER TABLE `admincorebundlerole`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `admincorebundleuser`
--
ALTER TABLE `admincorebundleuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlebanner`
--
ALTER TABLE `appbackendbundlebanner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlecenteraspiration`
--
ALTER TABLE `appbackendbundlecenteraspiration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlecentercharacteristic`
--
ALTER TABLE `appbackendbundlecentercharacteristic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `appbackendbundledonation`
--
ALTER TABLE `appbackendbundledonation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `appbackendbundledonationcurrencies`
--
ALTER TABLE `appbackendbundledonationcurrencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `appbackendbundledonationqty`
--
ALTER TABLE `appbackendbundledonationqty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlehelperblock`
--
ALTER TABLE `appbackendbundlehelperblock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlekid`
--
ALTER TABLE `appbackendbundlekid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlemenu`
--
ALTER TABLE `appbackendbundlemenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlepage`
--
ALTER TABLE `appbackendbundlepage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlerequirementscatalog`
--
ALTER TABLE `appbackendbundlerequirementscatalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlesponsor`
--
ALTER TABLE `appbackendbundlesponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `appbackendbundlevisitcounter`
--
ALTER TABLE `appbackendbundlevisitcounter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=357;
--
-- AUTO_INCREMENT de la tabla `appfrontendbundleuser`
--
ALTER TABLE `appfrontendbundleuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `ext_translations`
--
ALTER TABLE `ext_translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admincorebundleuser_admincorebundlerole`
--
ALTER TABLE `admincorebundleuser_admincorebundlerole`
ADD CONSTRAINT `FK_EF15F738A76ED395` FOREIGN KEY (`user_id`) REFERENCES `admincorebundleuser` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_EF15F738D60322AC` FOREIGN KEY (`role_id`) REFERENCES `admincorebundlerole` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `appbackendbundledonation`
--
ALTER TABLE `appbackendbundledonation`
ADD CONSTRAINT `FK_61F547ACA76ED395` FOREIGN KEY (`user_id`) REFERENCES `appbackendbundlesponsor` (`id`);

--
-- Filtros para la tabla `requirementscatalog_kid`
--
ALTER TABLE `requirementscatalog_kid`
ADD CONSTRAINT `FK_E38E6B026A973770` FOREIGN KEY (`kid_id`) REFERENCES `appbackendbundlekid` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_E38E6B02AE8D7EF5` FOREIGN KEY (`requirementscatalog_id`) REFERENCES `appbackendbundlerequirementscatalog` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `sponsor_kid`
--
ALTER TABLE `sponsor_kid`
ADD CONSTRAINT `FK_3289767612F7FB51` FOREIGN KEY (`sponsor_id`) REFERENCES `appbackendbundlesponsor` (`id`) ON DELETE CASCADE,
ADD CONSTRAINT `FK_328976766A973770` FOREIGN KEY (`kid_id`) REFERENCES `appbackendbundlekid` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
