<?php

namespace Admin\CoreBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Entity\Config;
use Admin\CoreBundle\Form\ConfigType;
use Admin\CoreBundle\Form\Type\IconButtonType;


/**
 * ConfigController
 * 
 */
class ConfigController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all Config .
	 * @Route("/manage/config/list/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="admin_corebundle_config_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('Admin\CoreBundle\Entity\Config')->findAll();

            /*
			$repo = $em->getRepository('Admin\CoreBundle\Entity\Config');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'C');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);
            
			$entities = $query->getResult();
            */

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			//$ObjList->total= $res['Total'];
			$ObjList->total= 0;
			$ObjList->message= '';
			$ObjList->dql= '';
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AdminCoreBundle:Config:list.html.twig",$this->webconf);
	}

	/**
	 * Create Config .
	 * @Route("/manage/config/create", name="admin_corebundle_config_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new Config();
		$entity->setTranslatableLocale('es');

		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_config_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_config_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_config_edit',array('id'=>$entity->getId())));
		}
		
		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AdminCoreBundle:Config:create.html.twig",$this->webconf);

	}

	/**
	 * Update Config.
	 * @Route("/manage/config/edit/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"}, name="admin_corebundle_config_edit") 
	 */
	public function editAction($id,$_locale)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('Admin\CoreBundle\Entity\Config')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find Config width $id.");
		}

		$entity->setTranslatableLocale($_locale);
		$em->refresh($entity);

		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:Config:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing Config entity.
	 *
	 * @Route("/manage/config/update/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"}, name="admin_corebundle_config_update")
	 */
	public function updateAction(Request $request, $id,$_locale)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('Admin\CoreBundle\Entity\Config')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Config width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {

			$entity->setTranslatableLocale($_locale);
			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_config_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_config_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_config_edit',array('_locale'=>$_locale,'id'=>$id)));

		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AdminCoreBundle:Config:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete Config .
	 * @Route("/manage/config/delete/{id}", name="admin_corebundle_config_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('Admin\CoreBundle\Entity\Config')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Config width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('admin_corebundle_config_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:Config:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete Config list.
	 * @Route("/manage/config/delete_list", name="admin_corebundle_config_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE Admin\CoreBundle\Entity\Config C WHERE C.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('admin_corebundle_config_list'));
	}

	/**
	 * Creates a form to delete a Config entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('admin_corebundle_config_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a Config entity.
	 *
	 * @param Config $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Config $entity)
	{
		$form = $this->createForm(new ConfigType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_config_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
		));

		$form->add('save', 'submit', array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('saveAndReturnToList', 'submit', array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('saveAndAddNew', 'submit', array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Config $entity)
	{
		$form = $this->createForm(new ConfigType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_config_create'),
			'method' => 'POST',
		));

		$form->add('add', 'submit', array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('saveAndReturnToList', 'submit', array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('saveAndAddNew', 'submit', array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary glyphicons circle_ok btn_save'),
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));

		return $form;
	}

}


?>