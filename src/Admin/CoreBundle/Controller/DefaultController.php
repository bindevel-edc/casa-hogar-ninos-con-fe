<?php

namespace Admin\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Finder\Finder;
use Doctrine\DBAL\DriverManager;
use Admin\CoreBundle\Entity\User;
use Admin\CoreBundle\Entity\Role;


class DefaultController extends Controller
{

	/**
	 * Admin Index.
	 * @Route("/manage/", name="manage_home")
	 */
    public function indexAction()
    {

		$this->webconf=array();
        
        //$this->webconf['content'] = 
        
		return $this->render('AdminCoreBundle:Default:dashboard.html.twig', $this->webconf);
	}
    
	/**
	 * Admin Index.
	 * @Route("/manage/statistics", name="manage_statistics")
	 */
    public function statisticAction()
    {

		//$this->webconf['content'] = $this->render('AdminCoreBundle:Default:content.html.twig', $this->webconf);;
		
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('App\BackendBundle\Entity\VisitCounter');		
        $entityName = $repo->getClassName();        
        
        /*
        $dql = "SELECT DISTINCT v.os FROM $entityName v";
        $query = $em->createQuery($dql);
        $items = $query->getResult();

        $osList = array();
        
        if($items){
            
            foreach($items as $item){
                array_push($osList,$item);
            }
            
            //echo json_encode($osList);
        }*/       
        
        $dql = "SELECT v.os,COUNT(v.os) as val FROM $entityName v GROUP BY v.os ORDER BY v.os";
        $query = $em->createQuery($dql);
        $items = $query->getResult();

        $ObjList = new \stdClass();
        $ObjList->osInfo = array();
        $ObjList->browserInfo = array();
        $ObjList->deviceInfo = array();
        $ObjList->success = true;

        if($items){
            foreach($items as $item){
                array_push($ObjList->osInfo,$item);
            }
        }
        
        $dql = "SELECT v.navigator,COUNT(v.navigator) as val FROM $entityName v GROUP BY v.navigator ORDER BY v.navigator";
        $query = $em->createQuery($dql);
        $items = $query->getResult();
        
        if($items){
            foreach($items as $item){
                array_push($ObjList->browserInfo,$item);
            }
        }
        
        $dql = "SELECT v.device,COUNT(v.device) as val FROM $entityName v GROUP BY v.device ORDER BY v.device";
        $query = $em->createQuery($dql);
        $items = $query->getResult();
        
        if($items){
            foreach($items as $item){
                array_push($ObjList->deviceInfo,$item);
            }
        }
        
        $ObjList->today = 0;
        $ObjList->month = 0;
        $ObjList->year = 0;
        
        $dql = "SELECT COUNT(v) as val FROM $entityName v WHERE ((DAY(v.dateOfVisit) = DAY(NOW())) AND (MONTH(v.dateOfVisit) = MONTH(NOW())))";
        $query = $em->createQuery($dql);
        
        $item = $query->getSingleResult();
        if($item){
            $ObjList->today = $item['val'];
        }
        
        $dql = "SELECT COUNT(v) as val FROM $entityName v WHERE MONTH(v.dateOfVisit) = MONTH(NOW())";
        $query = $em->createQuery($dql);
        
        $item = $query->getSingleResult();
        if($item){
            $ObjList->month = $item['val'];
        }
        
        $dql = "SELECT COUNT(v) as val FROM $entityName v WHERE YEAR(v.dateOfVisit) = YEAR(NOW())";
        $query = $em->createQuery($dql);
        
        $item = $query->getSingleResult();
        if($item){
            $ObjList->year = $item['val'];
        }
        
        $ObjList->message= '';

        $response = new JsonResponse($ObjList);
        
        return $response;
	}

	/**
	 * Create database schema.
	 * @Route("/manage/createDbSchema", name="app_create_db")
	 */
	public function createDbSchemaAction(){
		
		$em = $this->getDoctrine()->getManager();
		
		$connection = $em->getConnection();
		$params = $connection->getParams();
		
        $name = isset($params['path']) ? $params['path'] : $params['dbname'];

        unset($params['dbname']);
		
        $tmpConnection = DriverManager::getConnection($params);

        // Only quote if we don't have a path
        if (!isset($params['path'])) {
            $name = $tmpConnection->getDatabasePlatform()->quoteSingleIdentifier($name);
        }

		$error = false;
        try {
            
			$tmpConnection->getSchemaManager()->createDatabase($name);
			$metadatas = $em->getMetadataFactory()->getAllMetadata();

			if (!empty($metadatas)) {
				// Create SchemaTool
				$schemaTool = new \Doctrine\ORM\Tools\SchemaTool($em);
				$schemaTool->createSchema($metadatas);
				//$sqls = $schemaTool->getCreateSchemaSql($metadatas);
			}		
			
            //echo sprintf('Created database for connection named %s', $name);
			
        } catch (\Exception $e) {
			
			$error = true;
			$ObjErr = new \stdClass();
			$ObjErr->fname = __FUNCTION__;
			$ObjErr->success = false;
			$ObjErr->message = $e->getMessage();
			$ObjErr->exception = new \stdClass();
			$ObjErr->exception->code = $e->getCode();
			$ObjErr->exception->file = $e->getFile();
			$ObjErr->exception->line = $e->getLine();
			$ObjErr->exception->trace = $e->getTraceAsString();
			echo json_encode($ObjErr);	

        }

        $tmpConnection->close();
		
		if($error) return new Response(''); 
		return new Response('{success:true}');
		
	}	

	/**
	 * Update database schema.
	 * @Route("/manage/updateDbSchema", name="app_update_db")
	 */
	public function updateDbSchemaAction(){
		
        // Defining if update is complete or not (--complete not defined means $saveMode = true)
        $saveMode = true;
		
		$em = $this->getDoctrine()->getManager();

        $error = false;
        try {
            
			$metadatas = $em->getMetadataFactory()->getAllMetadata();

			if(!empty($metadatas)) {
				// Create SchemaTool
				$schemaTool = new \Doctrine\ORM\Tools\SchemaTool($em);
				$schemaTool->updateSchema($metadatas, $saveMode);
				//$sqls = $schemaTool->getUpdateSchemaSql($metadatas, $saveMode);
			}		
			
            return new Response('{success:true}');
			
        } catch (\Exception $e) {

			$ObjErr = new \stdClass();
			$ObjErr->fname = __FUNCTION__;
			$ObjErr->success = false;
			$ObjErr->message = $e->getMessage();
			$ObjErr->exception = new \stdClass();
			$ObjErr->exception->code = $e->getCode();
			$ObjErr->exception->file = $e->getFile();
			$ObjErr->exception->line = $e->getLine();
			$ObjErr->exception->trace = $e->getTraceAsString();
			
			return new Response(json_encode($ObjErr));	

        }

	}

	/**
	 * Update Symfony 2 assets.
	 * @Route("/manage/updateAssets", name="app_update_assets")
	 */
	public function updateAssetsAction(){


        $filesystem = $this->container->get('filesystem');
        $kernel = $this->container->get('kernel');

		$targetArg = '../web';
		
        // Create the bundles directory otherwise symlink will fail.
        $bundlesDir = $targetArg.'/bundles/';
		
		try {

			$filesystem->mkdir($bundlesDir, 0777);

			foreach ($kernel->getBundles() as $bundle) {
				if (is_dir($originDir = $bundle->getPath().'/Resources/public')) {
					$targetDir  = $bundlesDir.preg_replace('/bundle$/', '', strtolower($bundle->getName()));

					//echo sprintf('Installing assets for %s into %s<br/>', $bundle->getNamespace(), $targetDir);

					$filesystem->remove($targetDir);
					$filesystem->mkdir($targetDir, 0777);
					// We use a custom iterator to ignore VCS files
					$filesystem->mirror($originDir, $targetDir, Finder::create()->ignoreDotFiles(false)->in($originDir));
						
				}
			}
		
        } catch (\Exception $e) {

			$ObjErr = new \stdClass();
			$ObjErr->fname = __FUNCTION__;
			$ObjErr->success = false;
			$ObjErr->message = $e->getMessage();
			$ObjErr->exception = new \stdClass();
			$ObjErr->exception->code = $e->getCode();
			$ObjErr->exception->file = $e->getFile();
			$ObjErr->exception->line = $e->getLine();
			$ObjErr->exception->trace = $e->getTraceAsString();
			
			return new Response(json_encode($ObjErr));	

        }

		return new Response('{success:true}');
	}

	/**
	 * Security initialization.
	 * @Route("/setupAdmin", name="setup_admin") 
	 */
	public function setupAdminAction()
	{

		$em = $this->getDoctrine()->getManager();
		$user = $em->getRepository('Admin\CoreBundle\Entity\User')->findOneByUsername('admin');

		$changed = false;
		$userChanged = false;
		
		if (!$user){

			$user = new User();
			$user->setUsername('admin');
			$user->setFullName('System Admin');
			$user->setEmail('admin@localhost.loc');
			$user->setPassword('s20a123/*');
			$user->setPhotoPath('user.png');
			$changed = true;
			$userChanged = true;

		}

		$role1 = $em->getRepository('Admin\CoreBundle\Entity\Role')->findOneByRole('ROLE_USER');
		if(!$role1){
			$role1 = new Role();
			$role1->setRole('ROLE_USER');
			$em->persist($role1);
			$changed = true;
		}

		$role2 = $em->getRepository('Admin\CoreBundle\Entity\Role')->findOneByRole('ROLE_ADMIN');
		if(!$role2){
			$role2 = new Role();
			$role2->setRole('ROLE_ADMIN');
			$em->persist($role2);
			$changed = true;
		}

		$role3 = $em->getRepository('Admin\CoreBundle\Entity\Role')->findOneByRole('ROLE_SUPER_ADMIN');
		if(!$role3){
			$role3 = new Role();
			$role3->setRole('ROLE_SUPER_ADMIN');
			$em->persist($role3);
			$changed = true;
		}

		if($userChanged){
			$user->addRole($role3);
			$em->persist($user);
		}

		if($changed){
			$em->flush();
		}

		$response = new Response();
		$response->setContent('ok');
		$response->setStatusCode(200);
		$response->headers->set('Content-Type','text/html');

		return $response;

	}
	
}
