<?php

namespace Admin\CoreBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use Admin\CoreBundle\Entity\Role;
use Admin\CoreBundle\Form\RoleType;


/**
 * RoleController
 * 
 */
class RoleController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all Role .
	 * @Route("/manage/admin/corebundle/role/list", name="admin_corebundle_role_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			//$entities = $em->getRepository('Admin\CoreBundle\Entity\Role')->findAll();
		
			$repo = $em->getRepository('Admin\CoreBundle\Entity\Role');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'R');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);

			$entities = $query->getResult();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= $res['Total'];
			$ObjList->message= '';
			$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AdminCoreBundle:Role:list.html.twig",$this->webconf);
	}

	/**
	 * Create Role .
	 * @Route("/manage/admin/corebundle/role/create", name="admin_corebundle_role_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new Role();



		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_role_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_role_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_role_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AdminCoreBundle:Role:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity Role.
	 * @Route("/manage/admin/corebundle/role/edit/{id}",requirements={"id" = "\d+"},name="admin_corebundle_role_edit") 
	 */
	public function editAction($id)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('Admin\CoreBundle\Entity\Role')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find Role width $id.");
		}



		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:Role:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing Role entity.
	 * @Route("/manage/admin/corebundle/role/update/{id}",requirements={"id" = "\d+"},name="admin_corebundle_role_update") 
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('Admin\CoreBundle\Entity\Role')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Role width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {


			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_role_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_role_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_role_edit',array('id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AdminCoreBundle:Role:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete Role .
	 * @Route("/manage/admin/corebundle/role/delete/{id}", name="admin_corebundle_role_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('Admin\CoreBundle\Entity\Role')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Role width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('admin_corebundle_role_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:Role:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete Role list.
	 * @Route("/manage/admin/corebundle/role/delete_list", name="admin_corebundle_role_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE Admin\CoreBundle\Entity\Role R WHERE R.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('admin_corebundle_role_list'));
	}

	/**
	 * Creates a form to delete a Role entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('admin_corebundle_role_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a Role entity.
	 *
	 * @param Role $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Role $entity)
	{
		$form = $this->createForm(new RoleType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_role_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormRole'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Role $entity)
	{
		$form = $this->createForm(new RoleType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_role_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormRole'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}

}


?>