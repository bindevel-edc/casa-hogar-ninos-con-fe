<?php

namespace Admin\CoreBundle\Controller;

use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SecurityController extends Controller
{

	/**
	 * Lists all User .
	 * @Route("/manage/login", name="manage_login")
	 */
	public function loginAction()
	{
    
        /*
		$request = $this->getRequest();
		$session = $request->getSession();

		if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
			$error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
		} else {
			$error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
		}

		//$response = new RedirectResponse('http://example.com/');
		//$response-send();	
        */
        
        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();        
		
		return $this->render('AdminCoreBundle:Security:login.html.twig', array(
			'last_username' => $lastUsername,
			'error'			=> $error,
		));
	}

	/**
	 * @Route("/manage/login_check", name="manage_login_check")
	 */
	public function loginCheckAction()
	{
        //throw new \RuntimeException('loginCheckAction() should never be called.');
        //return new Response("");
	}

	/**
	 * @Route("/manage/logout", name="manage_logout")
	 */
	public function logoutAction()
	{

	}

}