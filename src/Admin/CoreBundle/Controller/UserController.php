<?php

namespace Admin\CoreBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use Admin\CoreBundle\Entity\User;
use Admin\CoreBundle\Form\UserType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Admin\CoreBundle\Entity\Role;


/**
 * UserController
 * 
 */
class UserController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all User .
	 * @Route("/manage/admin/corebundle/user/list", name="admin_corebundle_user_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			//$entities = $em->getRepository('Admin\CoreBundle\Entity\User')->findAll();
		
			$repo = $em->getRepository('Admin\CoreBundle\Entity\User');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'U');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);

			$entities = $query->getResult();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= $res['Total'];
			$ObjList->message= '';
			$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AdminCoreBundle:User:list.html.twig",$this->webconf);
	}

	/**
	 * Create User .
	 * @Route("/manage/admin/corebundle/user/create", name="admin_corebundle_user_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new User();

		$validator = $this->get('validator');
		$metadata = $validator->getMetadataFor($entity);
		$metadata->addPropertyConstraint('photo', new NotBlank());


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_user_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_user_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_user_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AdminCoreBundle:User:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity User.
	 * @Route("/manage/admin/corebundle/user/edit/{id}",requirements={"id" = "\d+"},name="admin_corebundle_user_edit") 
	 */
	public function editAction($id)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('Admin\CoreBundle\Entity\User')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find User width $id.");
		}



		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:User:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing User entity.
	 * @Route("/manage/admin/corebundle/user/update/{id}",requirements={"id" = "\d+"},name="admin_corebundle_user_update") 
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('Admin\CoreBundle\Entity\User')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find User width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {


			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('admin_corebundle_user_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('admin_corebundle_user_create'));
			}

			return $this->redirect($this->generateUrl('admin_corebundle_user_edit',array('id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AdminCoreBundle:User:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete User .
	 * @Route("/manage/admin/corebundle/user/delete/{id}", name="admin_corebundle_user_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('Admin\CoreBundle\Entity\User')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find User width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('admin_corebundle_user_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AdminCoreBundle:User:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete User list.
	 * @Route("/manage/admin/corebundle/user/delete_list", name="admin_corebundle_user_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE Admin\CoreBundle\Entity\User U WHERE U.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('admin_corebundle_user_list'));
	}

	/**
	 * Creates a form to delete a User entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('admin_corebundle_user_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a User entity.
	 *
	 * @param User $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(User $entity)
	{
		$form = $this->createForm(new UserType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_user_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormUser'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(User $entity)
	{
		$form = $this->createForm(new UserType(), $entity, array(
			'action' => $this->generateUrl('admin_corebundle_user_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormUser'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}

}


?>