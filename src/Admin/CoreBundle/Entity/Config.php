<?php

namespace Admin\CoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Gedmo\Translatable\Translatable;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Config
 *
 * @ORM\Table(name="Config")
 * @ORM\Entity()
 */
class Config implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $varname;

	/**
	 * @var text
	 *
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true, unique=false)
	*/
	protected $value;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $type;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * Config Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->varname = '';
		$this->value = '';
		$this->type = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set varname
	 *
	 * @param string $varname
	 * @return Config
	*/
	public function setVarname($varname){
		if($this->varname !== $varname){
			$this->varname=$varname;
		}
		return $this;
	}

	/**
	 * Get varname
	 *
	 * @return string
	*/
	public function getVarname(){
		return $this->varname;
	}

	/**
	 * Set value
	 *
	 * @param text $value
	 * @return Config
	*/
	public function setValue($value){
		if($this->value !== $value){
			$this->value=$value;
		}
		return $this;
	}

	/**
	 * Get value
	 *
	 * @return text
	*/
	public function getValue(){
		return $this->value;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return Config
	*/
	public function setType($type){
		if($this->type !== $type){
			$this->type=$type;
		}
		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	*/
	public function getType(){
		return $this->type;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->varname = $this->varname;
		$obj->value = $this->value;
		$obj->type = $this->type;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->varname;
	}

}
?>