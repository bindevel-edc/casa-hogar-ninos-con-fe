<?php

namespace Admin\CoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Role
 *
 * @ORM\Table(name="AdminCoreBundleRole")
 * @ORM\Entity()
 */
class Role implements RoleInterface {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $role;

	/**
	 * Role Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->role = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set role
	 *
	 * @param string $role
	 * @return Role
	*/
	public function setRole($role){
		if($this->role !== $role){
			$this->role = $role;
		}
		return $this;
	}

	/**
	 * Get role
	 *
	 * @return string
	*/
	public function getRole(){
		return $this->role;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->role = $this->role;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->role;
	}

}
?>