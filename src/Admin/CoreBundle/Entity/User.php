<?php

namespace Admin\CoreBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * User
 *
 * @ORM\Table(name="AdminCoreBundleUser")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface, \Serializable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $username;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(name="fullName",type="string",length=255, nullable=false, unique=false)
	*/
	protected $fullName;

	/**
	 * @var string
	 *
	 * @Assert\Email(checkMX=false,checkHost=false)
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $email;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $password;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $salt;

	/**
	 * @ORM\ManyToMany(targetEntity="Role" )
	 * @ORM\JoinTable(name="AdminCoreBundleUser_AdminCoreBundleRole")
	 *
	*/
	protected $roles;

	/**
	 * @var UploadedFile
	 *
	*/
	protected $photo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="photoPath",type="string", length=255 , nullable=false, unique=false)
	*/
	protected $photoPath;

	/**
	 * User Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->username = '';
		$this->fullName = '';
		$this->email = '';
		$this->password = '';
		$this->salt = '';
		$this->roles = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set username
	 *
	 * @param string $username
	 * @return User
	*/
	public function setUsername($username){
		if($this->username !== $username){
			$this->username = $username;
		}
		return $this;
	}

	/**
	 * Get username
	 *
	 * @return string
	*/
	public function getUsername(){
		return $this->username;
	}

	/**
	 * Set fullName
	 *
	 * @param string $fullName
	 * @return User
	*/
	public function setFullName($fullName){
		if($this->fullName !== $fullName){
			$this->fullName = $fullName;
		}
		return $this;
	}

	/**
	 * Get fullName
	 *
	 * @return string
	*/
	public function getFullName(){
		return $this->fullName;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return User
	*/
	public function setEmail($email){
		if($this->email !== $email){
			$this->email = $email;
		}
		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	*/
	public function getEmail(){
		return $this->email;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 * @return User
	*/
	public function setPassword($password){
		if($this->password !== $password){
			$this->setSalt(md5(time()));
			$encoder = new MessageDigestPasswordEncoder('sha512',true,10);
			$encrPass = $encoder->encodePassword($password, $this->getSalt());	
			$this->password = $encrPass;
		}
		return $this;
	}

	/**
	 * Get password
	 *
	 * @return string
	*/
	public function getPassword(){
		return $this->password;
	}

	/**
	 * Set salt
	 *
	 * @param string $salt
	 * @return User
	*/
	public function setSalt($salt){
		if($this->salt !== $salt){
			$this->salt = $salt;
		}
		return $this;
	}

	/**
	 * Get salt
	 *
	 * @return string
	*/
	public function getSalt(){
		return $this->salt;
	}

	/**
	 * Add new Role to collection
	 *
	 * @param \Admin\CoreBundle\Entity\Role $role
	 * @return User
	*/
	public function addRole(\Admin\CoreBundle\Entity\Role $role)
	{
		$this->roles[] = $role;
		return $this;
	}

	/**
	 * Remove a item from collection
	 *
	 * @param \Admin\CoreBundle\Entity\Role $role
	*/
	public function removeRole(\Admin\CoreBundle\Entity\Role $role)
	{
		$this->roles->removeElement($role);
	}

	/**
	 * Set roles collection
	 *
	 * @param \Doctrine\Common\Collections\Collection $role
	 * @return User
	*/
	public function setRoles(\Doctrine\Common\Collections\Collection $roles){
		if($this->roles != $roles){
			$this->roles = $roles;
		}
		return $this;
	}

	/**
	 * Get roles
	 *
	 * @return \Doctrine\Common\Collections\Collection Role
	*/
	public function getRoles()
	{
		return $this->roles->toArray();
	}

	/**
	 * Sets photo.
	 *
	 * @param UploadedFile $photo
	*/
	public function setPhoto(UploadedFile $photo = null)
	{
		// check if we have an old photo path
		if (is_file($this->getAbsolutePhotoPath())) {
			// store the old photo to delete after the update
			$this->_tempPhoto = $this->getAbsolutePhotoPath();
		}

		$this->photo = $photo;
		$this->photoPath = 'initial';
	}

	/**
	 * Get photo.
	 *
	 * @return UploadedFile
	*/
	public function getPhoto()
	{
		return $this->photo;
	}

	/**
	 * Set photo path.
	 *
	 * @param string photo
	*/
	public function setPhotoPath($photoPath)
	{
		$this->photoPath = $photoPath;
	}

	/**
	 * Get photo path.
	 *
	 * @return string
	*/
	public function getPhotoPath()
	{
		return $this->photoPath;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	*/
	public function preUploadPhoto()
	{
		if($this->photo == null) return;
		if (null !== $this->photoPath) {
			$filename = sha1(uniqid(mt_rand(), true));
			$this->photoPath = $filename.'.'.$this->photo->getClientOriginalExtension();
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	*/
	public function uploadPhoto()
	{
		if (null === $this->photo) {
			return;
		}

		$this->photo->move($this->getUploadPhotoDir(), $this->photoPath);
		if (isset($this->_tempPhoto)) {
			// delete the old photo
				unlink($this->_tempPhoto);
				// clear the temp photo path
				$this->_tempPhoto = null;
		}
		unset($this->photo);
	}

	/**
	 * @ORM\PostRemove()
	*/
	public function removeUploadPhoto()
	{
		if($photo = $this->getAbsolutePhotoPath()){
			unlink($photo);
		}
	}

	public function getPhotoWebPath()
	{
		return null === $this->photoPath ? null : '/'.$this->getUploadPhotoBaseDir().'/'.$this->photoPath;
	}

	public function getAbsolutePhotoPath()
	{
		return null === $this->photoPath ? null : $this->getUploadPhotoDir().'/'.$this->photoPath;
	}

	public function getUploadPhotoDir()
	{
		$path = __DIR__.'/../../../../web/'.$this->getUploadPhotoBaseDir();
		if(!file_exists($path)){
			if(!@mkdir($path,0700,true)) return null;
		}
		return $path;
	}

	public function getUploadPhotoBaseDir()
	{
		return 'Admin/CoreBundle/User/photo';
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->username = $this->username;
		$obj->fullName = $this->fullName;
		$obj->email = $this->email;
		$obj->password = $this->password;
		$obj->salt = $this->salt;
		$obj->roles = '...';
		$obj->photo = $this->photo;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->username;
	}
	/* Extra methods*/


	/**
	 * Erase credentials
	 *
	 * @return null
	 */
	public function eraseCredentials() {

	}

	/**
	 * serialize
	 *
	 * @return serialize
	 */
	public function serialize()
	{
		return serialize($this->getId());
	}

	/**
	 * unserialize
	 *
	 * @param $data
	 */
	public function unserialize($data)
	{
		$this->id = unserialize($data);
	}


	protected function mergePasswordAndSalt($password, $salt)
	{
		if (empty($salt)) {
			return $password;
		}

		if (false !== strrpos($salt, '{') || false !== strrpos($salt, '}')) {
			throw new \InvalidArgumentException('Cannot use { or } in salt.');
		}

		return $password.'{'.$salt.'}';
	}


	public function encodePassword($raw, $salt)
	{


		$salted = $this->mergePasswordAndSalt($raw, $salt);
		$digest = hash($this->algorithm, $salted, true);

		// "stretch" hash
		for ($i = 1; $i < $this->iterations; $i++) {
			$digest = hash($this->algorithm, $digest.$salted, true);
		}

		return $this->encodeHashAsBase64 ? base64_encode($digest) : bin2hex($digest);
	}

}
?>