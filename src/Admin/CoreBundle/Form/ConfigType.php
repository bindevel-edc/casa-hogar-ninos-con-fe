<?php

namespace Admin\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConfigType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$config = $builder->getData();
		$varType = $config->getType();
		$getVarName= $config->getVarname();

		$builder
			->add('varname', 'text', array(
				'attr' => array(
					'id' => 'Idvarname'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('type','choice',array(
				'label' => 'Tipo'
				,'choices' => array(
					'text'=>'text',
					'textarea'=>'textarea',
					'html'=>'html',
					'email'=>'email',
					'integer'=>'integer',
					'number'=>'number',
					'password'=>'password',
					'percent'=>'percent',
					'search'=>'search',
					'url'=>'url',
					'timezone'=>'timezone',
					/*
					'template'=>'template',
					'date'=>'date',
					'datetime'=>'datetime',
					'time'=>'time',
					'birthday'=>'birthday',
					*/
					'checkbox'=>'checkbox'
				)				
			))
			;


		if($varType){
			
			if($varType == 'checkbox'){
				$config->setValue((bool)$config->getValue());
			} else if ($varType == 'integer'){
				$config->setValue((int)$config->getValue());
			} else if ($varType == 'percent'){
				$config->setValue((int)$config->getValue());
			} else if ($varType == 'number'){
				$config->setValue((int)$config->getValue());
			}
			
			//$formMapper->add('value',$varType,array('label' => 'Valor','required' => false));

            if($varType == 'html'){
            
                $builder->add('value','textarea', array(
                    'attr' => array(
                        'id' => 'Idvalue'
                        ,'data-validation' => 'true'
                        ,'class' => 'HTMLEditor'
                    )
                    ,'label' => 'Valor'
                    ,'required' => false
                ));
                
            } else {

                $config->setValue($config->getValue());
                
                if($varType == 'text'){
                  $value = strip_tags($config->getValue());
                  $config->setValue($value);
                }

                
                $builder->add('value',$varType, array(
                    'attr' => array(
                        'id' => 'Idvalue'
                        ,'data-validation' => 'true'
                    )
                    ,'label' => 'Valor'
                    ,'required' => false
                ));
                
            }
			
		} else {
			$builder->add('value','text',array('label' => 'Valor','required' => false));
		}
			/*
			->add('type', 'text', array(
				'attr' => array(
					'id' => 'Idtype'
					,'data-validation' => 'true'
				)
				,'label' => 'Tipo'
				,'required' => true
				,'max_length' => 255
			))
			*/


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Admin\CoreBundle\Entity\Config'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'admin_corebundle_config';
	}
}
