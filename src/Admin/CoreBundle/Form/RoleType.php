<?php

namespace Admin\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class RoleType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('role', 'text', array(
				'attr' => array(
					'id' => 'Idrole'
					,'data-validation' => 'true'
				)
				,'label' => 'Rol'
				,'required' => true
				,'max_length' => 255
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Admin\CoreBundle\Entity\Role'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'admin_corebundle_role';
	}
}
