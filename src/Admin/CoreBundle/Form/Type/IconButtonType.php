<?php

namespace Admin\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;

class IconButtonType extends SubmitType
{

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['icon'] = $options['icon'];
        $view->vars['mapped'] = false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('icon'));
    }

    public function getParent()
    {
        return ButtonType::class;
    }

    public function getName()
    {
        return 'icon_btn';
    }
}

?>