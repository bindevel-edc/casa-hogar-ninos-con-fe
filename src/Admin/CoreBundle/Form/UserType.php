<?php

namespace Admin\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class UserType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('username', 'text', array(
				'attr' => array(
					'id' => 'Idusername'
					,'data-validation' => 'true'
				)
				,'label' => 'Usuario'
				,'required' => true
				,'max_length' => 255
			))

			->add('fullName', 'text', array(
				'attr' => array(
					'id' => 'IdfullName'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre completo'
				,'required' => true
				,'max_length' => 255
			))

			->add('email', 'text', array(
				'attr' => array(
					'id' => 'Idemail'
					,'data-validation' => 'true'
				)
				,'label' => 'Correo'
				,'required' => true
				,'max_length' => 255
			))

			->add('password', 'password', array(
				'attr' => array(
					'id' => 'Idpassword'
					,'data-validation' => 'true'
				)
				,'label' => 'Contraseña'
				,'required' => true
				,'max_length' => 255
			))

			->add('roles', 'entity', array(
				'attr' => array(
					'id' => 'Idroles'
					,'data-validation' => 'true'
				)
				,'label' => 'Roles'
				,'class' => 'Admin\CoreBundle\Entity\Role'
				,'property' => 'role'
				,'multiple' => true
				//,'expanded' => true
			))

			->add('photo', 'file', array(
				'attr' => array(
					'id' => 'Idphoto'
					,'data-validation' => 'true'
				)
				,'label' => 'Foto'
				,'required' => true
				,'image_path' => 'photoWebPath'
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Admin\CoreBundle\Entity\User'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'admin_corebundle_user';
	}
}
