<?php
namespace Admin\CoreBundle\Helpers;
use Doctrine\ORM\EntityManager;

/**
 * DQLFilter
 * 
 */
class DQLFilter
{
	public static function buildDQLFilter(EntityManager $em,$entityName,$alias){
	
		// collect request parameters
		$start  = isset($_REQUEST['start'])  ? $_REQUEST['start']  :  0;
		$count  = isset($_REQUEST['limit'])  ? $_REQUEST['limit']  : 10;
		$sort   = isset($_REQUEST['sort'])   ? $_REQUEST['sort']   : '';
		$dir    = isset($_REQUEST['dir'])    ? $_REQUEST['dir']    : 'ASC';
		$filters = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : null;

		if($sort == ''){
			if(property_exists($entityName, 'position')){
				$sort = 'position';
			}
		} else {
			if(!property_exists($entityName,$sort)){
				$sort = '';
			}
		}
		
		// GridFilters sends filters as an Array if not json encoded
		if (is_array($filters)) {
			$encoded = false;
		} else {
			$encoded = true;
			$filters = json_decode($filters);
		}

		// initialize variables
		$where = '';
		$qs = '';

		// loop through filters sent by client
		if (is_array($filters)) {
			for ($i=0;$i<count($filters);$i++){
				$filter = $filters[$i];

				// assign filter data (location depends if encoded or not)
				if ($encoded) {
					$field = $filter->field;
					$value = $filter->value;
					$compare = isset($filter->comparison) ? $filter->comparison : null;
					$filterType = $filter->type;
				} else {
					$field = $filter['field'];
					$value = $filter['data']['value'];
					$compare = isset($filter['data']['comparison']) ? $filter['data']['comparison'] : null;
					$filterType = $filter['data']['type'];
				}

				switch($filterType){
					case 'string' : $qs .= " AND $alias.".$field." LIKE '%".$value."%'"; Break;
					case 'list' :
						if (strstr($value,',')){
							$fi = explode(',',$value);
							for ($q=0;$q<count($fi);$q++){
								$fi[$q] = "'".$fi[$q]."'";
							}
							$value = implode(',',$fi);
							$qs .= " AND $alias.".$field." IN (".$value.")";
						}else{
							$qs .= " AND $alias.".$field." = '".$value."'";
						}
					Break;
					case 'boolean' : $qs .= " AND $alias.".$field." = ".($value); Break;
					case 'numeric' :
						switch ($compare) {
							case 'eq' : $qs .= " AND $alias.".$field." = ".$value; Break;
							case 'lt' : $qs .= " AND $alias.".$field." < ".$value; Break;
							case 'gt' : $qs .= " AND $alias.".$field." > ".$value; Break;
						}
					Break;
					case 'date' :
						switch ($compare) {
							case 'eq' : $qs .= " AND $alias.".$field." = '".date('Y-m-d',strtotime($value))."'"; Break;
							case 'lt' : $qs .= " AND $alias.".$field." < '".date('Y-m-d',strtotime($value))."'"; Break;
							case 'gt' : $qs .= " AND $alias.".$field." > '".date('Y-m-d',strtotime($value))."'"; Break;
						}
					Break;
				}
			}
			$where .= $qs;
		}

		// query the database
		if($where != ""){
			$where = substr($where,4,strlen($where));
			$where = " WHERE $where";
		}
		
		$query = "SELECT $alias FROM $entityName $alias ".$where;
		$queryCount = "SELECT COUNT($alias) Total FROM $entityName $alias ".$where;
		
		if ($sort != "") {
			$query .= " ORDER BY $alias.".$sort." ".$dir;
			$queryCount .= " ORDER BY $alias.".$sort." ".$dir;
		}
		
		$q = $em->createQuery($query)
			->setFirstResult( $start)
			->setMaxResults($count);
		
		$q1 = $em->createQuery($queryCount);

		$queries = array();
		array_push($queries,$q);
		array_push($queries,$q1);
		
		return $queries;
	}

}