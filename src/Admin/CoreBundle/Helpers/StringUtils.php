<?php
namespace Admin\CoreBundle\Helpers;

class StringUtils {

	static public function getSlug($str, $separador = '-')
	{

		$patterns = array();
		$replacements = array();
		$patterns[0] = '/á/';
		$replacements[0] = 'a';
		$patterns[1] = '/é/';
		$replacements[1] = 'e';
		$patterns[2] = '/í/';
		$replacements[2] = 'i';
		$patterns[3] = '/ó/';
		$replacements[3] = 'o';
		$patterns[4] = '/ú/';
		$replacements[4] = 'u';

		$patterns[5] = '/&aacute;/';
		$replacements[5] = 'a';
		$patterns[6] = '/&eacute;/';
		$replacements[6] = 'e';
		$patterns[7] = '/&iacute;/';
		$replacements[7] = 'i';
		$patterns[8] = '/&oacute;/';
		$replacements[8] = 'o';
		$patterns[9] = '/&uacute;/';
		$replacements[9] = 'u';

		//Código copiado de http://cubiq.org/the-perfect-php-clean-url-generator
		$slug = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$slug = preg_replace($patterns,$replacements,$slug);
		$slug = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $slug);
		$slug = strtolower(trim($slug, $separador));
		$slug = preg_replace("/[\/_|+ -]+/", $separador, $slug);

		return $slug;
	}
	
	static public function dateDiffStr($start_date,$end_date){
	
		//$end_date = new DateTime($tm);
		$since_start = $start_date->diff($end_date);

		$text = ' Hace ';
		if($since_start->y > 0){
			$text .=$since_start->y ;
			$text .= ($since_start->y > 1) ? ' años ' : ' año ';
		}
			
		if($since_start->m > 0){
			$text .=$since_start->m; 
			$text .=($since_start->m > 1) ? ' meses ' : ' mes ';
		}	
		
		if($since_start->d > 0){
			$text .=$since_start->d; 
			$text .= ($since_start->d > 1) ? ' dias ' : ' día ';
		}
			
		if($since_start->h > 0){
			$text .=$since_start->h;
			$text .= ($since_start->h > 1) ? ' horas ' : ' hora ';
		}
		
        /*
		if($since_start->i > 0){
			$text .=$since_start->i;
			$text .= ($since_start->i > 1) ? ' minutos ' : ' minuto ';
		}
			
		if($since_start->s > 0){
			$text .=$since_start->s;
			$text .= ($since_start->s > 1) ? ' segundos ' : ' segundo ';
		}
        */

		return $text;
	}

}

?>