<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\Banner;
use App\BackendBundle\Form\BannerType;
use Symfony\Component\Validator\Constraints\NotBlank;


/**
 * BannerController
 * 
 */
class BannerController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all Banner .
	 * @Route("/manage/app/backendbundle/banner/list", name="app_backendbundle_banner_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\Banner')->findAll();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			$ObjList->message= '';

		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:Banner:list.html.twig",$this->webconf);
	}

	/**
	 * Create Banner .
	 * @Route("/manage/app/backendbundle/banner/create", name="app_backendbundle_banner_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new Banner();
		$entity->setTranslatableLocale('es');
		$validator = $this->get('validator');
		$metadata = $validator->getMetadataFor($entity);
		$metadata->addPropertyConstraint('photo', new NotBlank());


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_banner_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_banner_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:Banner:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity Banner.
	 * @Route("/manage/app/backendbundle/banner/edit/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_banner_edit") 
	 */
	public function editAction($id,$_locale)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find Banner width $id.");
		}

		$entity->setTranslatableLocale($_locale);
		$em->refresh($entity);


		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:Banner:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing Banner entity.
	 * @Route("/manage/app/backendbundle/banner/update/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_banner_update") 
	 */
	public function updateAction(Request $request, $id,$_locale)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Banner width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {

			$entity->setTranslatableLocale($_locale);

			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_banner_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_banner_edit',array('_locale'=>$_locale,'id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:Banner:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete Banner .
	 * @Route("/manage/app/backendbundle/banner/delete/{id}", name="app_backendbundle_banner_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Banner width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:Banner:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete Banner list.
	 * @Route("/manage/app/backendbundle/banner/delete_list", name="app_backendbundle_banner_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\Banner B WHERE B.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
	}

	/**
	 * Creates a form to delete a Banner entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_banner_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Banner $entity)
	{
		$form = $this->createForm(new BannerType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_banner_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormBanner'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Banner $entity)
	{
		$form = $this->createForm(new BannerType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_banner_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormBanner'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up Banner .
	 * @Route("/manage/app/backendbundle/banner/moveUp/{id}", name="app_backendbundle_banner_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Banner width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
	}

	/**
	 * Move down Banner .
	 * @Route("/manage/app/backendbundle/banner/moveDown/{id}", name="app_backendbundle_banner_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Banner width $id.');
			}

			$dql = "SELECT MAX(B.position) AS maxValue FROM App\BackendBundle\Entity\Banner B ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
	}

	/**
	 * Move Banner to top.
	 * @Route("/manage/app/backendbundle/banner/moveToTop/{id}", name="app_backendbundle_banner_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Banner width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
	}

	/**
	 * Move Banner bottom.
	 * @Route("/manage/app/backendbundle/banner/moveToBottom/{id}", name="app_backendbundle_banner_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Banner')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Banner width $id.');
			}

			$dql = "SELECT MAX(B.position) AS maxValue FROM App\BackendBundle\Entity\Banner B ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_banner_list'));
	}
}


?>