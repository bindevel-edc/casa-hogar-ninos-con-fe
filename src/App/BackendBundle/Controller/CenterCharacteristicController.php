<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\CenterCharacteristic;
use App\BackendBundle\Form\CenterCharacteristicType;


/**
 * CenterCharacteristicController
 * 
 */
class CenterCharacteristicController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all CenterCharacteristic .
	 * @Route("/manage/app/backendbundle/centercharacteristic/list", name="app_backendbundle_centercharacteristic_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {

			$entities = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->findAll();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			$ObjList->message= '';
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:CenterCharacteristic:list.html.twig",$this->webconf);
	}

	/**
	 * Create CenterCharacteristic .
	 * @Route("/manage/app/backendbundle/centercharacteristic/create", name="app_backendbundle_centercharacteristic_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new CenterCharacteristic();
		$entity->setTranslatableLocale('es');


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:CenterCharacteristic:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity CenterCharacteristic.
	 * @Route("/manage/app/backendbundle/centercharacteristic/edit/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_centercharacteristic_edit") 
	 */
	public function editAction($id,$_locale)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find CenterCharacteristic width $id.");
		}

		$entity->setTranslatableLocale($_locale);
		$em->refresh($entity);


		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:CenterCharacteristic:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing CenterCharacteristic entity.
	 * @Route("/manage/app/backendbundle/centercharacteristic/update/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_centercharacteristic_update") 
	 */
	public function updateAction(Request $request, $id,$_locale)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find CenterCharacteristic width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {

			$entity->setTranslatableLocale($_locale);

			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_edit',array('_locale'=>$_locale,'id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:CenterCharacteristic:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete CenterCharacteristic .
	 * @Route("/manage/app/backendbundle/centercharacteristic/delete/{id}", name="app_backendbundle_centercharacteristic_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find CenterCharacteristic width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:CenterCharacteristic:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete CenterCharacteristic list.
	 * @Route("/manage/app/backendbundle/centercharacteristic/delete_list", name="app_backendbundle_centercharacteristic_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\CenterCharacteristic C WHERE C.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
	}

	/**
	 * Creates a form to delete a CenterCharacteristic entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_centercharacteristic_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a CenterCharacteristic entity.
	 *
	 * @param CenterCharacteristic $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(CenterCharacteristic $entity)
	{
		$form = $this->createForm(new CenterCharacteristicType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_centercharacteristic_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormCenterCharacteristic'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(CenterCharacteristic $entity)
	{
		$form = $this->createForm(new CenterCharacteristicType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_centercharacteristic_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormCenterCharacteristic'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up CenterCharacteristic .
	 * @Route("/manage/app/backendbundle/centercharacteristic/moveUp/{id}", name="app_backendbundle_centercharacteristic_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find CenterCharacteristic width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
	}

	/**
	 * Move down CenterCharacteristic .
	 * @Route("/manage/app/backendbundle/centercharacteristic/moveDown/{id}", name="app_backendbundle_centercharacteristic_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find CenterCharacteristic width $id.');
			}

			$dql = "SELECT MAX(C.position) AS maxValue FROM App\BackendBundle\Entity\CenterCharacteristic C ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
	}

	/**
	 * Move CenterCharacteristic to top.
	 * @Route("/manage/app/backendbundle/centercharacteristic/moveToTop/{id}", name="app_backendbundle_centercharacteristic_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find CenterCharacteristic width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
	}

	/**
	 * Move CenterCharacteristic bottom.
	 * @Route("/manage/app/backendbundle/centercharacteristic/moveToBottom/{id}", name="app_backendbundle_centercharacteristic_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\CenterCharacteristic')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find CenterCharacteristic width $id.');
			}

			$dql = "SELECT MAX(C.position) AS maxValue FROM App\BackendBundle\Entity\CenterCharacteristic C ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_centercharacteristic_list'));
	}
}


?>