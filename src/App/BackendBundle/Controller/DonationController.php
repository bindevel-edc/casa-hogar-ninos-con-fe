<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\Donation;



/**
 * DonationController
 * 
 */
class DonationController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all Donation .
	 * @Route("/manage/app/backendbundle/donation/list", name="app_backendbundle_donation_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\Donation')->findAll();
		
            /*
			$repo = $em->getRepository('App\BackendBundle\Entity\Donation');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'D');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);

			$entities = $query->getResult();
            */

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			//$ObjList->total= $res['Total'];
			$ObjList->message= '';
			//$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:Donation:list.html.twig",$this->webconf);
	}

}


?>