<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\DonationCurrencies;
use App\BackendBundle\Form\DonationCurrenciesType;


/**
 * DonationCurrenciesController
 * 
 */
class DonationCurrenciesController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all DonationCurrencies .
	 * @Route("/manage/app/backendbundle/donationcurrencies/list", name="app_backendbundle_donationcurrencies_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->findAll();
		
            /*
			$repo = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'D');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);

			$entities = $query->getResult();
            */

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			//$ObjList->total= $res['Total'];
			$ObjList->message= '';
			//$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:DonationCurrencies:list.html.twig",$this->webconf);
	}

	/**
	 * Create DonationCurrencies .
	 * @Route("/manage/app/backendbundle/donationcurrencies/create", name="app_backendbundle_donationcurrencies_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new DonationCurrencies();



		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:DonationCurrencies:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity DonationCurrencies.
	 * @Route("/manage/app/backendbundle/donationcurrencies/edit/{id}",requirements={"id" = "\d+"},name="app_backendbundle_donationcurrencies_edit") 
	 */
	public function editAction($id)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find DonationCurrencies width $id.");
		}



		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:DonationCurrencies:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing DonationCurrencies entity.
	 * @Route("/manage/app/backendbundle/donationcurrencies/update/{id}",requirements={"id" = "\d+"},name="app_backendbundle_donationcurrencies_update") 
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find DonationCurrencies width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {


			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_edit',array('id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:DonationCurrencies:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete DonationCurrencies .
	 * @Route("/manage/app/backendbundle/donationcurrencies/delete/{id}", name="app_backendbundle_donationcurrencies_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationCurrencies width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:DonationCurrencies:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete DonationCurrencies list.
	 * @Route("/manage/app/backendbundle/donationcurrencies/delete_list", name="app_backendbundle_donationcurrencies_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\DonationCurrencies D WHERE D.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
	}

	/**
	 * Creates a form to delete a DonationCurrencies entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_donationcurrencies_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a DonationCurrencies entity.
	 *
	 * @param DonationCurrencies $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(DonationCurrencies $entity)
	{
		$form = $this->createForm(new DonationCurrenciesType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_donationcurrencies_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormDonationCurrencies'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(DonationCurrencies $entity)
	{
		$form = $this->createForm(new DonationCurrenciesType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_donationcurrencies_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormDonationCurrencies'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up DonationCurrencies .
	 * @Route("/manage/app/backendbundle/donationcurrencies/moveUp/{id}", name="app_backendbundle_donationcurrencies_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationCurrencies width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
	}

	/**
	 * Move down DonationCurrencies .
	 * @Route("/manage/app/backendbundle/donationcurrencies/moveDown/{id}", name="app_backendbundle_donationcurrencies_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationCurrencies width $id.');
			}

			$dql = "SELECT MAX(D.position) AS maxValue FROM App\BackendBundle\Entity\DonationCurrencies D ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
	}

	/**
	 * Move DonationCurrencies to top.
	 * @Route("/manage/app/backendbundle/donationcurrencies/moveToTop/{id}", name="app_backendbundle_donationcurrencies_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationCurrencies width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
	}

	/**
	 * Move DonationCurrencies bottom.
	 * @Route("/manage/app/backendbundle/donationcurrencies/moveToBottom/{id}", name="app_backendbundle_donationcurrencies_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationCurrencies')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationCurrencies width $id.');
			}

			$dql = "SELECT MAX(D.position) AS maxValue FROM App\BackendBundle\Entity\DonationCurrencies D ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationcurrencies_list'));
	}
}


?>