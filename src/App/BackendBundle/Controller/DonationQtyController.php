<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\DonationQty;
use App\BackendBundle\Form\DonationQtyType;


/**
 * DonationQtyController
 * 
 */
class DonationQtyController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all DonationQty .
	 * @Route("/manage/app/backendbundle/donationqty/list", name="app_backendbundle_donationqty_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\DonationQty')->findAll();
		
            /*
			$repo = $em->getRepository('App\BackendBundle\Entity\DonationQty');		
			$entityName = $repo->getClassName();

			$queries = DQLFilter::buildDQLFilter($em,$entityName,'D');
			
			$query = $queries[0];
			$dql = $query->getDql();
			
			$queryCount = $queries[1];
			$res = $queryCount->getSingleResult();
			unset($queryCount);

			$entities = $query->getResult();
            */

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			//$ObjList->total= $res['Total'];
			$ObjList->message= '';
			//$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:DonationQty:list.html.twig",$this->webconf);
	}

	/**
	 * Create DonationQty .
	 * @Route("/manage/app/backendbundle/donationqty/create", name="app_backendbundle_donationqty_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new DonationQty();



		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_donationqty_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:DonationQty:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity DonationQty.
	 * @Route("/manage/app/backendbundle/donationqty/edit/{id}",requirements={"id" = "\d+"},name="app_backendbundle_donationqty_edit") 
	 */
	public function editAction($id)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find DonationQty width $id.");
		}



		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:DonationQty:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing DonationQty entity.
	 * @Route("/manage/app/backendbundle/donationqty/update/{id}",requirements={"id" = "\d+"},name="app_backendbundle_donationqty_update") 
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find DonationQty width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {


			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_donationqty_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_edit',array('id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:DonationQty:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete DonationQty .
	 * @Route("/manage/app/backendbundle/donationqty/delete/{id}", name="app_backendbundle_donationqty_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationQty width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:DonationQty:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete DonationQty list.
	 * @Route("/manage/app/backendbundle/donationqty/delete_list", name="app_backendbundle_donationqty_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\DonationQty D WHERE D.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
	}

	/**
	 * Creates a form to delete a DonationQty entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_donationqty_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a DonationQty entity.
	 *
	 * @param DonationQty $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(DonationQty $entity)
	{
		$form = $this->createForm(new DonationQtyType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_donationqty_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormDonationQty'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(DonationQty $entity)
	{
		$form = $this->createForm(new DonationQtyType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_donationqty_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormDonationQty'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up DonationQty .
	 * @Route("/manage/app/backendbundle/donationqty/moveUp/{id}", name="app_backendbundle_donationqty_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationQty width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
	}

	/**
	 * Move down DonationQty .
	 * @Route("/manage/app/backendbundle/donationqty/moveDown/{id}", name="app_backendbundle_donationqty_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationQty width $id.');
			}

			$dql = "SELECT MAX(D.position) AS maxValue FROM App\BackendBundle\Entity\DonationQty D ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
	}

	/**
	 * Move DonationQty to top.
	 * @Route("/manage/app/backendbundle/donationqty/moveToTop/{id}", name="app_backendbundle_donationqty_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationQty width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
	}

	/**
	 * Move DonationQty bottom.
	 * @Route("/manage/app/backendbundle/donationqty/moveToBottom/{id}", name="app_backendbundle_donationqty_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\DonationQty')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find DonationQty width $id.');
			}

			$dql = "SELECT MAX(D.position) AS maxValue FROM App\BackendBundle\Entity\DonationQty D ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_donationqty_list'));
	}
}


?>