<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\RequirementsCatalog;
use App\BackendBundle\Form\RequirementsCatalogType;
use App\BackendBundle\Entity\Kid;


/**
 * RequirementsCatalogController
 * 
 */
class RequirementsCatalogController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all RequirementsCatalog .
	 * @Route("/manage/app/backendbundle/requirementscatalog/list", name="app_backendbundle_requirementscatalog_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->findAll();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			$ObjList->message= '';

		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:RequirementsCatalog:list.html.twig",$this->webconf);
	}

	/**
	 * Create RequirementsCatalog .
	 * @Route("/manage/app/backendbundle/requirementscatalog/create", name="app_backendbundle_requirementscatalog_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new RequirementsCatalog();
		$entity->setTranslatableLocale('es');


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:RequirementsCatalog:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity RequirementsCatalog.
	 * @Route("/manage/app/backendbundle/requirementscatalog/edit/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_requirementscatalog_edit") 
	 */
	public function editAction($id,$_locale)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find RequirementsCatalog width $id.");
		}

		$entity->setTranslatableLocale($_locale);
		$em->refresh($entity);


		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:RequirementsCatalog:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing RequirementsCatalog entity.
	 * @Route("/manage/app/backendbundle/requirementscatalog/update/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="app_backendbundle_requirementscatalog_update") 
	 */
	public function updateAction(Request $request, $id,$_locale)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find RequirementsCatalog width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {

			$entity->setTranslatableLocale($_locale);

			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_edit',array('_locale'=>$_locale,'id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:RequirementsCatalog:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete RequirementsCatalog .
	 * @Route("/manage/app/backendbundle/requirementscatalog/delete/{id}", name="app_backendbundle_requirementscatalog_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find RequirementsCatalog width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:RequirementsCatalog:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete RequirementsCatalog list.
	 * @Route("/manage/app/backendbundle/requirementscatalog/delete_list", name="app_backendbundle_requirementscatalog_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\RequirementsCatalog R WHERE R.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
	}

	/**
	 * Creates a form to delete a RequirementsCatalog entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_requirementscatalog_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a RequirementsCatalog entity.
	 *
	 * @param RequirementsCatalog $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(RequirementsCatalog $entity)
	{
		$form = $this->createForm(new RequirementsCatalogType(true), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_requirementscatalog_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormRequirementsCatalog'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(RequirementsCatalog $entity)
	{
		$form = $this->createForm(new RequirementsCatalogType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_requirementscatalog_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormRequirementsCatalog'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up RequirementsCatalog .
	 * @Route("/manage/app/backendbundle/requirementscatalog/moveUp/{id}", name="app_backendbundle_requirementscatalog_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find RequirementsCatalog width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
	}

	/**
	 * Move down RequirementsCatalog .
	 * @Route("/manage/app/backendbundle/requirementscatalog/moveDown/{id}", name="app_backendbundle_requirementscatalog_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find RequirementsCatalog width $id.');
			}

			$dql = "SELECT MAX(R.position) AS maxValue FROM App\BackendBundle\Entity\RequirementsCatalog R ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
	}

	/**
	 * Move RequirementsCatalog to top.
	 * @Route("/manage/app/backendbundle/requirementscatalog/moveToTop/{id}", name="app_backendbundle_requirementscatalog_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find RequirementsCatalog width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
	}

	/**
	 * Move RequirementsCatalog bottom.
	 * @Route("/manage/app/backendbundle/requirementscatalog/moveToBottom/{id}", name="app_backendbundle_requirementscatalog_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\RequirementsCatalog')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find RequirementsCatalog width $id.');
			}

			$dql = "SELECT MAX(R.position) AS maxValue FROM App\BackendBundle\Entity\RequirementsCatalog R ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_requirementscatalog_list'));
	}
}


?>