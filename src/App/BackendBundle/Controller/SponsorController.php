<?php

namespace App\BackendBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Admin\CoreBundle\Helpers\DQLFilter;
use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\Sponsor;
use App\BackendBundle\Form\SponsorType;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\BackendBundle\Entity\Kid;


/**
 * SponsorController
 * 
 */
class SponsorController extends Controller
{


	/*
	
	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
		$this->loadConf();
	}

	public function loadConf(){
		$this->webconf=array();
	}
	
	*/

	function __construct() {
		$this->webconf=array();
	}

	/**
	 * Lists all Sponsor .
	 * @Route("/manage/app/backendbundle/sponsor/list", name="app_backendbundle_sponsor_list")
	 */
	public function listAction()
	{
	
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\Sponsor')->findAll();

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			$ObjList->message= '';

		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppBackendBundle:Sponsor:list.html.twig",$this->webconf);
	}

	/**
	 * Create Sponsor .
	 * @Route("/manage/app/backendbundle/sponsor/create", name="app_backendbundle_sponsor_create")
	 */
	public function createAction(Request $request)
	{

		$entity = new Sponsor();

		$validator = $this->get('validator');
		$metadata = $validator->getMetadataFor($entity);
		$metadata->addPropertyConstraint('photo', new NotBlank());


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			if ($form->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
			} else if($form->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_sponsor_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_edit',array('id'=>$entity->getId())));
		}
		
		

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppBackendBundle:Sponsor:create.html.twig",$this->webconf);

	}

	/**
	 * Create a edit for the entity Sponsor.
	 * @Route("/manage/app/backendbundle/sponsor/edit/{id}",requirements={"id" = "\d+"},name="app_backendbundle_sponsor_edit") 
	 */
	public function editAction($id)
	{

		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException("Unable to find Sponsor width $id.");
		}



		$editForm = $this->createEditForm($entity);
		$deleteForm = $this->createDeleteForm($id);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:Sponsor:edit.html.twig",$this->webconf);

	}

	/**
	 * Edits an existing Sponsor entity.
	 * @Route("/manage/app/backendbundle/sponsor/update/{id}",requirements={"id" = "\d+"},name="app_backendbundle_sponsor_update") 
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Sponsor width id:$id.');
		}

		$entity2 = clone $entity;
		
		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {


			$em->flush();

			if ($editForm->get('saveAndReturnToList')->isClicked()) {
				return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
			} else if($editForm->get('saveAndAddNew')->isClicked()){
				return $this->redirect($this->generateUrl('app_backendbundle_sponsor_create'));
			}

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_edit',array('id'=>$id)));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		$this->webconf['delete_form'] = $deleteForm->createView();
		
		return $this->render("AppBackendBundle:Sponsor:edit.html.twig",$this->webconf);

	}

	/**
	 * Delete Sponsor .
	 * @Route("/manage/app/backendbundle/sponsor/delete/{id}", name="app_backendbundle_sponsor_delete")  
	 */
	public function deleteAction(Request $request, $id)
	{
	
		$deleteForm = $this->createDeleteForm($id);
		$deleteForm->handleRequest($request);

		if ($deleteForm->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Sponsor width $id.');
			}

			$em->remove($entity);
			$em->flush();
			
			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
		}

		$this->webconf['id'] = $id;
		$this->webconf['delete_form'] = $deleteForm->createView();

		return $this->render("AppBackendBundle:Sponsor:delete.html.twig",$this->webconf);
	}

	/**
	 * Delete Sponsor list.
	 * @Route("/manage/app/backendbundle/sponsor/delete_list", name="app_backendbundle_sponsor_delete_list")  
	 * @Method({"POST"})
	 */
	public function deleteListAction(Request $request)
	{

		if(isset($_POST['delete_item'])){
			if(is_array($_POST['delete_item'])){
				
				$arr = array();
				foreach($_POST['delete_item'] as $key=>$value){
					if(preg_match("/\d+/",$value)) $arr[] = $value;
				}

				$list = implode(',',$arr);

				if(count($arr) > 0){
					$em = $this->getDoctrine()->getManager();
					$dql = "DELETE App\BackendBundle\Entity\Sponsor S WHERE S.id in ($list)";
					$query = $em->createQuery($dql);
					$query->execute();
				}
			}
		}

		return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
	}

	/**
	 * Creates a form to delete a Sponsor entity by id.
	 *
	 * @param mixed $id The entity id
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($id)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('app_backendbundle_sponsor_delete', array('id' => $id)))
			->setMethod('POST')
			->add('id', 'hidden', array('attr' => array('id'=>$id)))
			->getForm()
		;
	}

	/**
	 * Creates a form to edit a Sponsor entity.
	 *
	 * @param Sponsor $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Sponsor $entity)
	{
		$form = $this->createForm(new SponsorType(true), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_sponsor_update', array('id' => $entity->getId())),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormSponsor'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Guardar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Sponsor $entity)
	{
		$form = $this->createForm(new SponsorType(), $entity, array(
			'action' => $this->generateUrl('app_backendbundle_sponsor_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormSponsor'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Crear',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndReturnToList', new IconButtonType(), array(
			'label' => 'Guardar y listar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('saveAndAddNew', new IconButtonType(), array(
			'label' => 'Guardar y crear uno nuevo',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		$form->add('cancel', new IconButtonType(), array(
			'label' => 'Cancelar',
			'attr' => array(
				'class' => 'btn btn-icon btn-danger'
				,'data-command' => 'goBack'
			),
			'icon' => 'fa fa-ban',
		));
		
		return $form;
	}


	/**
	 * Move up Sponsor .
	 * @Route("/manage/app/backendbundle/sponsor/moveUp/{id}", name="app_backendbundle_sponsor_move_up")  
	 */
	public function moveUpAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Sponsor width $id.');
			}
			
			$pos = $entity->getPosition();
			if($pos > 0) {
				$pos--;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
	}

	/**
	 * Move down Sponsor .
	 * @Route("/manage/app/backendbundle/sponsor/moveDown/{id}", name="app_backendbundle_sponsor_move_down")  
	 */
	public function moveDownAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Sponsor width $id.');
			}

			$dql = "SELECT MAX(S.position) AS maxValue FROM App\BackendBundle\Entity\Sponsor S ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$pos++;
				$entity->setPosition($pos);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
	}

	/**
	 * Move Sponsor to top.
	 * @Route("/manage/app/backendbundle/sponsor/moveToTop/{id}", name="app_backendbundle_sponsor_moveto_top")  
	 */
	public function moveToTopAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Sponsor width $id.');
			}

			$entity->setPosition(0);
			$pos = $entity->getPosition();
			$em->flush();

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
	}

	/**
	 * Move Sponsor bottom.
	 * @Route("/manage/app/backendbundle/sponsor/moveToBottom/{id}", name="app_backendbundle_sponsor_moveto_bottom")  
	 */
	public function moveToBottomAction(Request $request, $id)
	{

			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Sponsor width $id.');
			}

			$dql = "SELECT MAX(S.position) AS maxValue FROM App\BackendBundle\Entity\Sponsor S ";
			$max = $em->createQuery($dql)->getSingleScalarResult();

			$pos = $entity->getPosition();
			if($pos < $max) {
				$entity->setPosition($max);
				$em->flush();
			}

			return $this->redirect($this->generateUrl('app_backendbundle_sponsor_list'));
	}
}


?>