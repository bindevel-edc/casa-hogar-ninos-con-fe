<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Banner
 *
 * @ORM\Table(name="AppBackendBundleBanner")
 * @ORM\Entity(repositoryClass="App\BackendBundle\Entity\BannerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Banner implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $name;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $title;

	/**
	 * @var UploadedFile
	 * @Assert\Image()
	 *
	*/
	protected $photo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="photoPath",type="string", length=255 , nullable=false, unique=false)
	*/
	protected $photoPath;

	/**
	 * @var integer
	 *
	 * @Gedmo\SortablePosition
	 * @ORM\Column(type="integer", nullable=true, unique=false)
	*/
	protected $position;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * Banner Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->name = '';
		$this->title = '';
		$this->position = 0;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Banner
	*/
	public function setName($name){
		if($this->name !== $name){
			$this->name = $name;
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getName(){
		return $this->name;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Banner
	*/
	public function setTitle($title){
		if($this->title !== $title){
			$this->title = $title;
		}
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	*/
	public function getTitle(){
		return $this->title;
	}

	/**
	 * Sets photo.
	 *
	 * @param UploadedFile $photo
	*/
	public function setPhoto(UploadedFile $photo = null)
	{
		// check if we have an old photo path
		if (is_file($this->getAbsolutePhotoPath())) {
			// store the old photo to delete after the update
			$this->_tempPhoto = $this->getAbsolutePhotoPath();
		}

		$this->photo = $photo;
		$this->photoPath = 'initial';
	}

	/**
	 * Get photo.
	 *
	 * @return UploadedFile
	*/
	public function getPhoto()
	{
		return $this->photo;
	}

	/**
	 * Set photo path.
	 *
	 * @param string photo
	*/
	public function setPhotoPath($photoPath)
	{
		$this->photoPath = $photoPath;
	}

	/**
	 * Get photo path.
	 *
	 * @return string
	*/
	public function getPhotoPath()
	{
		return $this->photoPath;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	*/
	public function preUploadPhoto()
	{
		if($this->photo == null) return;
		if (null !== $this->photoPath) {
			$filename = sha1(uniqid(mt_rand(), true));
			$this->photoPath = $filename.'.'.$this->photo->getClientOriginalExtension();
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	*/
	public function uploadPhoto()
	{
		if (null === $this->photo) {
			return;
		}

		$this->photo->move($this->getUploadPhotoDir(), $this->photoPath);
		if (isset($this->_tempPhoto)) {
				// delete the old photo
				if(file_exists($this->_tempPhoto)){
					unlink($this->_tempPhoto);
				}
				// clear the temp photo path
				$this->_tempPhoto = null;
		}
		unset($this->photo);
	}

	/**
	 * @ORM\PostRemove()
	*/
	public function removeUploadPhoto()
	{
		if($photo = $this->getAbsolutePhotoPath()){
			if(file_exists($path)){
				unlink($photo);
			}
		}
	}

	public function getPhotoWebPath()
	{
		return null === $this->photoPath ? null : '/'.$this->getUploadPhotoBaseDir().'/'.$this->photoPath;
	}

	public function getAbsolutePhotoPath()
	{
		return null === $this->photoPath ? null : $this->getUploadPhotoDir().'/'.$this->photoPath;
	}

	public function getUploadPhotoDir()
	{
		$path = __DIR__.'/../../../../web/'.$this->getUploadPhotoBaseDir();
		if(!file_exists($path)){
			if(!@mkdir($path,0700,true)) return null;
		}
		return $path;
	}

	public function getUploadPhotoBaseDir()
	{
		return 'App/BackendBundle/Banner/photo';
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return Banner
	*/
	public function setPosition($position){
		if($this->position !== $position){
			$this->position = $position;
		}
		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer
	*/
	public function getPosition(){
		return $this->position;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->name = $this->name;
		$obj->title = $this->title;
		$obj->photo = $this->photo;
		$obj->position = $this->position;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->name;
	}

}
?>