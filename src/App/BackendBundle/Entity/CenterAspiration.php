<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * CenterAspiration
 *
 * @ORM\Table(name="AppBackendBundleCenterAspiration")
 * @ORM\Entity()
 */
class CenterAspiration implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $title;

	/**
	 * @var text
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $description;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * CenterAspiration Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->title = '';
		$this->description = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return CenterAspiration
	*/
	public function setTitle($title){
		if($this->title !== $title){
			$this->title = $title;
		}
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	*/
	public function getTitle(){
		return $this->title;
	}

	/**
	 * Set description
	 *
	 * @param text $description
	 * @return CenterAspiration
	*/
	public function setDescription($description){
		if($this->description !== $description){
			$this->description = $description;
		}
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return text
	*/
	public function getDescription(){
		return $this->description;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->title = $this->title;
		$obj->description = $this->description;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->title;
	}

}
?>