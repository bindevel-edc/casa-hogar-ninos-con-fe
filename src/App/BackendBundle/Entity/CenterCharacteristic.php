<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * CenterCharacteristic
 *
 * @ORM\Table(name="AppBackendBundleCenterCharacteristic")
 * @ORM\Entity()
 */
class CenterCharacteristic implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $title;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $icon;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $content;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * CenterCharacteristic Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->title = '';
		$this->icon = '';
		$this->content = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return CenterCharacteristic
	*/
	public function setTitle($title){
		if($this->title !== $title){
			$this->title = $title;
		}
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	*/
	public function getTitle(){
		return $this->title;
	}

	/**
	 * Set icon
	 *
	 * @param string $icon
	 * @return CenterCharacteristic
	*/
	public function setIcon($icon){
		if($this->icon !== $icon){
			$this->icon = $icon;
		}
		return $this;
	}

	/**
	 * Get icon
	 *
	 * @return string
	*/
	public function getIcon(){
		return $this->icon;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 * @return CenterCharacteristic
	*/
	public function setContent($content){
		if($this->content !== $content){
			$this->content = $content;
		}
		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	*/
	public function getContent(){
		return $this->content;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->title = $this->title;
		$obj->icon = $this->icon;
		$obj->content = $this->content;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->title;
	}

}
?>