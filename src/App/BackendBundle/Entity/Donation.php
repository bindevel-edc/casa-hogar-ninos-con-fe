<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Donation
 *
 * @ORM\Table(name="AppBackendBundleDonation")
 * @ORM\Entity()
 */
class Donation {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @ORM\ManyToOne(targetEntity="Sponsor")
	 *
	*/
	protected $user;

	/**
	 * @var integer
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="integer", nullable=false, unique=false)
	*/
	protected $amount;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $currency;

	/**
	 * @var date
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="date", nullable=false, unique=false)
	*/
	protected $dateOf;

	/**
	 * Donation Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->amount = 0;
		$this->currency = '';
		$this->dateOf = new \DateTime('NOW');
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set user
	 *
	 * @param \App\BackendBundle\Entity\Sponsor $sponsor
	 * @return Donation
	*/
	public function setUser(\App\BackendBundle\Entity\Sponsor $sponsor = null){
		if($this->user != $sponsor){
			$this->user = $sponsor;
		}
		return $this;
	}

	/**
	 * Get user
	 *
	 * @return \App\BackendBundle\Entity\Sponsor
	*/
	public function getUser(){
		return $this->user;
	}

	/**
	 * Set amount
	 *
	 * @param integer $amount
	 * @return Donation
	*/
	public function setAmount($amount){
		if($this->amount !== $amount){
			$this->amount = $amount;
		}
		return $this;
	}

	/**
	 * Get amount
	 *
	 * @return integer
	*/
	public function getAmount(){
		return $this->amount;
	}

	/**
	 * Set currency
	 *
	 * @param string $currency
	 * @return Donation
	*/
	public function setCurrency($currency){
		if($this->currency !== $currency){
			$this->currency = $currency;
		}
		return $this;
	}

	/**
	 * Get currency
	 *
	 * @return string
	*/
	public function getCurrency(){
		return $this->currency;
	}

	/**
	 * Set dateOf
	 *
	 * @param date $dateOf
	 * @return Donation
	*/
	public function setDateOf($dateOf){
		if($this->dateOf !== $dateOf){
			$this->dateOf = $dateOf;
		}
		return $this;
	}

	/**
	 * Get dateOf
	 *
	 * @return date
	*/
	public function getDateOf(){
		return $this->dateOf;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;

		if($this->user != null){
			$obj->user = (string) $this->user;
			$obj->idUser = $this->user->getId();
		} else {
			$obj->user = '-';
			$obj->idUser = -1;
		}

		$obj->amount = $this->amount;
		$obj->currency = $this->currency;
		$theDate = $this->dateOf->format('d/m/Y');
		$obj->dateOf = $theDate;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return (string) $this->user;
	}

}
?>