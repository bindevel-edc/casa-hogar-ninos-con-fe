<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DonationCurrencies
 *
 * @ORM\Table(name="AppBackendBundleDonationCurrencies")
 * @ORM\Entity()
 */
class DonationCurrencies {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $type;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $label;

	/**
	 * DonationCurrencies Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->type = '';
		$this->label = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set type
	 *
	 * @param string $type
	 * @return DonationCurrencies
	*/
	public function setType($type){
		if($this->type !== $type){
			$this->type = $type;
		}
		return $this;
	}

	/**
	 * Get type
	 *
	 * @return string
	*/
	public function getType(){
		return $this->type;
	}

	/**
	 * Set label
	 *
	 * @param string $label
	 * @return DonationCurrencies
	*/
	public function setLabel($label){
		if($this->label !== $label){
			$this->label = $label;
		}
		return $this;
	}

	/**
	 * Get label
	 *
	 * @return string
	*/
	public function getLabel(){
		return $this->label;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->type = $this->type;
		$obj->label = $this->label;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->type;
	}

}
?>