<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * DonationQty
 *
 * @ORM\Table(name="AppBackendBundleDonationQty")
 * @ORM\Entity()
 */
class DonationQty {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var float
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="float", nullable=false, unique=false)
	*/
	protected $value;

	/**
	 * DonationQty Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->value = 0;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set value
	 *
	 * @param float $value
	 * @return DonationQty
	*/
	public function setValue($value){
		if($this->value !== $value){
			$this->value = $value;
		}
		return $this;
	}

	/**
	 * Get value
	 *
	 * @return float
	*/
	public function getValue(){
		return $this->value;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->value = $this->value;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return (string) $this->value;
	}

}
?>