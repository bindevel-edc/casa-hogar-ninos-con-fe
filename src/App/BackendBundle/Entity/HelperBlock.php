<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * HelperBlock
 *
 * @ORM\Table(name="AppBackendBundleHelperBlock")
 * @ORM\Entity()
 */
class HelperBlock implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $title;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $content;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $subtitle;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=255 , nullable=false, unique=false)
	*/
	protected $photo;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * HelperBlock Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->title = '';
		$this->content = '';
		$this->subtitle = '';
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return HelperBlock
	*/
	public function setTitle($title){
		if($this->title !== $title){
			$this->title = $title;
		}
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	*/
	public function getTitle(){
		return $this->title;
	}

	/**
	 * Set content
	 *
	 * @param string $content
	 * @return HelperBlock
	*/
	public function setContent($content){
		if($this->content !== $content){
			$this->content = $content;
		}
		return $this;
	}

	/**
	 * Get content
	 *
	 * @return string
	*/
	public function getContent(){
		return $this->content;
	}

	/**
	 * Set subtitle
	 *
	 * @param string $subtitle
	 * @return HelperBlock
	*/
	public function setSubtitle($subtitle){
		if($this->subtitle !== $subtitle){
			$this->subtitle = $subtitle;
		}
		return $this;
	}

	/**
	 * Get subtitle
	 *
	 * @return string
	*/
	public function getSubtitle(){
		return $this->subtitle;
	}

	/**
	 * Set photo
	 *
	 * @param image $photo
	 * @return HelperBlock
	*/
	public function setPhoto($photo){
		if($this->photo !== $photo){
			$this->photo = $photo;
		}
		return $this;
	}

	/**
	 * Get photo
	 *
	 * @return image
	*/
	public function getPhoto(){
		return $this->photo;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->title = $this->title;
		$obj->content = $this->content;
		$obj->subtitle = $this->subtitle;
		$obj->photo = $this->photo;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->title;
	}

}
?>