<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Admin\CoreBundle\Helpers\StringUtils;

/**
 * Kid
 *
 * @ORM\Table(name="AppBackendBundleKid")
 * @ORM\Entity()
 */
class Kid implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $name;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(name="introText",type="string",length=255, nullable=false, unique=false)
	*/
	protected $introText;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", length=255 , nullable=false, unique=false)
	*/
	protected $photo;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $gallery;

	/**
	 * @var date
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="date", nullable=false, unique=false)
	*/
	protected $birthdate;

	/**
	 * @var text
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="text",length=255, nullable=false, unique=false)
	*/
	protected $biography;

	/**
	 * @var integer
	 *
	*/
	protected $old;

	/**
	 * @var date
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(name="dateOfReception",type="date", nullable=false, unique=false)
	*/
	protected $dateOfReception;

	/**
	 * @var string
	 *
	*/
	protected $timeInTheHouse;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $ailments;

	/**
	 * @ORM\ManyToMany(targetEntity="RequirementsCatalog", mappedBy="kids", cascade={"persist", "remove"})
	 *
	*/
	protected $requirements;

	/**
	 * @ORM\ManyToMany(targetEntity="Sponsor", mappedBy="kids", cascade={"persist", "remove"})
	 *
	*/
	protected $sponsors;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $slug;
	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * Kid Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->slug = '';
		$this->name = '';
		$this->gallery = '-';
		$this->birthdate = new \DateTime('NOW');
		$this->biography = '';
		$this->old = 0;
		$this->dateOfReception = new \DateTime('NOW');
		//$this->timeInTheHouse = '';
		$this->timeInTheHouse = StringUtils::dateDiffStr(new \DateTime('NOW'),$this->dateOfReception);
		$this->ailments = '-';
		$this->requirements = new ArrayCollection();
		$this->sponsors = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Kid
	*/
	public function setName($name){
		if($this->name !== $name){
			$this->name = $name;
			$this->setSlug($name);
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getName(){
		return $this->name;
	}

	/**
	 * Set introText
	 *
	 * @param string $name
	 * @return Kid
	*/
	public function setIntroText($introText){
		if($this->introText !== $introText){
			$this->introText = $introText;
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getIntroText(){
		return $this->introText;
	}

	/**
	 * Set photo
	 *
	 * @param image $photo
	 * @return Kid
	*/
	public function setPhoto($photo){
		if($this->photo !== $photo){
			$this->photo = $photo;
		}
		return $this;
	}

	/**
	 * Get photo
	 *
	 * @return image
	*/
	public function getPhoto(){
		return $this->photo;
	}

	/**
	 * Set gallery
	 *
	 * @param string $gallery
	 * @return Kid
	*/
	public function setGallery($gallery){
		if($this->gallery !== $gallery){
			$this->gallery = $gallery;
		}
		return $this;
	}

	/**
	 * Get gallery
	 *
	 * @return string
	*/
	public function getGallery(){
		return $this->gallery;
	}

	/**
	 * Set birthdate
	 *
	 * @param date $birthdate
	 * @return Kid
	*/
	public function setBirthdate($birthdate){
    
		if($this->birthdate !== $birthdate){
			$this->birthdate = $birthdate;            
		}

		return $this;
	}

	/**
	 * Get birthdate
	 *
	 * @return date
	*/
	public function getBirthdate(){
		return $this->birthdate;
	}

	/**
	 * Set biography
	 *
	 * @param text $biography
	 * @return Kid
	*/
	public function setBiography($biography){
		if($this->biography !== $biography){
			$this->biography = $biography;
		}
		return $this;
	}

	/**
	 * Get biography
	 *
	 * @return text
	*/
	public function getBiography(){
		return $this->biography;
	}

	/**
	 * Set old
	 *
	 * @param integer $old
	 * @return Kid
	*/
	public function setOld($old){
		if($this->old !== $old){
			$this->old = $old;
		}
		return $this;
	}

	/**
	 * Get old
	 *
	 * @return integer
	*/
	public function getOld(){
    
        $currentY = new \DateTime('NOW');
        $y1 = (int) $this->birthdate->format('Y');
        $y2 = (int) $currentY->format('Y');
        $this->old = $y2 - $y1; 
        
		return $this->old;
	}

	/**
	 * Set dateOfReception
	 *
	 * @param date $dateOfReception
	 * @return Kid
	*/
	public function setDateOfReception($dateOfReception){
		if($this->dateOfReception !== $dateOfReception){
			$this->dateOfReception = $dateOfReception;
		}
		return $this;
	}

	/**
	 * Get dateOfReception
	 *
	 * @return date
	*/
	public function getDateOfReception(){
		return $this->dateOfReception;
	}

	/**
	 * Set timeInTheHouse
	 *
	 * @param string $timeInTheHouse
	 * @return Kid
	*/
	public function setTimeInTheHouse($timeInTheHouse){
		if($this->timeInTheHouse !== $timeInTheHouse){
			$this->timeInTheHouse = $timeInTheHouse;
		}
		return $this;
	}

	/**
	 * Get timeInTheHouse
	 *
	 * @return string
	*/
	public function getTimeInTheHouse(){
    
        $this->timeInTheHouse = StringUtils::dateDiffStr(new \DateTime('NOW'),$this->dateOfReception);
    
		return $this->timeInTheHouse;
	}

	/**
	 * Set ailments
	 *
	 * @param string $ailments
	 * @return Kid
	*/
	public function setAilments($ailments){
		if($this->ailments !== $ailments){
			$this->ailments = $ailments;
		}
		return $this;
	}

	/**
	 * Get ailments
	 *
	 * @return string
	*/
	public function getAilments(){
		return $this->ailments;
	}

	/**
	 * Add new RequirementsCatalog to collection
	 *
	 * @param \App\BackendBundle\Entity\RequirementsCatalog $requirementscatalog
	 * @return Kid
	*/
	public function addRequirement(\App\BackendBundle\Entity\RequirementsCatalog $requirementscatalog)
	{

		if(!$this->requirements->contains($requirementscatalog)){
			$this->requirements->add($requirementscatalog);
			$requirementscatalog->addKid($this);
		}
         
		return $this;
	}

	/**
	 * Remove a item from collection
	 *
	 * @param \App\BackendBundle\Entity\RequirementsCatalog $requirementscatalog
	*/
	public function removeRequirement(\App\BackendBundle\Entity\RequirementsCatalog $requirementscatalog)
	{
		$this->requirements->removeElement($requirementscatalog);
	}

	/**
	 * Set requirements collection
	 *
	 * @param \Doctrine\Common\Collections\Collection $requirementscatalog
	 * @return Kid
	*/
	public function setRequirements(\Doctrine\Common\Collections\Collection $requirements){
		if($this->requirements != $requirements){
			$this->requirements = $requirements;

			foreach($requirements as $requirementscatalog) {
				$requirementscatalog->addKid($this);
			}
		}
		return $this;
	}

	/**
	 * Get requirements
	 *
	 * @return \Doctrine\Common\Collections\Collection RequirementsCatalog
	*/
	public function getRequirements()
	{
		return $this->requirements;
	}

	/**
	 * Add new Sponsor to collection
	 *
	 * @param \App\BackendBundle\Entity\Sponsor $sponsor
	 * @return Kid
	*/
	public function addSponsor(\App\BackendBundle\Entity\Sponsor $sponsor)
	{
		if(!$this->sponsors->contains($sponsor)){
			$this->sponsors->add($sponsor);
			$sponsor->addKid($this);
		}
		return $this;
	}

	/**
	 * Remove a item from collection
	 *
	 * @param \App\BackendBundle\Entity\Sponsor $sponsor
	*/
	public function removeSponsor(\App\BackendBundle\Entity\Sponsor $sponsor)
	{
		$this->sponsors->removeElement($sponsor);
	}

	/**
	 * Set sponsors collection
	 *
	 * @param \Doctrine\Common\Collections\Collection $sponsor
	 * @return Kid
	*/
	public function setSponsors(\Doctrine\Common\Collections\Collection $sponsors){
		if($this->sponsors != $sponsors){
			$this->sponsors = $sponsors;

			foreach($sponsors as $sponsor) {
				$sponsor->addKid($this);
			}
		}
		return $this;
	}

	/**
	 * Get sponsors
	 *
	 * @return \Doctrine\Common\Collections\Collection Sponsor
	*/
	public function getSponsors()
	{
		return $this->sponsors;
	}

	/**
	 * Set slug
	 *
	* @param string $slug
	 * @return Kid
	*/
	public function setSlug($slug){
		if($this->slug !== $slug){
			$this->slug = StringUtils::getSlug($slug);
		}
		return $this;
	}

	/**
	 * Get slug
	*
	 * @return string
	*/
	public function getSlug(){
		return $this->slug;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->name = $this->name;
		$obj->photo = $this->photo;
		$obj->gallery = $this->gallery;
		$theDate = $this->birthdate->format('d/m/Y');
		$obj->birthdate = $theDate;
		$obj->biography = strip_tags($this->biography, '<br>');
		$obj->old = $this->getOld();
		$theDate = $this->dateOfReception->format('d/m/Y');
		$obj->dateOfReception = $theDate;
		$obj->timeInTheHouse = $this->getTimeInTheHouse();
		$obj->ailments = $this->ailments;
		$obj->introText = $this->introText;
		$obj->requirements = '...';
		$obj->sponsors = implode(",", $this->sponsors->toArray());
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->name;
	}

}
?>