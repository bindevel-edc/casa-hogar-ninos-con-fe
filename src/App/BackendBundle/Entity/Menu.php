<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Menu
 *
 * @ORM\Table(name="AppBackendBundleMenu")
 * @ORM\Entity(repositoryClass="App\BackendBundle\Entity\MenuRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Menu implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $name;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $caption;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", nullable=false, unique=false)
	*/
	protected $link;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(name="pagePosition",type="string",length=255, nullable=false, unique=false)
	*/
	protected $pagePosition;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="externalLink",type="boolean", nullable=true, unique=false)
	*/
	protected $externalLink;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(type="boolean", nullable=true, unique=false)
	*/
	protected $enable;

	/**
	 * @var integer
	 *
	 * @Gedmo\SortablePosition
	 * @Assert\NotBlank()
	 * @ORM\Column(type="integer", nullable=false, unique=false)
	*/
	protected $position;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string",length=255, nullable=true, unique=false)
	*/
	protected $icon;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="onlyDisplayIcon",type="boolean", nullable=true, unique=false)
	*/
	protected $onlyDisplayIcon;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * Menu Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->name = '';
		$this->caption = '';
		$this->link = '';
		$this->pagePosition = '';
		$this->externalLink = false;
		$this->enable = true;
		$this->position = 0;
		$this->icon = '';
		$this->onlyDisplayIcon = false;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Menu
	*/
	public function setName($name){
		if($this->name !== $name){
			$this->name = $name;
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getName(){
		return $this->name;
	}

	/**
	 * Set caption
	 *
	 * @param string $caption
	 * @return Menu
	*/
	public function setCaption($caption){
		if($this->caption !== $caption){
			$this->caption = $caption;
		}
		return $this;
	}

	/**
	 * Get caption
	 *
	 * @return string
	*/
	public function getCaption(){
		return $this->caption;
	}

	/**
	 * Set link
	 *
	 * @param string $link
	 * @return Menu
	*/
	public function setLink($link){
		if($this->link !== $link){
			$this->link = $link;
		}
		return $this;
	}

	/**
	 * Get link
	 *
	 * @return string
	*/
	public function getLink(){
		return $this->link;
	}

	/**
	 * Set pagePosition
	 *
	 * @param string $pagePosition
	 * @return Menu
	*/
	public function setPagePosition($pagePosition){
		if($this->pagePosition !== $pagePosition){
			$this->pagePosition = $pagePosition;
		}
		return $this;
	}

	/**
	 * Get pagePosition
	 *
	 * @return string
	*/
	public function getPagePosition(){
		return $this->pagePosition;
	}

	/**
	 * Set externalLink
	 *
	 * @param boolean $externalLink
	 * @return Menu
	*/
	public function setExternalLink($externalLink){
		if($this->externalLink !== $externalLink){
			$this->externalLink = $externalLink;
		}
		return $this;
	}

	/**
	 * Get externalLink
	 *
	 * @return boolean
	*/
	public function getExternalLink(){
		return $this->externalLink;
	}

	/**
	 * Set enable
	 *
	 * @param boolean $enable
	 * @return Menu
	*/
	public function setEnable($enable){
		if($this->enable !== $enable){
			$this->enable = $enable;
		}
		return $this;
	}

	/**
	 * Get enable
	 *
	 * @return boolean
	*/
	public function getEnable(){
		return $this->enable;
	}

	/**
	 * Set position
	 *
	 * @param integer $position
	 * @return Menu
	*/
	public function setPosition($position){
		if($this->position !== $position){
			$this->position = $position;
		}
		return $this;
	}

	/**
	 * Get position
	 *
	 * @return integer
	*/
	public function getPosition(){
		return $this->position;
	}

	/**
	 * Set icon
	 *
	 * @param string $icon
	 * @return Menu
	*/
	public function setIcon($icon){
		if($this->icon !== $icon){
			$this->icon = $icon;
		}
		return $this;
	}

	/**
	 * Get icon
	 *
	 * @return string
	*/
	public function getIcon(){
		return $this->icon;
	}

	/**
	 * Set onlyDisplayIcon
	 *
	 * @param boolean $onlyDisplayIcon
	 * @return Menu
	*/
	public function setOnlyDisplayIcon($onlyDisplayIcon){
		if($this->onlyDisplayIcon !== $onlyDisplayIcon){
			$this->onlyDisplayIcon = $onlyDisplayIcon;
		}
		return $this;
	}

	/**
	 * Get onlyDisplayIcon
	 *
	 * @return boolean
	*/
	public function getOnlyDisplayIcon(){
		return $this->onlyDisplayIcon;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * @ORM\PrePersist()
	*/
	public function prePersist(){
		
	}

	/**
	 * @ORM\PreUpdate()
	*/
	public function preUpdate(){
		
	}

	/**
	 * @ORM\PreRemove()
	*/
	public function preRemove(){
		
	}

	/**
	 * @ORM\PostLoad()
	*/
	public function postLoad(){
		
	}

	/**
	 * @ORM\PostPersist()
	*/
	public function postPersist(){
		
	}

	/**
	 * @ORM\PostRemove()
	*/
	public function postRemove(){
		
	}

	/**
	 * @ORM\PostUpdate()
	*/
	public function postUpdate (){
		
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->name = $this->name;
		$obj->caption = $this->caption;
		$obj->link = $this->link;
		$obj->pagePosition = $this->pagePosition;
		$obj->externalLink = $this->externalLink;
		$obj->enable = $this->enable;
		$obj->position = $this->position;
		$obj->icon = $this->icon;
		$obj->onlyDisplayIcon = $this->onlyDisplayIcon;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->name;
	}

}
?>