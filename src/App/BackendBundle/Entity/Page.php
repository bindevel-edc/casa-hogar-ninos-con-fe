<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;
use Admin\CoreBundle\Helpers\StringUtils;

/**
 * Page
 *
 * @ORM\Table(name="AppBackendBundlePage")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Page implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=550, nullable=false, unique=false)
	*/
	protected $title;

	/**
	 * @var text
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="text", nullable=false, unique=false)
	*/
	protected $content;

	/**
	 * @var integer
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="integer", nullable=false, unique=false)
	*/
	protected $score;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $slug;
	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * Page Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->slug = '';
		$this->title = '';
		$this->content = '';
		$this->score = 0;
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set title
	 *
	 * @param string $title
	 * @return Page
	*/
	public function setTitle($title){
		if($this->title !== $title){
			$this->title = $title;
			$this->setSlug($title);
		}
		return $this;
	}

	/**
	 * Get title
	 *
	 * @return string
	*/
	public function getTitle(){
		return $this->title;
	}

	/**
	 * Set content
	 *
	 * @param text $content
	 * @return Page
	*/
	public function setContent($content){
		if($this->content !== $content){
			$this->content = $content;
		}
		return $this;
	}

	/**
	 * Get content
	 *
	 * @return text
	*/
	public function getContent(){
		return $this->content;
	}

	/**
	 * Set score
	 *
	 * @param integer $score
	 * @return Page
	*/
	public function setScore($score){
		if($this->score !== $score){
			$this->score = $score;
		}
		return $this;
	}

	/**
	 * Get score
	 *
	 * @return integer
	*/
	public function getScore(){
		return $this->score;
	}

	/**
	 * Set slug
	 *
	* @param string $slug
	 * @return Page
	*/
	public function setSlug($slug){
		if($this->slug !== $slug){
			$this->slug = StringUtils::getSlug($slug);
		}
		return $this;
	}

	/**
	 * Get slug
	*
	 * @return string
	*/
	public function getSlug(){
		return $this->slug;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * @ORM\PrePersist()
	*/
	public function prePersist(){
		
	}

	/**
	 * @ORM\PreUpdate()
	*/
	public function preUpdate(){
		
	}

	/**
	 * @ORM\PreRemove()
	*/
	public function preRemove(){
		
	}

	/**
	 * @ORM\PostLoad()
	*/
	public function postLoad(){
		
	}

	/**
	 * @ORM\PostPersist()
	*/
	public function postPersist(){
		
	}

	/**
	 * @ORM\PostRemove()
	*/
	public function postRemove(){
		
	}

	/**
	 * @ORM\PostUpdate()
	*/
	public function postUpdate (){
		
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->title = $this->title;
		$obj->content = $this->content;
		$obj->score = $this->score;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->title;
	}

}
?>