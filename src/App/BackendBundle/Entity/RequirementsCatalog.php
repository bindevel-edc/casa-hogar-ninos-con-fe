<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * RequirementsCatalog
 *
 * @ORM\Table(name="AppBackendBundleRequirementsCatalog")
 * @ORM\Entity()
 */
class RequirementsCatalog implements Translatable {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $name;

	/**
	 * @var string
	 *
	 * @Gedmo\Translatable
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $description;

	/**
	 * @ORM\ManyToMany(targetEntity="Kid", inversedBy="requirements")
	 *
	*/
	protected $kids;

	/**
	 * @Gedmo\Locale
	 * Used locale to override Translation listener`s locale
	 * this is not a mapped field of entity metadata, just a simple property
	*/
	private $locale;

	/**
	 * RequirementsCatalog Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->name = '';
		$this->description = '';
		$this->kids = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return RequirementsCatalog
	*/
	public function setName($name){
		if($this->name !== $name){
			$this->name = $name;
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getName(){
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return RequirementsCatalog
	*/
	public function setDescription($description){
		if($this->description !== $description){
			$this->description = $description;
		}
		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	*/
	public function getDescription(){
		return $this->description;
	}

	/**
	 * Add new Kid to collection
	 *
	 * @param \App\BackendBundle\Entity\Kid $kid
	 * @return RequirementsCatalog
	*/
	public function addKid(\App\BackendBundle\Entity\Kid $kid)
	{
		if(!$this->kids->contains($kid)){
			$this->kids->add($kid);
		}
		return $this;
	}

	/**
	 * Remove a item from collection
	 *
	 * @param \App\BackendBundle\Entity\Kid $kid
	*/
	public function removeKid(\App\BackendBundle\Entity\Kid $kid)
	{
		$this->kids->removeElement($kid);
	}

	/**
	 * Set kids collection
	 *
	 * @param \Doctrine\Common\Collections\Collection $kid
	 * @return RequirementsCatalog
	*/
	public function setKids(\Doctrine\Common\Collections\Collection $kids){
		if($this->kids != $kids){
			$this->kids = $kids;
		}
		return $this;
	}

	/**
	 * Get kids
	 *
	 * @return \Doctrine\Common\Collections\Collection Kid
	*/
	public function getKids()
	{
		return $this->kids;
	}

	/**
	 * Set locale
	 *
	 * @param Gedmo\Locale $locale
	*/
	public function setTranslatableLocale($locale)
	{
		$this->locale = $locale;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->name = $this->name;
		$obj->description = $this->description;
		$obj->kids = implode(',',$this->kids->toArray());
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->name;
	}

}
?>