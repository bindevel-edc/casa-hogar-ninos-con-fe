<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Sponsor
 *
 * @ORM\Table(name="AppBackendBundleSponsor")
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("email")
 */
class Sponsor {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $name;

	/**
	 * @var string
	 *
	 * @Assert\Email(checkMX=false,checkHost=false)
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=true)
	*/
	protected $email;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $phones;

	/**
	 * @var text
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="text",length=255, nullable=false, unique=false)
	*/
	protected $address;

	/**
	 * @var UploadedFile
	 * @Assert\Image()
	 *
	*/
	protected $photo;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="photoPath",type="string", length=255 , nullable=false, unique=false)
	*/
	protected $photoPath;

	/**
	 * @ORM\ManyToMany(targetEntity="Kid", inversedBy="sponsors")
	 *
	*/
	protected $kids;

	/**
	 * @var string
	 *
	 * @Assert\Choice(choices = {"A","B"}, message = "Los valores correctos son PATROCINADOR DE LA CASA HOGAR,PATROCINADOR DE UN NI�O.")
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string", nullable=false, unique=false)
	*/
	protected $patronageType;

	/**
	 * @var decimal
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="decimal", nullable=false, unique=false)
	*/
	protected $amountMonthly;

	/**
	 * Sponsor Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->name = '';
		$this->email = '';
		$this->phones = '';
		$this->address = '';
		$this->kids = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Sponsor
	*/
	public function setName($name){
		if($this->name !== $name){
			$this->name = $name;
		}
		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	*/
	public function getName(){
		return $this->name;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Sponsor
	*/
	public function setEmail($email){
		if($this->email !== $email){
			$this->email = $email;
		}
		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	*/
	public function getEmail(){
		return $this->email;
	}

	/**
	 * Set phones
	 *
	 * @param string $phones
	 * @return Sponsor
	*/
	public function setPhones($phones){
		if($this->phones !== $phones){
			$this->phones = $phones;
		}
		return $this;
	}

	/**
	 * Get phones
	 *
	 * @return string
	*/
	public function getPhones(){
		return $this->phones;
	}

	/**
	 * Set address
	 *
	 * @param text $address
	 * @return Sponsor
	*/
	public function setAddress($address){
		if($this->address !== $address){
			$this->address = $address;
		}
		return $this;
	}

	/**
	 * Get address
	 *
	 * @return text
	*/
	public function getAddress(){
		return $this->address;
	}

	/**
	 * Sets photo.
	 *
	 * @param UploadedFile $photo
	*/
	public function setPhoto(UploadedFile $photo = null)
	{
		// check if we have an old photo path
		if (is_file($this->getAbsolutePhotoPath())) {
			// store the old photo to delete after the update
			$this->_tempPhoto = $this->getAbsolutePhotoPath();
		}

		$this->photo = $photo;
		$this->photoPath = 'initial';
	}

	/**
	 * Get photo.
	 *
	 * @return UploadedFile
	*/
	public function getPhoto()
	{
		return $this->photo;
	}

	/**
	 * Set photo path.
	 *
	 * @param string photo
	*/
	public function setPhotoPath($photoPath)
	{
		$this->photoPath = $photoPath;
	}

	/**
	 * Get photo path.
	 *
	 * @return string
	*/
	public function getPhotoPath()
	{
		return $this->photoPath;
	}

	/**
	 * @ORM\PrePersist()
	 * @ORM\PreUpdate()
	*/
	public function preUploadPhoto()
	{
		if($this->photo == null) return;
		if (null !== $this->photoPath) {
			$filename = sha1(uniqid(mt_rand(), true));
			$this->photoPath = $filename.'.'.$this->photo->getClientOriginalExtension();
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	*/
	public function uploadPhoto()
	{
		if (null === $this->photo) {
			return;
		}

		$this->photo->move($this->getUploadPhotoDir(), $this->photoPath);
		if (isset($this->_tempPhoto)) {
				// delete the old photo
				if(file_exists($this->_tempPhoto)){
					unlink($this->_tempPhoto);
				}
				// clear the temp photo path
				$this->_tempPhoto = null;
		}
		unset($this->photo);
	}

	/**
	 * @ORM\PostRemove()
	*/
	public function removeUploadPhoto()
	{
		if($photo = $this->getAbsolutePhotoPath()){
			if(file_exists($path)){
				unlink($photo);
			}
		}
	}

	public function getPhotoWebPath()
	{
		return null === $this->photoPath ? null : '/'.$this->getUploadPhotoBaseDir().'/'.$this->photoPath;
	}

	public function getAbsolutePhotoPath()
	{
		return null === $this->photoPath ? null : $this->getUploadPhotoDir().'/'.$this->photoPath;
	}

	public function getUploadPhotoDir()
	{
		$path = __DIR__.'/../../../../web/'.$this->getUploadPhotoBaseDir();
		if(!file_exists($path)){
			if(!@mkdir($path,0700,true)) return null;
		}
		return $path;
	}

	public function getUploadPhotoBaseDir()
	{
		return 'App/BackendBundle/Sponsor/photo';
	}

	/**
	 * Add new Kid to collection
	 *
	 * @param \App\BackendBundle\Entity\Kid $kid
	 * @return Sponsor
	*/
	public function addKid(\App\BackendBundle\Entity\Kid $kid)
	{
		if(!$this->kids->contains($kid)){
			$this->kids->add($kid);
		}
		return $this;
	}

	/**
	 * Remove a item from collection
	 *
	 * @param \App\BackendBundle\Entity\Kid $kid
	*/
	public function removeKid(\App\BackendBundle\Entity\Kid $kid)
	{
		$this->kids->removeElement($kid);
	}

	/**
	 * Set kids collection
	 *
	 * @param \Doctrine\Common\Collections\Collection $kid
	 * @return Sponsor
	*/
	public function setKids(\Doctrine\Common\Collections\Collection $kids){
		if($this->kids != $kids){
			$this->kids = $kids;
		}
		return $this;
	}

	/**
	 * Get kids
	 *
	 * @return \Doctrine\Common\Collections\Collection Kid
	*/
	public function getKids()
	{
		return $this->kids;
	}

	/**
	 * Set patronageType
	 *
	 * @param choice $patronageType
	 * @return Sponsor
	*/
	public function setPatronageType($patronageType){
		if($this->patronageType !== $patronageType){
			$this->patronageType = $patronageType;
		}
		return $this;
	}

	/**
	 * Get patronageType
	 *
	 * @return choice
	*/
	public function getPatronageType(){
		return $this->patronageType;
	}

	/**
	 * Set amountMonthly
	 *
	 * @param decimal $amountMonthly
	 * @return Sponsor
	*/
	public function setAmountMonthly($amountMonthly){
		if($this->amountMonthly !== $amountMonthly){
			$this->amountMonthly = $amountMonthly;
		}
		return $this;
	}

	/**
	 * Get amountMonthly
	 *
	 * @return decimal
	*/
	public function getAmountMonthly(){
		return $this->amountMonthly;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->name = $this->name;
		$obj->email = $this->email;
		$obj->phones = $this->phones;
		$obj->address = $this->address;
		$obj->photo = $this->photo;
		$obj->kids = implode(",", $this->kids->toArray());
		$obj->patronageType = $this->patronageType;
		$obj->amountMonthly = $this->amountMonthly;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->name;
	}

}
?>