<?php

namespace App\BackendBundle\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * VisitCounter
 *
 * @ORM\Table(name="AppBackendBundleVisitCounter")
 * @ORM\Entity()
 */
class VisitCounter {


	/**
	* @var integer
	*
	* @ORM\Id
	* @ORM\Column(type="integer")
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	protected $id;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $device;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $navigator;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $version;

	/**
	 * @var string
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="string",length=255, nullable=false, unique=false)
	*/
	protected $os;

	/**
	 * @var datetime
	 *
	 * @Assert\NotBlank()
	 * @ORM\Column(type="datetime", nullable=false, unique=false)
	*/
	protected $dateOfVisit;

	/**
	 * VisitCounter Constructor
	 *
	*/
	public function __construct(){
		$this->id = -1;
		$this->device = '';
		$this->navigator = '';
		$this->version = '';
		$this->os = '';
		$this->dateOfVisit = new \DateTime('NOW');
	}

	/**
	 * Get id
	 *
	 * @return integer 
	*/
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set device
	 *
	 * @param string $device
	 * @return VisitCounter
	*/
	public function setDevice($device){
		if($this->device !== $device){
			$this->device = $device;
		}
		return $this;
	}

	/**
	 * Get device
	 *
	 * @return string
	*/
	public function getDevice(){
		return $this->device;
	}

	/**
	 * Set navigator
	 *
	 * @param string $navigator
	 * @return VisitCounter
	*/
	public function setNavigator($navigator){
		if($this->navigator !== $navigator){
			$this->navigator = $navigator;
		}
		return $this;
	}

	/**
	 * Get navigator
	 *
	 * @return string
	*/
	public function getNavigator(){
		return $this->navigator;
	}

	/**
	 * Set version
	 *
	 * @param string $version
	 * @return VisitCounter
	*/
	public function setVersion($version){
		if($this->version !== $version){
			$this->version = $version;
		}
		return $this;
	}

	/**
	 * Get version
	 *
	 * @return string
	*/
	public function getVersion(){
		return $this->version;
	}

	/**
	 * Set os
	 *
	 * @param string $os
	 * @return VisitCounter
	*/
	public function setOs($os){
		if($this->os !== $os){
			$this->os = $os;
		}
		return $this;
	}

	/**
	 * Get os
	 *
	 * @return string
	*/
	public function getOs(){
		return $this->os;
	}

	/**
	 * Set dateOfVisit
	 *
	 * @param datetime $dateOfVisit
	 * @return VisitCounter
	*/
	public function setDateOfVisit($dateOfVisit){
		if($this->dateOfVisit !== $dateOfVisit){
			$this->dateOfVisit = $dateOfVisit;
		}
		return $this;
	}

	/**
	 * Get dateOfVisit
	 *
	 * @return datetime
	*/
	public function getDateOfVisit(){
		return $this->dateOfVisit;
	}

	/**
	 * toJson()
	 * @return string
	*/
	public function toJson(){
		$obj = $this->toObject();
		return json_encode($obj);
	}

	/**
	 * toObject()
	 * @return stdClass object
	*/
	public function toObject(){
		$obj = new \stdClass();
		$obj->id = $this->id;
		$obj->device = $this->device;
		$obj->navigator = $this->navigator;
		$obj->version = $this->version;
		$obj->os = $this->os;
		$theDate = $this->dateOfVisit->format('d/m/Y');
		$obj->dateOfVisit = $theDate;
		return $obj;
	}

	/**
	 * __toString()
	 * @return string
	*/
	public function __toString(){
		return $this->device;
	}

}
?>