<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File as AssertFile;


class BannerType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text', array(
				'attr' => array(
					'id' => 'Idname'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('title', 'text', array(
				'attr' => array(
					'id' => 'Idtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Título'
				,'required' => true
				,'max_length' => 255
			))

			->add('photo', 'file', array(
				'attr' => array(
					'id' => 'Idphoto'
					,'data-validation' => 'true'
				)
				,'label' => 'Imagen'
				,'required' => true
				,'image_path' => 'photoWebPath'
				,'constraints'=> array(
					new AssertFile(array('maxSize'=>UploadedFile::getMaxFilesize()))
				)
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Banner'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_banner';
	}
}
