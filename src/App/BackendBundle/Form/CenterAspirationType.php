<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class CenterAspirationType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title', 'text', array(
				'attr' => array(
					'id' => 'Idtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Título'
				,'required' => true
				,'max_length' => 255
			))

			->add('description', 'textarea', array(
				'attr' => array(
					'id' => 'Iddescription'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
					,'class' => 'HTMLEditor'
				)
				,'label' => 'Descripción'
				,'required' => true
			,'max_length' => 255
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\CenterAspiration'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_centeraspiration';
	}
}
