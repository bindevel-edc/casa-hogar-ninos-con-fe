<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Admin\CoreBundle\Form\Type\IconSelectorType;

class CenterCharacteristicType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title', 'text', array(
				'attr' => array(
					'id' => 'Idtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Título'
				,'required' => true
				,'max_length' => 255
			))

			->add('icon', new IconSelectorType(), array(
				'attr' => array(
					'id' => 'Idicon'
					,'data-validation' => 'true'
				)
				,'label' => 'Icono'
				,'required' => true
			))

			->add('content', 'text', array(
				'attr' => array(
					'id' => 'Idcontent'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Contenido'
				,'required' => true
				,'max_length' => 255
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\CenterCharacteristic'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_centercharacteristic';
	}
}
