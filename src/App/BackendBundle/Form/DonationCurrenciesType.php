<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class DonationCurrenciesType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('type', 'text', array(
				'attr' => array(
					'id' => 'Idtype'
					,'data-validation' => 'true'
				)
				,'label' => 'Tipo'
				,'required' => true
				,'max_length' => 255
			))

			->add('label', 'text', array(
				'attr' => array(
					'id' => 'Idlabel'
					,'data-validation' => 'true'
				)
				,'label' => 'Texto'
				,'required' => true
				,'max_length' => 255
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\DonationCurrencies'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_donationcurrencies';
	}
}
