<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class DonationType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('user', 'text', array(
				'attr' => array(
					'id' => 'Iduser'
					,'data-validation' => 'true'
				)
				,'label' => 'Patrocinador'
				,'required' => false
				,'max_length' => 255
			))

			->add('amount', 'integer', array(
				'attr' => array(
					'id' => 'Idamount'
					,'data-validation' => 'true'
				)
				,'label' => 'Cantidad'
				,'required' => true
			))

			->add('currency', 'text', array(
				'attr' => array(
					'id' => 'Idcurrency'
					,'data-validation' => 'true'
				)
				,'label' => 'Moneda'
				,'required' => true
				,'max_length' => 255
			))

			->add('dateOf', 'date', array(
				'attr' => array(
					'id' => 'IddateOf'
					,'data-validation' => 'true'
					,'class' => 'useDatepicker'
				)
				,'label' => 'Fecha'
				,'widget' => 'single_text'
				,'format' => 'dd-MM-yyyy'
				,'required' => true
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Donation'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_donation';
	}
}
