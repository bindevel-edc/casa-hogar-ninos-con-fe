<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class HelperBlockType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title', 'text', array(
				'attr' => array(
					'id' => 'Idtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Título'
				,'required' => true
				,'max_length' => 255
			))

			->add('content', 'text', array(
				'attr' => array(
					'id' => 'Idcontent'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Contenido'
				,'required' => true
				,'max_length' => 255
			))

			->add('subtitle', 'text', array(
				'attr' => array(
					'id' => 'Idsubtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Subtítulo'
				,'required' => true
				,'max_length' => 255
			))

			->add('photo', 'text', array(
				'attr' => array(
					'id' => 'Idphoto'
					,'data-validation' => 'true'
				)
				,'label' => 'Photo'
				,'required' => true
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\HelperBlock'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_helperblock';
	}
}
