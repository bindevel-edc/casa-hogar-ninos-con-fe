<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class KidType extends AbstractType
{

	function __construct($showReqs = false) {
		$this->showReqs = $showReqs;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text', array(
				'attr' => array(
					'id' => 'Idname'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('introText', 'text', array(
				'attr' => array(
					'id' => 'IdintroText'
					,'data-validation' => 'true'
				)
				,'label' => 'Texto de perfil'
				,'required' => true
				,'max_length' => 255
			))

			->add('photo', 'text', array(
				'attr' => array(
					'id' => 'Idphoto'
					,'data-validation' => 'true'
				)
				,'label' => 'Foto de perfil'
				,'required' => true
			))

			->add('gallery', 'text', array(
				'attr' => array(
					'id' => 'Idgallery'
					,'data-validation' => 'true'
				)
				,'label' => 'Galería multimedia'
				,'required' => true
				,'max_length' => 255
			))

			->add('birthdate', 'date', array(
				'attr' => array(
					'id' => 'Idbirthdate'
					,'data-validation' => 'true'
					,'class' => 'useDatepicker'
				)
				,'label' => 'Fecha de nacimiento'
				,'widget' => 'single_text'
				,'format' => 'dd-MM-yyyy'
				,'required' => true
			))

			->add('biography', 'textarea', array(
				'attr' => array(
					'id' => 'Idbiography'
					,'data-validation' => 'true'
					,'class' => 'HTMLEditor'
				)
				,'label' => 'Biografía'
				,'required' => true
			,'max_length' => 255
			))

			->add('old', 'integer', array(
				'attr' => array(
					'id' => 'Idold'
					,'data-validation' => 'true'
				)
				,'label' => 'Edad'
				,'required' => true
				,'read_only' => true
			))

			->add('dateOfReception', 'date', array(
				'attr' => array(
					'id' => 'IddateOfReception'
					,'data-validation' => 'true'
					,'class' => 'useDatepicker'
				)
				,'label' => 'Fecha de acogida'
				,'widget' => 'single_text'
				,'format' => 'dd-MM-yyyy'
				,'required' => true
			))

			->add('timeInTheHouse', 'text', array(
				'attr' => array(
					'id' => 'IdtimeInTheHouse'
					,'data-validation' => 'true'
				)
				,'label' => 'Tiempo que lleva'
				,'required' => true
				,'read_only' => true
				,'max_length' => 255
			))

			->add('ailments', 'text', array(
				'attr' => array(
					'id' => 'Idailments'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Padecimientos'
				,'required' => true
				,'max_length' => 255
			))
		;
		
		if($this->showReqs){
			$builder->add('requirements', 'entity', array(
				'attr' => array(
					'id' => 'IdRequirements'
					,'data-validation' => 'true'
				)
				,'label' => 'Necesidades'
				,'class' => 'App\BackendBundle\Entity\RequirementsCatalog'
				,'property' => 'name'
				,'multiple' => true
				,'expanded' => false
                ,'by_reference' => false
			));
		}

	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Kid'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_kid';
	}
}
