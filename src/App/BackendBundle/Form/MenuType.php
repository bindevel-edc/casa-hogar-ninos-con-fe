<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Admin\CoreBundle\Form\Type\IconSelectorType;

class MenuType extends AbstractType
{

	function __construct($menuChoices) {
		$this->menuChoices = $menuChoices;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{

		$config = $builder->getData();
		$isExt = $config->getExternalLink();

		$builder
			->add('name', 'text', array(
				'attr' => array(
					'id' => 'Idname'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('caption', 'text', array(
				'attr' => array(
					'id' => 'Idcaption'
					,'data-validation' => 'true'
				)
				,'label' => 'Texto'
				,'required' => true
				,'max_length' => 255
			))

			->add('links', 'choice', array(
				'attr' => array(
					'id' => 'IdpagePosition'
					,'data-validation' => 'true'
				)
				,'choices'   => $this->menuChoices
				,'data' => '/'
				,'label' => 'Enlace'
				,'required' => false
				,'mapped' => false
			))

			->add('pagePosition', 'choice', array(
				'attr' => array(
					'id' => 'IdpagePosition'
					,'data-validation' => 'true'
				)
				,'choices'   => array(
					'top'   => 'Superior',
					'bottom'   => 'Fondo',
				)
				,'label' => 'Posición en la pág.'
				,'required' => true
			))

			->add('externalLink', 'checkbox', array(
				'attr' => array(
					'id' => 'IdexternalLink'
					,'data-validation' => 'true'
				)
				,'label' => 'Enlace externo'
				,'required' => false
			))

			->add('enable', 'checkbox', array(
				'attr' => array(
					'id' => 'Idenable'
					,'data-validation' => 'true'
				)
				,'label' => 'Habilitado'
				,'required' => false
			))

			->add('icon', new IconSelectorType(), array(
				'attr' => array(
					'id' => 'Idicon'
					,'data-validation' => 'true'
				)
				,'label' => 'Icono'
				,'required' => false
			))

			->add('onlyDisplayIcon', 'checkbox', array(
				'attr' => array(
					'id' => 'IdonlyDisplayIcon'
					,'data-validation' => 'true'
				)
				,'label' => 'Mostrar solo el ícono'
				,'required' => false
			))
		;

			if($isExt){

				$builder
				->add('link', 'text', array(
					'attr' => array(
						'id' => 'Idlink'
						,'data-validation' => 'true'
					)

					,'label' => 'Enlace'
					,'required' => true
					,'max_length' => 255
				));
			} else {

				$builder
				->add('link', 'choice', array(
					'attr' => array(
						'id' => 'IdLinks'
						,'data-validation' => 'true'
					)
					,'choices'   => $this->menuChoices
					,'label' => 'Enlace'
					,'required' => true
				));
			}
			
			$builder->get('link')->resetViewTransformers();
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Menu'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_menu';
	}
}
