<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class PageType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('title',TextType::class, array(
				'attr' => array(
					'id' => 'Idtitle'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Título'
				,'required' => true
				,'max_length' => 550
			))

			->add('content',TextareaType::class, array(
				'attr' => array(
					'id' => 'Idcontent'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
					,'class' => 'HTMLEditor'
				)
				,'label' => 'Contenido'
				,'required' => true
			))


		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Page'
		));
	}

	/**
	 * @return string
	 */
	public function getBlockPrefix()
	{
		return 'app_backendbundle_page';
	}
}
