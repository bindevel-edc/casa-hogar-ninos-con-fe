<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class RequirementsCatalogType extends AbstractType
{

	function __construct($showKids = false) {
		$this->showKids = $showKids;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text', array(
				'attr' => array(
					'id' => 'Idname'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('description', 'text', array(
				'attr' => array(
					'id' => 'Iddescription'
					,'data-validation' => 'true'
					,'data-translatable' => 'true'
				)
				,'label' => 'Descripción'
				,'required' => true
				,'max_length' => 255
			))
		;
        
		if($this->showKids){
			$builder->add('kids', 'entity', array(
				'attr' => array(
					'id' => 'IdKids'
					,'data-validation' => 'true'
				)
				,'label' => 'Niños'
				,'class' => 'App\BackendBundle\Entity\Kid'
				,'property' => 'name'
				,'multiple' => true
				,'expanded' => false
			));
		}
        
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\RequirementsCatalog'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_requirementscatalog';
	}
}
