<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\File as AssertFile;


class SponsorType extends AbstractType
{


	function __construct($showKids = false) {
		$this->showKids = $showKids;
	}

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('name', 'text', array(
				'attr' => array(
					'id' => 'Idname'
					,'data-validation' => 'true'
				)
				,'label' => 'Nombre'
				,'required' => true
				,'max_length' => 255
			))

			->add('email', 'text', array(
				'attr' => array(
					'id' => 'Idemail'
					,'data-validation' => 'true'
				)
				,'label' => 'Email'
				,'required' => true
				,'max_length' => 255
			))

			->add('phones', 'text', array(
				'attr' => array(
					'id' => 'Idphones'
					,'data-validation' => 'true'
				)
				,'label' => 'Teléfonos'
				,'required' => true
				,'max_length' => 255
			))

			->add('address', 'textarea', array(
				'attr' => array(
					'id' => 'Idaddress'
					,'data-validation' => 'true'
				)
				,'label' => 'Dirección'
				,'required' => true
			,'max_length' => 255
			))

			->add('photo', 'file', array(
				'attr' => array(
					'id' => 'Idphoto'
					,'data-validation' => 'true'
				)
				,'label' => 'Foto'
				,'required' => true
				,'image_path' => 'photoWebPath'
				,'constraints'=> array(
					new AssertFile(array('maxSize'=>UploadedFile::getMaxFilesize()))
				)
			))
			->add('patronageType', 'choice', array(
				'attr' => array(
					'id' => 'IdpatronageType'
					,'data-validation' => 'true'
				)
				,'choices'   => array(
					'A'   => 'PATROCINADOR DE LA CASA HOGAR',
					'B'   => 'PATROCINADOR DE UN NIÑO',
				)
				,'label' => 'Tipo de patrocinio'
				,'required' => true
			))

			->add('amountMonthly', 'number', array(
				'attr' => array(
					'id' => 'IdamountMonthly'
					,'data-validation' => 'true'
				)
				,'label' => 'Monto mensual al que se compromete'
				,'required' => true
			))
		;

		if($this->showKids){
			$builder->add('kids', 'entity', array(
				'attr' => array(
					'id' => 'IdKids'
					,'data-validation' => 'true'
				)
				,'label' => 'Niños'
				,'class' => 'App\BackendBundle\Entity\Kid'
				,'property' => 'name'
				,'multiple' => true
				,'expanded' => false
                ,'required' => false
			));
		}

	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\Sponsor'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_sponsor';
	}
}
