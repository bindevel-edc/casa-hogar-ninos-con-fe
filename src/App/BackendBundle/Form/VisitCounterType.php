<?php

namespace App\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;



class VisitCounterType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('device', 'text', array(
				'attr' => array(
					'id' => 'Iddevice'
					,'data-validation' => 'true'
				)
				,'label' => 'Dispositivo'
				,'required' => true
				,'max_length' => 255
			))

			->add('navigator', 'text', array(
				'attr' => array(
					'id' => 'Idnavigator'
					,'data-validation' => 'true'
				)
				,'label' => 'Navegador'
				,'required' => true
				,'max_length' => 255
			))

			->add('version', 'text', array(
				'attr' => array(
					'id' => 'Idversion'
					,'data-validation' => 'true'
				)
				,'label' => 'Versión'
				,'required' => true
				,'max_length' => 255
			))

			->add('os', 'text', array(
				'attr' => array(
					'id' => 'Idos'
					,'data-validation' => 'true'
				)
				,'label' => 'Sistema Operativo'
				,'required' => true
				,'max_length' => 255
			))

			->add('dateOfVisit', 'date', array(
				'attr' => array(
					'id' => 'IddateOfVisit'
					,'data-validation' => 'true'
					,'class' => 'useDatepicker'
				)
				,'label' => 'Fecha de la visita'
				,'widget' => 'single_text'
				,'format' => 'dd-MM-yyyy'
				,'required' => true
			))


		;
	}
	
	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'App\BackendBundle\Entity\VisitCounter'
		));
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'app_backendbundle_visitcounter';
	}
}
