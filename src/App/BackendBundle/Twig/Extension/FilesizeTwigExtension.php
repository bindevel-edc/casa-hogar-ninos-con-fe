<?php
namespace App\BackendBundle\Twig\Extension;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FilesizeTwigExtension extends \Twig_Extension
{

	public function getFunctions()
	{
		return array(
			'max_file_size' => new \Twig_Function_Method($this, 'getMaxFilesize')
		);
	}

	public function getMaxFilesize() {
		return UploadedFile::getMaxFilesize();
	}

	/**
	 * Gets filters
	 *
	 * @return array
	 */
	public function getFilters()
	{
		return array(
			 new \Twig_SimpleFilter('format_bytes', array($this, 'formatBytes')),
		);
	}

	public function getName()
	{
		return 'file_size';
	}

	function formatBytes($bytes, $precision = 2)
	{
		$units = array('B', 'KB', 'MB', 'GB', 'TB');
		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		// Uncomment one of the following alternatives
		 $bytes /= pow(1024, $pow);

		return round($bytes, $precision) . ' ' . $units[$pow];
	}

}