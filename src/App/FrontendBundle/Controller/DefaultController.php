<?php

namespace App\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Email;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

use App\BackendBundle\Entity\VisitCounter;
use Symfony\Component\HttpFoundation\Session\Session;

use App\BackendBundle\Entity\Donation;

class DefaultController extends Controller
{

	public function setContainer(ContainerInterface $container = null)
	{

		setlocale(LC_ALL, 'es_ES');
		$this->container = $container;
		$this->runVisitCounter();
		$this->loadConf();

	}

	private function runVisitCounter(){

	
		//$session = new Session();
		//$session->start();
		
		$request = $this->getRequest();
		$session = $request->getSession();
		
		if(!$session->has('vCounter')){

			$session->set('vCounter', '001');
			
			DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);
			$dd = new DeviceDetector($_SERVER['HTTP_USER_AGENT']);
			$dd->parse();
			
			$clientInfo = $dd->getClient();
			$osInfo = $dd->getOs();		

			$device = '';
			
			if ($dd->isSmartphone()){
				$device = 'Smartphone';
			} else if ($dd->isFeaturePhone()){
				$device = 'FeaturePhone'; 
			} else if ($dd->isTablet()){
				$device = 'Tablet'; 
			} else if ($dd->isPhablet()){
				$device = 'Phablet'; 
			} else if ($dd->isConsole()){
				$device = 'Console'; 
			} else if ($dd->isPortableMediaPlayer()){
				$device = 'Portable MediaPlayer';	  
			} else if ($dd->isCarBrowser()){
				$device = 'CarBrowser'; 
			} else if ($dd->isTV()){
				$device = 'TV'; 
			} else if ($dd->isSmartDisplay()){
				$device = 'SmartDisplay'; 
			} else if ($dd->isCamera()){
				$device = 'Camera'; 
			} else if ($dd->isBrowser()){
				$device = 'Browser'; 
			} else if ($dd->isFeedReader()){
				$device = 'FeedReader'; 
			} else if ($dd->isMobileApp()){
				$device = 'MobileApp'; 
			} else if ($dd->isPIM()){
				$device = 'PIM'; 
			} else if ($dd->isLibrary()){
				$device = 'Library'; 
			} else if ($dd->isMediaPlayer()){
				$device = 'MediaPlayer'; 
			} else {
				$device = '-'; 
			}
			
			$visit = new VisitCounter();
			$visit->setDevice($device);		   
			$visit->setNavigator($clientInfo['name']);		  
			$visit->setVersion($clientInfo['version']);		   
			$visit->setOs($osInfo['name'].' '.$osInfo['version']);

			$em = $this->getDoctrine()->getManager();
			$em->persist($visit);
			$em->flush();		 
		
		}
	
		
	}

	public function loadConf(){

		$Conf = $this->getDoctrine()
				->getRepository('AdminCoreBundle:Config')
				->findAll();

		$em = $this->getDoctrine()->getManager();

		
		if($Conf){
			
			$this->loadTwig();
			$this->webconf=array();

			foreach($Conf as $item){
				$this->webconf[$item->getVarname()]= $this->myTwig->render($item->getValue(),$this->webconf);
			} 
			
			if(isset($this->webconf['SiteOffline'])){
				$this->SiteOffline = $this->webconf['SiteOffline'];
			}

		} else {
			$this->webconf=array();
		}

		$month = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		$mtext = ((int) date('d')).' '.$month[date('n')-1] ;

		$this->webconf['month'] = $mtext;
		$this->webconf['last_username'] = '';
		$this->webconf['error'] = '';

	}

	private function loadDefaults($_locale,$loadMenu = true){

		/*
		$bl = 'es';
		try {
			$bl = $this->getUserBaseLanguage()[0]->base;
		} catch(\Exception $e){
		
		}

		if($_locale != $bl){
		   $_locale = $bl; 
		}
		*/
		
		if($_locale == 'es'){
			setlocale(LC_ALL, 'es_ES');
		} else {
			setlocale(LC_ALL, 'en_US');
		} 

		$this->locale = $_locale;
		$this->webconf['AppLocale'] = $_locale;
		$this->webconf['amountMonthly'] = 0;
		
		if($loadMenu){
			$this->loadTopMenu();
			$this->loadBottomMenu();
		}
		
		$this->sponsorLogin = false;
		if ($this->get('security.authorization_checker')->isGranted('ROLE_SPONSOR')) {

			$this->sponsorLogin = true;
			$user = $this->getUser();
			$email = $user->getEmail();
			$this->webconf['userEml'] = $email;
			
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->findOneByEmail($email);
			
			$this->sponsor = $entity;
			$this->webconf['sponsor'] = $entity;
			
		}
	
		
	}

	private function loadTopMenu(){

		$em = $this->getDoctrine()->getManager();
		
		$query = $em->createQuery('SELECT m FROM AppBackendBundle:Menu m WHERE m.pagePosition = \'top\' AND m.enable = 1 ORDER BY m.position ASC');
		$TopMenu = $query->getResult(); 

		if($TopMenu){
			$this->webconf['TopMenu']=$TopMenu;
		} else {
			$this->webconf['TopMenu']=array();
		}
	}

	private function loadBottomMenu(){

		$em = $this->getDoctrine()->getManager();
		
		$query = $em->createQuery('SELECT m FROM AppBackendBundle:Menu m WHERE m.pagePosition = \'bottom\' AND m.enable = 1  AND m.icon <> \'-\' ORDER BY m.position ASC');
		$BottomMenu = $query->getResult(); 

		if($BottomMenu){
			$this->webconf['BottomMenu']=$BottomMenu;
		} else {
			$this->webconf['BottomMenu']=array();
		}

	}

	private function loadBanners(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT m FROM AppBackendBundle:Banner m ORDER BY m.position ASC');
		$Banners = $query->getResult(); 

		foreach($Banners as $banner){

			$imgpath = $banner->getAbsolutePhotoPath();

			if(!file_exists($imgpath)){
				continue;
			}
			
			/*
			//todo optimize images
			
			$theSize = getimagesize($imgpath);
			$width = $theSize[0];
			$height = $theSize[1];

			
			if(($width > 1170) && ($height > 500)){
				$image = new imageLib($imgpath);
				$image->resizeImage(1170,500,0, true);
				$image->saveImage($imgpath,100);
			}
			*/
			

		}
		
		if($Banners){
			$this->webconf['Banners']=$Banners;
		} else {
			$this->webconf['Banners']=array();
		}
	}

	private function loadRandomKids(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT k,RAND() AS HIDDEN rand FROM AppBackendBundle:Kid k ORDER BY rand')->setMaxResults(4);
		$randkids1 = $query->getResult(); 

		if($randkids1){

			$this->webconf['randkids1'] = $randkids1;

			$list = array();
			foreach($randkids1 as $kid){
				$list[] = $kid->getId();
			}

			$in = implode(',',$list);

			$query = $em->createQuery('SELECT k,RAND() AS HIDDEN rand FROM AppBackendBundle:Kid k WHERE k.id NOT IN ('. $in .') ORDER BY rand')->setMaxResults(4);
			$randkids2 = $query->getResult(); 

			if($randkids2){
				$this->webconf['randkids2'] = $randkids2;
			} else {
				$this->webconf['randkids2'] = array();
			}

		} else {
			$this->webconf['randkids1'] = array();
			$this->webconf['randkids2'] = array();
		}


	}
	
	private function loadKids(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT k FROM AppBackendBundle:Kid k');//->setMaxResults(4);
		$kids = $query->getResult(); 

		if($kids){
			$this->webconf['kids'] = $kids;
		} else {
			$this->webconf['kids'] = array();
		}
	}

	private function loadCenterAspiration(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT c FROM AppBackendBundle:CenterAspiration c');
		$Aspirations = $query->getResult(); 

		if($Aspirations){
			$this->webconf['Aspirations']=$Aspirations;
		} else {
			$this->webconf['Aspirations']=array();
		}
	}

	private function loadHelperBlock(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT h FROM AppBackendBundle:HelperBlock h')->setMaxResults(6);
		$HelperBlocks = $query->getResult(); 

		if($HelperBlocks){
			$this->webconf['HelperBlocks']= $HelperBlocks;
		} else {
			$this->webconf['HelperBlocks']=array();
		}
	}
	
	private function loadCenterCharacteristic(){

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT c FROM AppBackendBundle:CenterCharacteristic c')->setMaxResults(8);
		$Characteristics = $query->getResult(); 

		if($Characteristics){
			$this->webconf['Characteristics']=$Characteristics;
		} else {
			$this->webconf['Characteristics']=array();
		}
	}

	public function loadTwig(){

		if(!isset($this->myTwig)){
			$loader = new \Twig_Loader_String();
			$this->myTwig = new \Twig_Environment($loader);
			$this->myTwig->addExtension(new \Symfony\Bundle\TwigBundle\Extension\AssetsExtension($this->container));
			$this->myTwig->addExtension(new \Symfony\Bundle\TwigBundle\Extension\ActionsExtension($this->container));
			$this->myTwig->addExtension(new \Symfony\Bridge\Twig\Extension\RoutingExtension($this->get('router')));
			$this->myTwig->addExtension(new \Symfony\Bridge\Twig\Extension\HttpKernelExtension($this->get('fragment.handler')));
		}
	}

	private function parseImageGallery($content){

		$em = $this->getDoctrine()->getManager();

		if(preg_match_all("/\{gallery\}(\d+)\{\/gallery\}/i",$content,$matches)) {

			$galleriesIds = implode(',',$matches[1]);
			$query = $em->createQuery("SELECT g FROM SanMartinBackendBundle:ImageGallery g WHERE g.id in ($galleriesIds)");
			$Galleries = $query->getResult(); 

			
			foreach($Galleries as $gallery){
				$id = $gallery->getId();
				$images = $gallery->getImages();
				foreach($images as $img){

					$imgName = $img->getImagePath();
					$imgFullPath = $img->getAbsolutePhotoPath();
					$newImgName = $img->getUploadImageDir().'/'.'th-'.$imgName;
					
					/*
					//263x148
					if(!file_exists($newImgName)){
						$image = new imageLib($imgFullPath);
						$image->resizeImage(350,300,0, true);
						$image->saveImage($newImgName,100);
					}
					*/
					$img->url = '/'.$img->getUploadPhotoBaseDir().'/'.'th-'.$imgName;
				}
			}

			$data = $this->renderView('AppBackendBundle:Default:image_gallery.html.twig',array('Gallery'=>$gallery));
			$content = str_replace("{gallery}$id{/gallery}",$data,$content);
		}
		
		return $content;
	}

	function getUserBaseLanguage() {
		//global $_SERVER;
		$accept_languages			= $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		$accept_languages_arr		= explode(",",$accept_languages);
		$i = 0;
		foreach($accept_languages_arr as $accept_language) {
			preg_match ("/^(([a-zA-Z]+)(-([a-zA-Z]+)){0,1})(;q=([0-9.]+)){0,1}/" , $accept_language, $matches );
			
			if (!isset($matches[6])) $matches[6]=1;
			
			$lang = new \stdClass();
			$lang->base = $matches[2];
			$lang->ext= $matches[4];
			$lang->lng = $matches[1];
			$lang->priority = $matches[6];
			$lang->_str = $accept_language;
			$result[$i++] = $lang;

		}
		return $result;
	}

	/**
	 * Update page section
	 * @Security("has_role('ROLE_ADMIN')")
	 * @Route("/update/{entity}/{property}/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"},name="index_update",requirements={"entity" = "[A-Z][a-zA-Z09]+","property" = "[a-z][a-zA-Z09]+","id" = "\d+"}) 
	 */
	public function updateAction($entity,$property,$id,$_locale)
	{
	
		$em = $this->getDoctrine()->getManager();
		$ResultObj = new \stdClass();
		$ResultObj->success = true;
		$ResultObj->message = 'Guardado exitosamente.';
		$ResultObj->total = 0;
		$ResultObj->data = array();
		$validator = $this->get('validator');
		
		$entities = array();
		$entities['Config'] = 'Admin\CoreBundle\Entity\Config';
		$entities['Banner'] = 'App\BackendBundle\Entity\Banner';
		$entities['CenterAspiration'] = 'App\BackendBundle\Entity\CenterAspiration';
		$entities['CenterCharacteristic'] = 'App\BackendBundle\Entity\CenterCharacteristic';
		$entities['HelperBlock'] = 'App\BackendBundle\Entity\HelperBlock';
		$entities['Kid'] = 'App\BackendBundle\Entity\Kid';
		$entities['Page'] = 'App\BackendBundle\Entity\Page';

		
		try {
		
			if(!isset($_POST['value'])){
				throw $this->createNotFoundException('No se puede actualizar, la propiedad no puede ser nula.');
			}
			
			$value = $_POST['value'];
			
			
			if(isset($entities[$entity])){
				
				$ns = $entities[$entity];
				$Obj = $em->getRepository($ns)->findOneById($id);

				if (!$Obj) {
					throw $this->createNotFoundException('No se puede actualizar, porque no se encuentra el elemento.');
				}
				
				$method = 'set'.ucfirst($property);
				if(!method_exists($Obj,$method)){
					throw $this->createNotFoundException("No se puede actualizar,la propiedad $property no existe.");
				}
				
				$Obj->$method($value);
				
				if(!method_exists($Obj,'setTranslatableLocale')){
					$Obj->setTranslatableLocale($_locale);
					$ResultObj->setTranslatableLocale = true;
				}
				
				
				$errors = $validator->validate($Obj);
				if (count($errors) > 0) {
					
					$ResultObj->message = '';
					$ResultObj->success = false;
					$ResultObj->errors = $errors;
					
					foreach($errors as $err){
						$ResultObj->message = $err->getMessage();
						$ResultObj->vClass	= get_class($err);
						$ResultObj->invalidValue = $err->getInvalidValue();
						$ResultObj->propertyPath = $err->getPropertyPath();
						$ResultObj->cause = $err->getCause();
						$ResultObj->obj = $Obj->toObject();
						//$ResultObj->message .= $err->__toString().'<br/>';
					}
					
				} else {
					$em->persist($Obj);
					$ResultObj->data[]=$Obj->toObject();
				}
				

				$em->flush();				 
			
			} else {
				throw $this->createNotFoundException("No se puede actualizar,la entidad $entity no existe.");
			}
			
			$ResultObj->data = $value;
			$ResultObj->total = count($ResultObj->data);
			
		}catch(\Exception $e){
			$ResultObj->line = $e->getLine();
			$ResultObj->message = $e->getMessage();
			$ResultObj->success = false;
		}
		
		return new Response(json_encode($ResultObj));
	}

	private function processForm(Request $request){
	
		
		$form = $this->createFormBuilder(array())
			->add('name', 'text', array(
			   'constraints' => array(
				   new NotBlank(),
				   new NotNull()
			)))
			->add('email', 'email', array(
			   'constraints' => array(
				   new NotBlank(),
				   new NotNull(),
				   new Email()
			)))
			->add('message', 'textarea', array(
			   'constraints' => array(
				   new NotBlank(),
				   new NotNull()
			)))
			->add('send', 'button')
			->getForm();
			
			$form->handleRequest($request);

			$this->webconf['formFail'] = false;
			
			if ($form->isValid()) {
				// data es un array con claves 'name', 'email', y 'message'
				$data = $form->getData();

				$mailServer = isset($this->webconf['MailServer']) ? intval($this->webconf['MailServer']) : '127.0.0.1';
				$mailPort = isset($this->webconf['MailPort']) ? intval($this->webconf['MailPort']) : 25;
				$mailUser = isset($this->webconf['MailUser']) ? $this->webconf['MailUser']:false;
				$mailPass = isset($this->webconf['MailPass']) ? $this->webconf['MailPass']:false;
				$mailRecipient = isset($this->webconf['MailRecipient']) ? $this->webconf['MailRecipient']:false;

				$mailer = $this->get('mailer');
				
				$transport = \Swift_SmtpTransport::newInstance($mailServer, $mailPort);
				if($mailUser !== false){
					$transport->setUsername($mailUser);
				}
				
				if($mailPass !== false){
					$transport->setPassword($mailPass);
				}
				
				$body = "Nombre: {$data['name']}\n";
				$body .= "Email: {$data['email']}\n\n";
				$body .= "{$data['message']}\n";
				
				$msg = \Swift_Message::newInstance()
				->setSubject('Nuevo Mensaje ')
				->setFrom($mailUser)
				->setTo($mailRecipient)
				->setBody($body)
				;

				try {
					if(!$mailer->send($msg,$failures)){

						$ErrList = '';
						foreach($failures as $err){
							$ErrList .= $err ."\n";
						}
						$this->LogErr("Error al enviar el correo $ErrList");
					}else{
						$SendStatus=true;
					}

				} catch (Exception $e) {
						$this->LogErr("Error al enviar el correo {$e->getMessage()}");
				}

					
			} else {
				if(isset($_POST["form"])){
					$this->webconf['formFail'] = true;
				}
			}

		$this->webconf['form'] = $form->createView();
	
	}

	private function loadQtys(){
	
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT q FROM AppBackendBundle:DonationQty q');
		$Qtys = $query->getResult(); 

		if($Qtys){
			$this->webconf['Qtys']= $Qtys;
		} else {
			$this->webconf['Qtys']=array();
		}		 
	
	}	 
	
	private function loadCurrencies(){
	
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT c FROM AppBackendBundle:DonationCurrencies c');
		$Currencies = $query->getResult(); 

		if($Currencies){
			$this->webconf['Currencies']= $Currencies;
		} else {
			$this->webconf['Currencies']=array();
		}		 
	
	}
	
	private function createPayPalApiCtx(){
	
		// ### Api context
		// Use an ApiContext object to authenticate
		// API calls. The clientId and clientSecret for the
		// OAuthTokenCredential class can be retrieved from
		// developer.paypal.com

		//$clientId = $this->webconf['PayPalClientId'];
		//$clientSecret = $this->webconf['PayPalClientSecret'];
		
		$clientId = $this->getParameter('paypal_api_pass');
		$clientSecret = $this->getParameter('paypal_api_user');		   
		
		$apiContext = new ApiContext(
			new OAuthTokenCredential(
				$clientId,
				$clientSecret
			)
		);

		// Comment this line out and uncomment the PP_CONFIG_PATH
		// 'define' block if you want to use static file
		// based configuration

		//$useSandBox = true;
		$useSandBox = $this->getParameter('paypal_use_sandbox');
		
		/*
		if(isset($this->webconf['PayPalUseSandBox'])){
			$useSandBox = $this->webconf['PayPalUseSandBox'];
		}
		*/
		
		$apiContext->setConfig(
			array(
				'mode' => ($useSandBox) ? 'sandbox' : 'live',
				'log.LogEnabled' => true,
				'log.FileName' => '../PayPal.log',
				'log.LogLevel' => ($useSandBox) ? 'DEBUG' : 'INFO', // PLEASE USE `INFO` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
				'cache.enabled' => true,
				// 'http.CURLOPT_CONNECTTIMEOUT' => 30
				// 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
				//'log.AdapterFactory' => '\PayPal\Log\DefaultLogFactory' // Factory class implementing \PayPal\Log\PayPalLogFactory
			)
		);

		// Partner Attribution Id
		// Use this header if you are a PayPal partner. Specify a unique BN Code to receive revenue attribution.
		// To learn more or to request a BN Code, contact your Partner Manager or visit the PayPal Partner Portal
		// $apiContext->addRequestHeader('PayPal-Partner-Attribution-Id', '123123123');

		return $apiContext;
	
	}
	
	/**
	 * @Route("/processDonation", name="processDonation")
	 * @Method({"POST"})
	 */
	public function processDonationAction(){
	
		
		$api = $this->createPayPalApiCtx();
		
		$request = $this->getRequest();
		
		$fAmount = $request->request->get('amount',10.00);
		$currencyCode = $request->request->get('currency_code','USD');
		
		// ### Payer
		// A resource representing a Payer that funds a payment
		// For paypal account payments, set payment method
		// to 'paypal'.		   
		$payer = new Payer();
		$payer->setPaymentMethod('paypal');
		
		// ### Itemized information
		// (Optional) Lets you specify item wise
		// information
		$item1 = new Item();
		$item1->setName('Donation')
			->setCurrency($currencyCode)
			->setQuantity(1)
			->setPrice($fAmount);		

		$item_list = new ItemList();
		$item_list->setItems(array($item1));
		
		// ### Amount
		// Lets you specify a payment amount.
		// You can also specify additional details
		// such as shipping, tax.

		$amount = new Amount();
		$amount->setCurrency($currencyCode)
			->setTotal($fAmount);
	
		$transaction = new Transaction();
		$transaction->setAmount($amount)
			->setItemList($item_list)
			->setDescription('Pedido de prueba');

		$redirect_urls = new RedirectUrls();
		$url = $this->generateUrl('paymentStatus', array(),UrlGeneratorInterface::ABSOLUTE_URL);
		$redirect_urls->setReturnUrl($url)->setCancelUrl($url);

		$payment = new Payment();
		$payment->setIntent('sale')
			->setPayer($payer)
			->setRedirectUrls($redirect_urls)
			->setTransactions(array($transaction));

		try {
			$payment->create($api);
		} catch (\PayPal\Exception\PPConnectionException $ex) {

			$kernel = $this->get('kernel');
		
			if ($kernel->isDebug()) {
				echo "Exception: " . $ex->getMessage() . PHP_EOL;
				var_dump(json_decode($ex->getData(), true));
				exit;
			} else {
				return $this->render('AppFrontendBundle:Default:error.html.twig', $this->webconf);
			}
		}

		foreach($payment->getLinks() as $link) {
			if($link->getRel() == 'approval_url') {
				$redirect_url = $link->getHref();
				break;
			}
		}

		$session = $request->getSession();
		// add payment ID to session
		$session->set('paypal_payment_id',$payment->getId());
		$session->set('currencyCode',$currencyCode);
		$session->set('amount',$fAmount);

		if(isset($redirect_url)) {
			// redirect to paypal
			return $this->redirect($redirect_url);
		}

		//Ups! Error desconocido.
		return $this->render('AppFrontendBundle:Default:error.html.twig', $this->webconf);		  
	}

	/**
	 * @Route("/paymentStatus/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="paymentStatus")
	 */
	public function paymentStatusAction(Request $request,$_locale)
	{
	
		$this->loadDefaults($_locale,true);
		if($this->sponsorLogin){
			if(!$this->sponsor){
				return $this->redirect($this->generateUrl('sponsor_web_home_create'));
			}
		}
		
		// Get the payment ID before session clear
		$session = $request->getSession();
	  
		
		$payment_id = $session->get('paypal_payment_id');
		$currencyCode = $session->get('currencyCode');
		$amount = $session->get('amount');

		// clear the session payment ID
		$session->remove('paypal_payment_id');
		$session->remove('currencyCode');
		$session->remove('amount');

		$payerId = $request->query->get('PayerID');
		$token =   $request->query->get('token');

		if (empty($payerId) || empty($token)) {
			return $this->render('AppFrontendBundle:Default:error.html.twig', $this->webconf);		  
		} else if(!$payment_id){
			return $this->render('AppFrontendBundle:Default:error.html.twig', $this->webconf); 
		}

		$api = $this->createPayPalApiCtx();
		$payment = Payment::get($payment_id,$api);

		// PaymentExecution object includes information necessary 
		// to execute a PayPal account payment. 
		// The payer_id is added to the request query parameters
		// when the user is redirected from paypal back to your site
		$execution = new PaymentExecution();
		$execution->setPayerId($payerId);

		//Execute the payment
		$result = $payment->execute($execution, $api);

		//echo '<pre>';print_r($result);echo '</pre>';exit; // DEBUG RESULT, remove it later

		if ($result->getState() == 'approved') { // payment made
			
			// Registrar el pedido --- ok
			// Eliminar carrito 
			// Enviar correo a user
			// Enviar correo a admin
			// Redireccionar

			//$this->saveOrder(\Session::get('cart'));

			$don = new Donation();
			if(isset($this->sponsor)){
				$don->setUser($this->sponsor);
			}
			
			$don->setAmount($amount);
			$don->setCurrency($currencyCode);

			$em = $this->getDoctrine()->getManager();
			$em->persist($don);
			$em->flush();

			
			return $this->render('AppFrontendBundle:Default:thanks.html.twig', $this->webconf);
		}
		
		return $this->redirect($this->generateUrl('web_home'));
	}

	/**
	 * @Route("/donar/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="donate")
	 */
	public function donateAction(Request $request,$_locale)
	{
	
		$this->loadDefaults($_locale,true);
		if($this->sponsorLogin){
			if(!$this->sponsor){
				return $this->redirect($this->generateUrl('sponsor_web_home_create'));
			}
			$this->webconf['amountMonthly'] = $this->sponsor->getAmountMonthly();
		}
		
		$this->loadCurrencies();
		$this->loadQtys();
		/*
		$this->loadBanners();
		$this->loadKids();
		$this->loadHelperBlock();
		$this->loadCenterCharacteristic();
		$this->loadCenterAspiration();
		$this->processForm($request);
		*/	  
		
		return $this->render('AppFrontendBundle:Default:donate.html.twig', $this->webconf);		   
	}

	
	/**
	 * @Route("/submitContactInfo/{_locale}", defaults={"_locale": "es"}, requirements={"_locale": "en|es"}, name="submit_contact_info")
	 * @Method("POST")
	 */
	public function contactAction(Request $request,$_locale){

		$this->processForm($request);
		//return new Response(json_encode($request),200,array('Content-Type' => 'application/json'));
		return $this->render('AppFrontendBundle:Default:contact-form.html.twig', $this->webconf);

	}
	
	/**
	 * @Route("/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="web_home")
	 */
	public function indexAction(Request $request,$_locale)
	{
		
		
		$this->loadDefaults($_locale,true);
		if($this->sponsorLogin){
			if(!$this->sponsor){
				return $this->redirect($this->generateUrl('sponsor_web_home_create'));
			}
		}
		
		$this->loadBanners();
		$this->loadKids();
		$this->loadRandomKids();
		$this->loadHelperBlock();
		$this->loadCenterCharacteristic();
		$this->loadCenterAspiration();
		$this->processForm($request);
		
		return $this->render('AppFrontendBundle:Default:index.html.twig', $this->webconf);
	}

	/**
	 * @Route("/pagina/{slug}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","slug":"\w+(-\w+)*"}, name="load_page")
	 */
	public function loadPage(Request $request,$slug,$_locale)
	{
		$this->loadDefaults($_locale,true);
		$this->loadBanners();

		$em = $this->getDoctrine()->getManager();
		$article = $em->getRepository('AppBackendBundle:Page')->findOneBySlug($slug);

		if (!$article) {
			throw $this->createNotFoundException("Unable to find Article width $slug.");
		}

		$content = $article->getContent();
		$this->webconf['article'] = $article;
		$this->webconf['content'] = $content;

		return $this->render('AppFrontendBundle:Default:page.html.twig', $this->webconf);
	}
}
