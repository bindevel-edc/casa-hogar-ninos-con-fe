<?php

namespace App\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Email;

use Admin\CoreBundle\Form\Type\IconButtonType;
use App\BackendBundle\Entity\Sponsor;
use App\BackendBundle\Form\SponsorType;
use App\BackendBundle\Entity\Kid;


use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

use App\BackendBundle\Entity\VisitCounter;
use Symfony\Component\HttpFoundation\Session\Session;

class SponsorController extends Controller
{

	public function setContainer(ContainerInterface $container = null)
	{

		setlocale(LC_ALL, 'es_ES');
		$this->container = $container;
		$this->loadConf();

	}

	public function loadConf(){

		$Conf = $this->getDoctrine()
				->getRepository('AdminCoreBundle:Config')
				->findAll();

		$em = $this->getDoctrine()->getManager();

		
		if($Conf){
			
			$this->loadTwig();
			$this->webconf=array();

			foreach($Conf as $item){
				$this->webconf[$item->getVarname()]= $this->myTwig->render($item->getValue(),$this->webconf);
			} 
			
			if(isset($this->webconf['SiteOffline'])){
				$this->SiteOffline = $this->webconf['SiteOffline'];
			}

		} else {
			$this->webconf=array();
		}

		$month = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		$mtext = ((int) date('d')).' '.$month[date('n')-1] ;

		$this->webconf['month'] = $mtext;
		$this->webconf['last_username'] = '';
		$this->webconf['error'] = '';

	}

	private function loadDefaults($_locale){

		/*
		$bl = 'es';
		try {
			$bl = $this->getUserBaseLanguage()[0]->base;
		} catch(\Exception $e){
		
		}

		*/
		
		if($_locale == 'es'){
			setlocale(LC_ALL, 'es_ES');
		} else {
			setlocale(LC_ALL, 'en_US');
		} 

		$this->locale = $_locale;
		$this->webconf['AppLocale'] = $_locale;
        
        $user = $this->getUser();
        $email = $user->getEmail();
        $this->webconf['userEml'] = $email;
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('App\BackendBundle\Entity\Sponsor')->findOneByEmail($email);
        
        $this->sponsor = $entity;
        $this->webconf['sponsor'] = $entity;
    



	}

	public function loadTwig(){
		
		if(!isset($this->myTwig)){
			$loader = new \Twig_Loader_String();
			$this->myTwig = new \Twig_Environment($loader);
			$this->myTwig->addExtension(new \Symfony\Bundle\TwigBundle\Extension\AssetsExtension($this->container));
			$this->myTwig->addExtension(new \Symfony\Bundle\TwigBundle\Extension\ActionsExtension($this->container));
			$this->myTwig->addExtension(new \Symfony\Bridge\Twig\Extension\RoutingExtension($this->get('router')));
			$this->myTwig->addExtension(new \Symfony\Bridge\Twig\Extension\HttpKernelExtension($this->get('fragment.handler')));
		}
	}

	/**
	 * Creates a form to create a Banner entity.
	 *
	 * @param Banner $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createCreateForm(Sponsor $entity)
	{
		$form = $this->createForm(new SponsorType(), $entity, array(
			'action' => $this->generateUrl('sponsor_web_home_create'),
			'method' => 'POST',
			'attr' => array(
					'id' => 'IdCreateFormSponsor'
			)
		));

		$form->add('add', new IconButtonType() , array(
			'label' => 'Actualizar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));

		
		return $form;
	}
	
	/**
	 * Creates a form to edit a Sponsor entity.
	 *
	 * @param Sponsor $entity The entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createEditForm(Sponsor $entity)
	{
		$form = $this->createForm(new SponsorType(true), $entity, array(
			'action' => $this->generateUrl('sponsor_web_home_update'),
			//'method' => 'PUT',
			'attr' => array(
					'id' => 'IdEditFormSponsor'
			),
		));

		$form->add('save', new IconButtonType(), array(
			'label' => 'Actualizar',
			'attr' => array('class' => 'btn btn-icon btn-primary action-btn','type' => 'submit'),
			'icon' => 'fa fa-save',
		));
		
		$this->webconf['edit_mode'] = true;
		
		return $form;
	}
    

	/**
	 * Create Sponsor .
	 * @Route("/perfil/crear/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"},name="sponsor_web_home_create") 
	 */
	public function createAction(Request $request,$_locale)
	{

        $this->loadDefaults($_locale);
    
        if($this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_edit'));
        }
    
		$entity = new Sponsor();
        $entity->setEmail($this->webconf['userEml']);

		$validator = $this->get('validator');
		$metadata = $validator->getMetadataFor($entity);
		$metadata->addPropertyConstraint('photo', new NotBlank());


		$form = $this->createCreateForm($entity);
		$form->handleRequest($request);

		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('sponsor_web_home_edit'));
		}
		
		
		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $form->createView();

		return $this->render("AppFrontendBundle:Sponsor:create.html.twig",$this->webconf);

	}
    
	/**
	 * Create a edit for the entity Sponsor.
	 * @Route("/perfil/editar/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"},name="sponsor_web_home_edit") 
	 */
	public function editAction(Request $request,$_locale)
	{

        $this->loadDefaults($_locale);
        if(!$this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_create'));        
        }
        
        $entity = $this->sponsor;
        
		$editForm = $this->createEditForm($entity);

		$this->webconf['entity'] = $entity;
		$this->webconf['form'] = $editForm->createView();

		return $this->render("AppFrontendBundle:Sponsor:edit.html.twig",$this->webconf);

	}    

	/**
	 * Edits an existing Sponsor entity.
	 * @Route("/perfil/update/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"},name="sponsor_web_home_update") 
	 */
	public function updateAction(Request $request,$_locale)
	{

        $this->loadDefaults($_locale);
        if(!$this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_create'));        
        }
        
        $entity = $this->sponsor;
		$entity2 = clone $entity;
		
		$editForm = $this->createEditForm($entity);
		$editForm->handleRequest($request);

		if ($editForm->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $this->getUser()->setEmail($entity->getEmail());
			$em->flush();

			return $this->redirect($this->generateUrl('sponsor_web_home'));
		}

		unset($entity);

		$this->webconf['entity'] = $entity2;
		$this->webconf['form'] = $editForm->createView();
		
		return $this->render("AppFrontendBundle:Sponsor:edit.html.twig",$this->webconf);

	}
    
	/**
	 * Lists all Kid .
	 * @Route("/perfil/chicos/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="sponsor_web_home_kids")
	 */
	public function kidsAction(Request $request,$_locale)
	{
	
		$this->loadDefaults($_locale);
        if(!$this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_create'));        
        }        
        
		$em = $this->getDoctrine()->getManager();
		//try {
			
            
            $kids = $this->sponsor->getKids()->toArray();
            $ids = array_map(function($n)
            {
                return $n->getId();
            },$kids);

            if(count($ids) > 0){
                $ids = '('. implode(',',$ids) .')';
                $query = $em->createQuery("SELECT k FROM AppBackendBundle:Kid k WHERE k.id NOT IN $ids");
                $entities = $query->getResult();                
            } else {
                $entities = $em->getRepository('App\BackendBundle\Entity\Kid')->findAll();
            }

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			$ObjList->message= '';
			//$ObjList->dql= $dql;
			
		/*} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->data = array();
			$ObjList->success = false;
		}*/
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppFrontendBundle:Sponsor:kids.html.twig",$this->webconf);
	}
    
	/**
	 * Lists all Donation .
     * @Route("/perfil/donaciones/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="sponsor_web_home_donation_list")
	 */
	public function donationsListAction(Request $request,$_locale)
	{
	
		$this->loadDefaults($_locale);
        
        if(!$this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_create'));        
        }  
        
		$em = $this->getDoctrine()->getManager();
		
		try {
			
			$entities = $em->getRepository('App\BackendBundle\Entity\Donation')->findByUser($this->sponsor);
		

			$ObjList = new \stdClass();
			$ObjList->data = array();
			$ObjList->success = true;

			if($entities){
				foreach($entities as $item){
					array_push($ObjList->data,$item->toObject());
				}
			}
			
			$ObjList->total= count($ObjList->data);
			//$ObjList->total= $res['Total'];
			$ObjList->message= '';
			//$ObjList->dql= $dql;
			
		} catch(\Exception $e) {
			$ObjList = new \stdClass();
			$ObjList->message = $e->getMessage();
			$ObjList->success = false;
		}
		
		$this->webconf['ObjList'] = $ObjList;

		return $this->render("AppFrontendBundle:Sponsor:donations.html.twig",$this->webconf);
	}
    
	/**
	 * Add Childs to Sponsor.
	 * @Route("/perfil/adicionar/{id}/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es","id" = "\d+"},name="sponsor_web_home_add_child") 
	 */
	public function addKidAction(Request $request,$id,$_locale)
	{

        $obj = new \stdClass();
        $obj->success = true;
        
		try {
        
            $this->loadDefaults($_locale);
            if(!$this->sponsor){
                throw $this->createNotFoundException("Unable to find Sponsor width $id.");
            }
            
            $em = $this->getDoctrine()->getManager();
            $kid = $em->getRepository('App\BackendBundle\Entity\Kid')->find($id);

            if (!$kid) {
                throw $this->createNotFoundException("Unable to find Kid width $id.");
            }
            
            $this->sponsor->addKid($kid);
            $em->flush();
        
		}catch(\Exception $e){
			$obj->message = $e->getMessage();
			$obj->success = false;
		}
		
		return new JsonResponse($obj);        
        
    }
    
	/**
	 * @Route("/perfil/{_locale}",defaults={"_locale": "es"},requirements={"_locale": "en|es"}, name="sponsor_web_home")
	 */
	public function indexAction(Request $request,$_locale)
	{
		
		$this->loadDefaults($_locale);
        if(!$this->sponsor){
            return $this->redirect($this->generateUrl('sponsor_web_home_create'));        
        }        
		
		return $this->render('AppFrontendBundle:Sponsor:index.html.twig', $this->webconf);
	}
}
