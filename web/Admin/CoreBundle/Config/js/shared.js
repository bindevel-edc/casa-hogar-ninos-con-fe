jQuery(document).ready(function(){

    $('#admin_corebundle_config_type').change(function() {
        
        var type = "";  
        $("select option:selected").each(function () {
            type += $(this).text();  
        });    
    
        var id = 'admin_corebundle_config_value';
        var name = 'admin_corebundle_config[value]';
        var value = $('#'+id).val();

        var el = $('#'+id);
        var parent = el.parent();

        var html = '';
                
        var el2 = $("input.spinner-both");
        if(el2.length > 0){
            parent = parent.parent();
            el2 = $("input.spinner-both")[0];
        } else if($("div.slider-horizontal").length > 0){
            console.log(3,$("div.slider-horizontal").length);
            parent = parent.parent();
            el2 = null;    
        }        

                
        if(type == 'checkbox'){

            html = '<label class="checkbox">';
            html += '   <input type="'+ type +'" id="'+ id +'" name="' + name + '" data-validation="true" required="required" checked><i></i>';
            html += '</label>';
            parent.replaceWith(html);
            
        } else if(type == 'integer' || type == 'number'){
            
            html += '   <div class="form-group col col-6">';
            html += '       <input class="form-control spinner-both"  id="'+id+'" name="'+name+'" value="0">';
            html += '   </div>';

            parent.replaceWith(html);
            $('#'+id).spinner();
            
        } else if(type == 'date'){
            
            html = '<div class="input-group">';
            html += '    <input type="text" id="'+ id +'" name="' + name + '" placeholder="Select a date" class="form-control datepicker" data-dateformat="dd/mm/yy">';
            html += '    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
            html += '</div>';
            el.replaceWith(html);
            $('#'+id).datepicker();

        } else if(type == 'time'){
        
        } else if(type == 'percent'){
        
            html = '<div class="col col-6">';

            html += '<input type="text"';
            html += '    class="slider slider-primary"';
            html += '    id="'+ id +'"';
            html += '    name="'+ name +'"';
            html += '    data-slider-min="0" ';
            html += '    data-slider-value="50" ';
            html += '    data-slider-max="100"';
            html += '    data-slider-selection = "before"';
            html += '    data-slider-handle="round">';
            
            html += '</div>';
            parent.replaceWith(html);
            
		    $('#'+id).slider();
            
        
        } else if(type == 'textarea'){
            html = '<label class="textarea textarea-resizable">';
            html += '   <textarea id="'+ id +'">' + value + '</textarea>';
            html += '</label>';
            parent.replaceWith(html);
        } else if(type == 'html'){
            html = '<label class="textarea">';
            html += '   <textarea id="'+ id +'">' + value + '</textarea>';
            html += '</label>';
            parent.replaceWith(html);
            $('#'+id).ckeditor({});
        } else {
            html = '<label class="input">';
            html += '   <input type="'+ type +'" id="'+ id +'" name="' + name + '" data-validation="true" required="required">';
            html += '</label>';
            parent.replaceWith(html);
        }

        if(el2){
            el2.remove();
        }
        
        $('#'+id).focus();
        
        /*
        $('#timepicker').timepicker();
        <div class="input-group">
            <input class="form-control" id="timepicker" type="text" placeholder="Select time">
            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
        </div>        
        */
        
       

    });
    

});
