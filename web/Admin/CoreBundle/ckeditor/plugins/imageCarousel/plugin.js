
CKEDITOR.plugins.add( 'imageCarousel',
{
	
	init: function( editor )
	{
		editor.addCommand( 'imageCarouselDialog', new CKEDITOR.dialogCommand( 'imageCarouselDialog' ) );
 
		editor.ui.addButton( 'imageCarousel',
		{
			label: 'Insertar carrusel de imagenes',
			command: 'imageCarouselDialog',
			icon: this.path + 'carousel.png'
		} );
 
		CKEDITOR.dialog.add( 'imageCarouselDialog', function( editor )
		{
			
			return {
				title : 'Carrusel de imagenes',
				minWidth : 400,
				minHeight : 200,
				contents :
				[
					{
						id : 'general',
						label : 'Settings',
						elements :
						[
							{
								type : 'html',
								html : 'Seleccione una galer&iacute;a de imagenes.'		
							},{
								type : 'select',
								id : 'corouselName',
								label : 'Galería',
								items : 
								[
									[ '<none>', '-1' ],
									[ 'Galería 1', '1' ],
									[ 'Galería 2', '2' ],
									[ 'Galería 3', '3' ]
								],
								commit : function( data )
								{
									data.galleryId = this.getValue();
								}
							}
						]
					}
				],
				onOk : function()
				{
					var dialog = this,data = {};

					this.commitContent( data );
					
					editor.insertHtml( '{carousel}' + data.galleryId + '{/carousel}' );
				}
				,onShow : function(){
					var dialog = this;

					$.getJSON( "/manage/unitec/adminbundle/imagegallery/list?json=1",{})
					.done(function( response , textStatus, jqXHR ){
						if(response.success) {
							$('select.cke_dialog_ui_input_select option').remove();
							var categories = response.data;
							for(cat in categories){
								$('select.cke_dialog_ui_input_select').append('<option value="'+categories[cat].id+'">'+categories[cat].name+'</option>');
							}
						} else {
							alert('Error al cargar la información del servidor')
						}
					})
					.fail(function( jqXHR, textStatus, errorThrown ) {
						alert('Error al cargar la información del servidor:\n'+textStatus);
					});

				}
			};
		});
	}
});

