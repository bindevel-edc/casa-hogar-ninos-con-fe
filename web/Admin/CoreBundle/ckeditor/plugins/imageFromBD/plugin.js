
CKEDITOR.plugins.add( 'imageFromBD',
{
	
	init: function( editor )
	{
		editor.addCommand( 'imageFromBDDialog', new CKEDITOR.dialogCommand( 'imageFromBDDialog' ) );
 
		var imgPath = this.path + 'actions-insert-image-icon.png';

		editor.ui.addButton( 'imageFromBD',
		{
			label: 'Insertar imagen',
			command: 'imageFromBDDialog',
			icon: imgPath
		} );
		
		
		CKEDITOR.dialog.add( 'imageFromBDDialog', function( editor )
		{
			
			return {
				title : 'Galería de imagenes',
				minWidth : 400,
				minHeight : 200,
				contents :
				[
					{
						id : 'general',
						label : 'Settings',
						elements :
						[
							{
								type : 'html',
								html : '<center><img id="idImageViewer" src="'+imgPath+'" style="max-height: 400px;max-width: 400px;"></center>'
							},{
								type : 'select',
								id : 'galleryName',
								label : 'Galería',
								items : 
								[
									[ '<none>', '-1' ],
								],
								commit : function( data )
								{
									data.galleryId = this.getValue();
								}
							}
						]
					}
				],
				onOk : function()
				{
					var dialog = this,data = {};

					this.commitContent( data );
					
					editor.insertHtml( '{image}' + data.galleryId + '{/image}' );
				}
				,onShow : function(){
					var dialog = this;

					$.getJSON( "/manage/listAllImgs",{})
					.done(function( response , textStatus, jqXHR ){
						if(response.success) {
							$('select.cke_dialog_ui_input_select option').remove();
							var imgCat = response.data;
							console.dir(response);
							var first = false;
							for(cat in imgCat){
								//$('select.cke_dialog_ui_input_select').append('<option value="'+categories[cat].id+'">'+categories[cat].name+'</option>');
								var ctn = $('<optgroup style="font-weight: bold;font-size: 14px;" label="'+cat+'"></optgroup>').appendTo('select.cke_dialog_ui_input_select');
								for(img in imgCat[cat]){
									var obj = imgCat[cat][img];
									$(ctn).append('<option style="padding-left:10px;" value="'+obj.id+','+obj.type+'" data-src="'+obj.src+'">'+obj.name+'</option>');
									if(!first){
										$('#idImageViewer').attr("src", obj.src);
										first = true;
									}
								}
							}
						} else {
							alert('Error al cargar la información del servidor')
						}
					})
					.fail(function( jqXHR, textStatus, errorThrown ) {
						alert('Error al cargar la información del servidor:\n'+textStatus);
					});
					
					$('select.cke_dialog_ui_input_select').bind("keydown change", function() {
						var src = $(this.selectedOptions[0]).data("src");
						$('#idImageViewer').attr("src", src);
					});
					//

				}
			};
		});
	}
});

