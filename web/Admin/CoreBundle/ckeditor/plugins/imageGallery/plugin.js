
CKEDITOR.plugins.add( 'imageGallery',
{
	
	init: function( editor )
	{
		editor.addCommand( 'imageGalleryDialog', new CKEDITOR.dialogCommand( 'imageGalleryDialog' ) );
 
		editor.ui.addButton( 'imageGallery',
		{
			label: 'Insertar a galeria de imagenes',
			command: 'imageGalleryDialog',
			icon: this.path + 'gallery.gif'
		} );
 
		CKEDITOR.dialog.add( 'imageGalleryDialog', function( editor )
		{
			
			return {
				title : 'Galería de imagenes',
				minWidth : 400,
				minHeight : 200,
				contents :
				[
					{
						id : 'general',
						label : 'Settings',
						elements :
						[
							{
								type : 'html',
								html : 'Seleccione una galer&iacute;a de imagenes.'		
							},{
								type : 'select',
								id : 'galleryName',
								label : 'Galería',
								items : 
								[
									[ '<none>', '-1' ]
								],
								commit : function( data )
								{
									data.galleryId = this.getValue();
								}
							}
						]
					}
				],
				onOk : function()
				{
					var dialog = this,data = {};

					this.commitContent( data );
					
					editor.insertHtml( '{gallery}' + data.galleryId + '{/gallery}' );
				}
				,onShow : function(){
					var dialog = this;

					$.getJSON( "/manage/galleryList",{})
					.done(function( response , textStatus, jqXHR ){
						try {
							if(response.success) {
								$('select.cke_dialog_ui_input_select option').remove();
								var galleries = response.data;
								for(gal in galleries){
									$('select.cke_dialog_ui_input_select').append('<option value="'+galleries[gal]+'">'+galleries[gal]+'</option>');
								}
							} else {
								alert('Error al cargar la información del servidor')
							}
						} catch (e){
								alert('Error al cargar la información del servidor\nDatos erroneos.')
						}
						
					})
					.fail(function( jqXHR, textStatus, errorThrown ) {
						alert('Error al cargar la información del servidor:\n'+textStatus);
					});

				}
			};
		});
	}
});

