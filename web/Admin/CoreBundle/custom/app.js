$(document).ready(function(){

	$(".btn_cancel").click(function(event){
		if($('#backBtn').length > 0){
            $('#backBtn')[0].click();
        } else {
            var loc = $('#IdBackRoute').val();
            location.href = loc;
        }
	});
	
	$("button.close").click(function(event){
        if($('#backBtn').length > 0){
            $('#backBtn')[0].click();
        } else {
            var loc = $('#IdBackRoute').val();
            location.href = loc;
        }
	});

	$('.useDatepicker').datepicker({
		buttonImageOnly: true, 
		dateFormat: 'dd-mm-yy',
		yearRange: "-0:+1"  
	});
	
	$('[data-translatable=true]').each(function(){
	  $("label[for='"+this.id+"']").prepend('<i title="Este campo es multi-idioma" class="fa fa fa-comments"></i>');
	});

});