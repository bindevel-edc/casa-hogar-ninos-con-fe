	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

	pageSetUp();

	/*
	 * PAGE RELATED SCRIPTS
	 */

	// pagefunction

	var pagefunction = function() {

		$(".js-status-update a").click(function () {
		    var selText = $(this).text();
		    var $this = $(this);
		    $this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
		    $this.parents('.dropdown-menu').find('li').removeClass('active');
		    $this.parent().addClass('active');
		});

		/*
		 * RUN PAGE GRAPHS
		 */

		// Load FLOAT dependencies (related to page)
		loadScript("/Theme/Backend/js/plugin/chart.js/Chart.min.js", generatePageGraphs);

        function rndColor(){
            return 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')';
        }
        
		function generatePageGraphs() {
        
            
		    $(function () {

                var statisticsUrl = $('#statisticsUrl').val();
                
                var renderGraph = function(id,labels,data,label,bgColors){
                
                    var ctx = $(id);
                    var Chart1 = new Chart(ctx, {
                        type: 'bar',
                        data: {
                            labels: labels,
                            datasets: [{
                                label: label,
                                data: data,
                                backgroundColor: bgColors,
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            maintainAspectRatio: true,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });                 
                
                }
                
                $.ajax({
                  url: statisticsUrl,
                }).done(function(data) {

                    $('#visit-today').text(data.today + ' Visitas');
                    $('#visit-month').text(data.month + ' Visitas');
                    $('#visit-year').text(data.year + ' Visitas');
                    
                   var osInfo = data.osInfo;
                   var browserInfo = data.browserInfo;
                   var deviceInfo = data.deviceInfo;
                   
                   var values = new Array();
                   var bgColors = new Array();
                   
                   var labels  = osInfo.map(function(item) {
                      values.push(item.val);
                      bgColors.push(rndColor());
                      return item.os;
                   });

                   renderGraph("#Chart-1",labels,values,'Visitas',bgColors); 
                   
                   values = new Array();
                   bgColors = new Array();
                   
                   labels  = browserInfo.map(function(item) {
                      values.push(item.val);
                      bgColors.push(rndColor());
                      return item.navigator;
                   });

                   renderGraph("#Chart-2",labels,values,'Visitas',bgColors);  
                   
                   values = new Array();
                   bgColors = new Array();
                   
                   labels  = deviceInfo.map(function(item) {
                      values.push(item.val);
                      bgColors.push(rndColor());
                      return item.device;
                   });

                   renderGraph("#Chart-3",labels,values,'Visitas',bgColors);
                   
                   
                });                
                               

		    });

		}


	};

	// end pagefunction

	// run pagefunction on load
	pagefunction();