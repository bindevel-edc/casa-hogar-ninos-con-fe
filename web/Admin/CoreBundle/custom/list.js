function getUrlParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) 
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) 
		{
			return sParameterName[1];
		}
	}
	
	return undefined;
}


$(document).ready(function(){

	var sort = getUrlParameter('sort');

	if(sort != undefined){
		$('select[name=sort]').val(sort);
	}

	$('select[name=sort]').on('change', function(){
		var selected = $('select[name=sort] option:selected').val();
		alert(selected);
		var form = $(this).parents('form:first');
		form.submit();
	});

	/*
	$('select[name=sort]').on('click', function(){
		var form = $(this).parents('form:first');
		form.submit();
	});
	*/

});
