jQuery(document).ready(function(){

    if($.browser.webkit){
        var label = $('[for=app_backendbundle_centercharacteristic_icon]');
        label.prepend('<i></i>');

        $('#app_backendbundle_centercharacteristic_icon').change(function(){
            label.removeClass().addClass('fa '+this.value);
        });
    }

});


