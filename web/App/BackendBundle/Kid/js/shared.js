var DateDiff = {

    inDays: function(d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();

        return parseInt((t2-t1)/(24*3600*1000));
    },

    inWeeks: function(d1, d2) {
        var t2 = d2.getTime();
        var t1 = d1.getTime();

        return parseInt((t2-t1)/(24*3600*1000*7));
    },

    inMonths: function(d1, d2) {
        var d1Y = d1.getFullYear();
        var d2Y = d2.getFullYear();
        var d1M = d1.getMonth();
        var d2M = d2.getMonth();

        return (d2M+12*d2Y)-(d1M+12*d1Y);
    },

    inYears: function(d1, d2) {
        return d2.getFullYear()-d1.getFullYear();
    }
}

function dateDiffTostr(date1,date2) {


    var second=1000, minute=second*60, hour=minute*60, day=hour*24, week=day*7;
    var timediff = date2 - date1;

    if (isNaN(timediff)) return '-';
    
    var years =  date2.getFullYear() - date1.getFullYear();
    var months = (( date2.getFullYear() * 12 + date2.getMonth())- ( date1.getFullYear() * 12 + date1.getMonth() ));
    var weeks    = Math.floor(timediff / week);
    var days     =  Math.floor(timediff / day); 
    var hours    = Math.floor(timediff / hour); 
    var minutes  = Math.floor(timediff / minute);
    var seconds  = Math.floor(timediff / second);  

    var result = '';
    
    if(years > 0) {
        result += years + ((years > 1) ? ' años ' : ' año ');
    } else if(months > 0){
        result += months + ((months > 1) ? ' meses ' : ' mes ');
    } else if(weeks > 0) {
        result += weeks + ((weeks > 1) ? ' semanas ' : ' semana ');
    } else if (days > 0) {
        result += days + ((days > 1) ? ' días ' : ' día ');
    } else if (hours > 0) {
        result += hours + ((hours > 1) ? ' horas ' : ' hora ');
    }

    //if(result != )
    
    return result;
}

jQuery(document).ready(function(){

	bread_crumb = $("#ribbon ol.breadcrumb");
	bread_crumb.empty();
	bread_crumb.append($("<li>Administraci&oacute;n</li>"));
	bread_crumb.append($("<li>Editar</li>"));
	bread_crumb.append($("<li>Niños</li>"));
	
    
	$('.useDatepicker').datepicker({
		dateFormat : 'dd-mm-yy',
		prevText : '<i class="fa fa-chevron-left"></i>',
		nextText : '<i class="fa fa-chevron-right"></i>'
	});

    $('#app_backendbundle_kid_birthdate').change(function(e){
        
        var date = this.value;
        if(/^\d{2}-\d{2}-\d{4}$/.test(date)){
            
            var arrDate = date.split('-');
            var d1 = new Date(arrDate[2],arrDate[1],arrDate[0]);
            var d2 = new Date();
            var yearsOld = DateDiff.inYears(d1,d2);
            
            $('#app_backendbundle_kid_old').val(yearsOld);
        }
        
    });
    
    
    $('#app_backendbundle_kid_dateOfReception').change(function(e){
        
        var date = this.value;
        if(/^\d{2}-\d{2}-\d{4}$/.test(date)){
            
            var arrDate = date.split('-');
            var d1 = new Date(arrDate[2],arrDate[1],arrDate[0]);
            var d2 = new Date();
            var yearsOld = dateDiffTostr(d1,d2);
            
            $('#app_backendbundle_kid_timeInTheHouse').val(yearsOld);
        }
        
    });
    
    
    

});
