jQuery(document).ready(function(){

	var iconContainer = $("label[for=app_backendbundle_menu_icon]");
	var display = $(iconContainer).append('<i class="fa fa-fw"></i>').children('i')[0];
	var iconSelector = $('#app_backendbundle_menu_icon')[0];
	var cssClass = iconSelector.options[iconSelector.selectedIndex].value;
	$(display).attr('class','fa fa-fw ' + cssClass);

	$('#app_backendbundle_menu_icon').change(function(evt){
		cssClass = iconSelector.options[iconSelector.selectedIndex].value;
		$(display).attr('class','fa fa-fw ' + cssClass);
	});

	$('#app_backendbundle_menu_externalLink').click(function(){

		var obj = $('#app_backendbundle_menu_link')[0];
		//var tag = obj.tagName;
		//tag == 'SELECT'
		var value = obj.value;

		if(this.checked){
			$("#app_backendbundle_menu_link").parent().attr('class', 'input');
			$("#app_backendbundle_menu_link").replaceWith('<input type="text" value="'+value+'" data-validation="true" required="required" name="app_backendbundle_menu[link]" id="app_backendbundle_menu_link">');
		} else {

			$("#app_backendbundle_menu_link").parent().attr('class', 'select');
			var select = $("#app_backendbundle_menu_links").clone();

			select[0].id = 'app_backendbundle_menu_link';
			select[0].name = 'app_backendbundle_menu[link]';
			$(select).show();
			
			$("#app_backendbundle_menu_link").replaceWith(select);

		}

	});

	$('#app_backendbundle_menu_links').hide();
	$('label[for=app_backendbundle_menu_links]').hide();


});
