App = typeof(App) != "undefined" ? App  : {};
App.Commands = {

	commands:[]

	,register: function(cmd,name){
		
		if(typeof(cmd) != "function") return;
		if(typeof(name) != "string") return;
		
		this.commands[name] = cmd; 
	}

	,execute: function(cmd){
		if(typeof(cmd) != "string") return;
		if(this.commands.indexOf(cmd)) this.commands[cmd]();
	}
}

App.Commands.register(function(){
	var backRoute = $('#IdBackRoute');
	if(backRoute.length > 0){
		location.href = backRoute[0].value;
	}
},'goBack');

$('[data-command]').click(function(evt){
	evt.preventDefault();
	App.Commands.execute(this.dataset.command);
})