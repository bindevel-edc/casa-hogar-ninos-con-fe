$(document).ready(function() {

	pageSetUp();
	
	var responsiveHelper_data_store = undefined;
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};							
	
	/* COLUMN FILTER  */
	var otable = $('#data_store').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_data_store) {
				responsiveHelper_data_store = new ResponsiveDatatablesHelper($('#data_store'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_data_store.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_data_store.respond();
		},		
	
	});
	
		   
	// Apply the filter
	$("#data_store thead th input.form-control").on( 'keyup change', function () {
		
		otable
			.column( $(this).parent().index()+':visible' )
			.search( this.value )
			.draw();

	} );
	
	$('#delBtn').hide();
	
	/* END COLUMN FILTER */						
	
	var chkParent = $('th.sorting_asc:eq(0)');
	chkParent.removeClass('sorting_asc')
	
	var selector = '#data_store.table > thead > tr > th > label.checkbox > input';
	var selector2 = 'tr.selectable > td > label.checkbox > input';

	$(selector).change(function() {
		chkParent.removeClass('sorting_asc');
		chkParent.removeClass('sorting_desc');
	});

	$(selector).click(function(){
		 
		var chk = this;
		if(chk.checked){
			$('#delBtn').show();
		} else {
			$('#delBtn').hide();
		}
		   
		$(selector2).each(function(){
			$(this).prop('checked',chk.checked);
		});				
	});
	
	$(selector2).change(function(){
		
		if($(selector2+':checked').length > 0){
			$('#delBtn').show();
		} else {
			$('#delBtn').hide();
		}		

	});

});
