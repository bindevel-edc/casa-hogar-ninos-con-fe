function wordWrap(str, maxWidth) {
    var newLineStr = "\n"; done = false; res = '';
    do {                    
        found = false;
        // Inserts new line at first whitespace of the line
        for (i = maxWidth - 1; i >= 0; i--) {
            if (testWhite(str.charAt(i))) {
                res = res + [str.slice(0, i), newLineStr].join('');
                str = str.slice(i + 1);
                found = true;
                break;
            }
        }
        // Inserts new line at maxWidth position, the word is too long to wrap
        if (!found) {
            res += [str.slice(0, maxWidth), newLineStr].join('');
            str = str.slice(maxWidth);
        }

        if (str.length < maxWidth)
            done = true;
    } while (!done);

    return res + str;
}

function testWhite(x) {
    var white = new RegExp(/^\s$/);
    return white.test(x.charAt(0));
};

(function($){
	$(document).ready(function(){

		var banners = [];
		$(".img-banner").each(function(){
			var src = $(this).data('src');
			var title = $(this).data('title');  
			title = wordWrap(title,20);
			
			var lines = title.split('\n');
			title = '';
			var i = 0;
			lines.forEach(function(line){
				i++;
				title += "<p class=\"slogan-ln"+ i +"\">"+ line +"</p>";
			});
			
			banners.push({src:src,title:title});
		});
		
		var c = 0;
		var j = banners.length;
		if(banners.length > 1){

			//$("#banner-background").backstretch(banners[i].src);
			//$(".slogan").html(banners[i].title);

			var loaded = 0;
			for (var i = 0; i < j; i++) {

				var url = banners[i].src;
				var title = banners[i].title;

				var imageObj = new Image();
				imageObj.src = url;
				imageObj.onload= (function(){

					loaded++;
					if(loaded == j){
						setInterval(function(){

							if(c == j){
								c = 0;
							}

							//[0].src = banners[c].src
							//$("#banner-background").backstretch(banners[c].src);
							$("#banner-background").css('background-image', 'url(' + banners[c].src + ')');;
							$(".slogan").html(banners[c].title);
						
							c++;
						},5000);
					}

				});

			}


			
		} else if(banners.length == 1) {
			//$("#banner-background").backstretch(banners[0].src);
			$("#banner-background").css('background-image', 'url(' + banners[c].src + ')');;
		}

	}); // End document ready
})(this.jQuery);

