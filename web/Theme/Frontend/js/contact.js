jQuery(document).ready(function(){

	function setupSubmitContact(){

		$('#form_send').click(function(evt){

			var url = $('#submit_contact_info').val();
			var data = $('#footer-form').serialize();

			$.ajax({
				type: "POST",
				url: url,
				data: data,
				success: function(data){
					$("#contact-form-container").html(data);
					setupSubmitContact();
					if($('.help-block').length == 0){
						$("#form_name").val('');
						$("#form_email").val('');
						$("#form_message").val('');
					}
				}
			});
			
		});

	}
	
	setupSubmitContact();

});