//MDB anims
wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true // default
});
wow.init();

// Responsive slick
$(document).on('ready', function () {
    $(".life").slick({  
        prevArrow: "<button type='button' data-role='none' class='slick-prev slick-arrow blue-outline-prev' aria-label='Previous' role='button' style='display: block;'>&lt;</button>",
        nextArrow: "<button type='button' data-role='none' class='slick-next slick-arrow blue-outline-next' aria-label='Next' role='button' style='display: block;'>&gt;</button>",
        dots: false,
        infinite: true,
        centerMode: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {

                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 550,
                settings: {
                    centerPadding: '10%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $(".lifeInCenter").slick({   
        prevArrow: "<button type='button' data-role='none' class='slick-prev slick-arrow blue-outline-prev' aria-label='Previous' role='button' style='display: block;'>&lt;</button>",
        nextArrow: "<button type='button' data-role='none' class='slick-next slick-arrow blue-outline-next' aria-label='Next' role='button' style='display: block;'>&gt;</button>",
        dots: false,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [                
            {
                breakpoint: 550,
                settings: {
                    centerPadding: '10%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $(".donationsSlick").slick({   
        prevArrow: "<button type='button' data-role='none' class='slick-prev slick-arrow white-outline-prev' aria-label='Previous' role='button' style='display: block;'>&lt;</button>",
        nextArrow: "<button type='button' data-role='none' class='slick-next slick-arrow white-outline-next' aria-label='Next' role='button' style='display: block;'>&gt;</button>",
        dots: false,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        responsive: [                
            {
                breakpoint: 550,
                settings: {
                    centerPadding: '10%',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});