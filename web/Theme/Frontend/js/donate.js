// Allows only numbers 1 decimal point followed by two numbers
function getKey(e) {
  if (window.event)
    return window.event.keyCode;
  else if (e)
    return e.which;
  else
    return null;
}

function restrictChars(e, obj) {
  var CHAR_AFTER_DP = 2; // number of decimal places   
  var validList = "0123456789."; // allowed characters in field   
  var key, keyChar;
  key = getKey(e);
  if (key == null) return true;
  // control keys   
  // null, backspace, tab, carriage return, escape   
  if (key == 0 || key == 8 || key == 9 || key == 13 || key == 27)
    return true;
  // get character   
  keyChar = String.fromCharCode(key);
  // check valid characters   
  if (validList.indexOf(keyChar) != -1) {
    // check for existing decimal point   
    var dp = 0;
    if ((dp = obj.value.indexOf(".")) > -1) {
      if (keyChar == ".")
        return false; // only one allowed   
      else {
        // room for more after decimal point?   
        if (obj.value.length - dp <= CHAR_AFTER_DP)
          return true;
      }
    } else return true;
  }
  // not a valid character   
  return false;
}
// PayPal Donate Form Validation
$.validator.addMethod('minStrict', function(value, el, param) {
  return value > param;
});

$(function() {

  // Setup form validation on the #register-form element
  $("#paypal").validate({

    // Specify the validation rules
    rules: {
      amount: {
        required: true,
        minStrict: 4.99 // Set Minimum Amount
      },
      currency_code: {
        required: true
      }
    },
    // Specify the validation error messages
    messages: {
      amount: {
        required: "Please enter amount",
        minStrict: "Minimum Amount 5.00", // Minimum Amount Message
      },
      currency_code: {
        required: "Please select currency"
      },

    },

    submitHandler: function(form) {
      form.submit();
    }
  });

});
// Tooltip show gift aid message
$(function() {
    $(document).tooltip({
      open: function(event, ui) {
        ui.tooltip.delay(8000).fadeTo(2000, 0);
      }
    });
});
// Select One Time Donation - Own, 10, 25, 50
$(document).ready(function a(){
    $("#paypal input[type=radio]").click(function(){
       $("#amount").val(this.value);
    }); 
});