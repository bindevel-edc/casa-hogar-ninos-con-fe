jQuery(document).ready(function(){

	var allow = $('#is_admin').length > 0;
	var for_fail = $('#for_fail').length > 0;

	if(for_fail){
		location.href = '#contacto';
	} else {
		$('#form_name').val('');
		$('#form_email').val('');
		$('#form_message').val('');
	}
	
	if(allow){

		var lang = location.pathname;
		if(lang.startsWith('/app_dev.php/')){
			lang = '/';
		} else if(lang.startsWith('/app_dev.php')){
			lang = '/';
		}

		if(lang == '/'){
			lang = '/es';
		}


		/*$('.editable-content').each(function(){
		
			var obj = $(this);
			var entity = obj.data('entity');
			var property = obj.data('property');
			var id = obj.data('id');

			obj.editable({
			   url: '/update/' + entity + '/' + property + '/' + id + lang
			   ,type: 'text'
			   ,pk: id
			   ,showbuttons:'bottom'
			});		   
			
		});
		*/

		CKEDITOR.dtd.$editable.span = 1
		CKEDITOR.dtd.$editable.a = 1



		$('.editable-content').each(function(){

			this.setAttribute( 'contenteditable', true );
			var editor = CKEDITOR.inline(this, {
				startupFocus: true
				,filebrowserBrowseUrl:'/filemanager/dialog.php?type=2&editor=ckeditor&fldr='
				,filebrowserUploadUrl:'/filemanager/dialog.php?type=2&editor=ckeditor&fldr='
				,filebrowserImageBrowseUrl:'/filemanager/dialog.php?type=1&editor=ckeditor&fldr='
			});

			var obj = $(this);
			var entity = obj.data('entity');
			var property = obj.data('property');
			var id = obj.data('id');
			var url = location.href;
			
			if(!url.endsWith('/')){
				url = '/' + url;
			}
			url = '/update/' + entity + '/' + property + '/' + id + lang;
			url = url.replace('/pagina/','/');

			editor.addCommand("saveCommand", { // create named command
				exec: function(edt) {

					var jqxhr = $.ajax({
						url:url
						,dataType: "json"
						,method:'POST'
						,data: { value: edt.getData() }
					})
					.fail(function() {
						alert( "error" );
					}).success(function(data){
						var info = data;
						try {
							alert(info.message);
						} catch(e) {
							alert('Error al guardar.');
						}
					});

				}
			});

			editor.ui.addButton('SuperButton', { // add new button and bind our command
				label: "Guardar",
				command: 'saveCommand',
				toolbar: 'clipboard',
				icon: '/Theme/FrontendAdmin/ckeditor/plugins/icons/save.png'
			});

		});


	}
   

});

