Ext.ns("System.Component.Validator");

Ext.apply(Ext.form.VTypes,{
	rules: function(val, field){
		try{
			
			
			if(field.rules instanceof Array){

				var value = field.getValue();
				rules = field.rules;
				var l = rules.length;
				
				for(i=0;i<l;i++){
					if(!rules[i].validate(value)){
						this.rulesText = rules[i].message;
						return false;
					}	
				}
			
			}
			return true;
			
		}catch(e){
			this.rulesText = e.message;
			return false;
		}
	},
	rulesText: '', 
});

System.Component.Validator.Validator = Ext.extend(Object,{
	message : "",
	constructor : function(options){
		Ext.apply(this,options || {});
	},
	
	validate : function(value){
		//implement
		return true;
	},
	
});

System.Component.Validator.NotBlank = Ext.extend(System.Component.Validator.Validator,{
	
	message : "This value should not be blank.",
	
	constructor : function(options){
		System.Component.Validator.NotBlank.superclass.constructor.call(this,options);
	},
	
	validate : function(value){
		//System.Component.Validator.NotBlank.superclass.validate.call(this,value);
		if (false === value || value.length < 1 ) {
            return false;
        }
		
		return true;
	},
	
});

System.Component.Validator.NotNull = Ext.extend(System.Component.Validator.Validator,{
	message : "This value should not be null.",
	
	constructor : function(options){

		System.Component.Validator.NotNull.superclass.constructor.call(this,options);
	},
	
	validate : function(value){
		//System.Component.Validator.NotNull.superclass.validate.call(this,value);
        if ((null === value) || (value == undefined)) {
            return false;
        }		
		return true;
	},
	
});

System.Component.Validator.Email = Ext.extend(System.Component.Validator.Validator,{
	
	message : "This value is not a valid email address."
	,email : /^(\w+)([\-+.][\w]+)*@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/
	
	,constructor : function(options){
		System.Component.Validator.Email.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Email.superclass.validate.call(this,value);
		return this.email.test(value);	
	}
	
});

System.Component.Validator.Length = Ext.extend(System.Component.Validator.Validator,{
	
	message : "This value is not valid."
	,maxMessage : 'This value is too long. It should have %d character or less.\nThis value is too long. It should have %d characters or less.'
	,minMessage : 'This value is too short. It should have %d character or more.\nThis value is too short. It should have %d characters or more.'
	,exactMessage : 'This value should have exactly %d character.\nThis value should have exactly %d characters.'
	,max : null
	,min : null
	
	,constructor : function(options){
		System.Component.Validator.Length.superclass.constructor.call(this,options);
        if (null === this.min && null === this.max) {
            throw new Error('Either option "min" or "max" must be given for constraint');
        }
	}
	
	,validate : function(value){
		//System.Component.Validator.Length.superclass.validate.call(this,value);
		length = value.length;
		
        if (this.min == this.max && length != this.min) {
            this.exactMessage.sprintf(value,this.min);
            return false;
        }

        if (null !== this.max && length > this.max) {
            this.maxMessage.sprintf(value,this.max);
            return false;
        }

        if (null !== this.min && length < this.min) {
            this.minMessage.sprintf(value,this.min);
			return false; 
        }		
		
		return true;
	}
	
});


System.Component.Validator.Url = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This value is not a valid URL.'
    ,protocols : ['http', 'https']
	
	,constructor : function(options){
		
		System.Component.Validator.Url.superclass.constructor.call(this,options);
		
		if(!this.protocols instanceof Array ){
			throw new Error('Protocols must be an Array type');
		}
		
		this.protocols = this.protocols.join("|");
		this.url = '((^(%s)):\\/\\/([\\-\\w]+\\.)+\\w{2,3}(\\/[%\\-\\w]+(\\.\\w{2,})?)*(([\\w\\-\\.\\?\\\\\\/+@&#;`~=%!]*)(\.\w{2,})?)*\/?)';
		this.url = this.url.sprintf(this.protocols);
		this.url = new RegExp(this.url,'i');
		
	}
	
	,validate : function(value){
		//System.Component.Validator.Url.superclass.validate.call(this,value);
		return this.url.test(value);
		
	}
	
});

System.Component.Validator.Regex = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This value is not valid.'
    ,pattern : null
    ,match : true
	
	,constructor : function(options){
		System.Component.Validator.Regex.superclass.constructor.call(this,options);

		if(!this.pattern instanceof RegExp){
			throw new Error('Pattern must be an RegExp type');
		}
		
	}
	
	,validate : function(value){
		//System.Component.Validator.Regex.superclass.validate.call(this,value);
		
	
		if(typeof(this.pattern) == 'string'){
			try {
				this.pattern = eval(this.pattern);

				if(!this.pattern instanceof RegExp){
					throw new Error('Pattern must be an RegExp type');
				}
				
			} catch(e) {
				return false;
			}
		}


		return (this.pattern.test(value)) ? true : false;
		
	}
	
});

System.Component.Validator.Ip = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This is not a valid IP address.'
	
	,constructor : function(options){
		System.Component.Validator.Ip.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		return /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/.test(v);
	}
	
});

System.Component.Validator.Range = Ext.extend(System.Component.Validator.Validator,{
	
	minMessage : 'This value should be %d or more.'
	,maxMessage : 'This value should be %d or less.'
	,invalidMessage : 'This value should be a valid number.'
	,min : -1
	,max : -1

	
	,constructor : function(options){
		System.Component.Validator.Range.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Range.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Date = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This value is not a valid date.'
	
	,constructor : function(options){
		System.Component.Validator.Date.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Date.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.DateTime = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This value is not a valid datetime.'
	
	,constructor : function(options){
		System.Component.Validator.DateTime.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.DateTime.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Time = Ext.extend(System.Component.Validator.Validator,{
	
    message : 'This value is not a valid time.'
	
	,constructor : function(options){
		System.Component.Validator.Time.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Time.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Count = Ext.extend(System.Component.Validator.Validator,{
	
	minMessage : 'This collection should contain %d element or more.\nThis collection should contain %d elements or more.'
	,maxMessage : 'This collection should contain %d element or less.\nThis collection should contain %d elements or less.'
	,exactMessage : 'This collection should contain exactly %d element.\nThis collection should contain exactly %d elements.'
	,min : null
	,max : null
	
	,constructor : function(options){
		System.Component.Validator.Count.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Count.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Language = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'This value is not a valid language.'
	
	,constructor : function(options){
		System.Component.Validator.Language.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Language.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Locale = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'This value is not a valid locale.'
	
	,constructor : function(options){
		System.Component.Validator.Locale.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Locale.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Country = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'This value is not a valid country.'
	
	,constructor : function(options){
		System.Component.Validator.Country.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Country.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.File = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'This value is not a valid file.'
	,maxSize : null
	,mimeTypes : []
	,maxSizeMessage : 'The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is %d {{ suffix }}.'
	,mimeTypesMessage : 'The mime type of the file is invalid ({{ type }}). Allowed mime types are {{ types }}.'
	
	,constructor : function(options){
		System.Component.Validator.File.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.File.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Image = Ext.extend(System.Component.Validator.File,{
	
	message : 'This value is not a valid image.'
	,mimeTypes : 'image/*'
	
	,constructor : function(options){
		System.Component.Validator.Image.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Image.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.CardScheme = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'Unsupported card type or invalid card number.'
	,schemes : []
	
	,constructor : function(options){
		System.Component.Validator.CardScheme.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.CardScheme.superclass.validate.call(this,value);
		return true;
	}
	
});

System.Component.Validator.Luhn = Ext.extend(System.Component.Validator.Validator,{
	
	message : 'Invalid card number.'
	
	,constructor : function(options){
		System.Component.Validator.Luhn.superclass.constructor.call(this,options);
	}
	
	,validate : function(value){
		//System.Component.Validator.Luhn.superclass.validate.call(this,value);
		return true;
	}
	
});
